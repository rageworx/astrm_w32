# ASTRM client, Win32 DirectX 9 or above #
* This project is my private version of A-streaming device for Windows directX 9 and higher graphical accelerated rendering (using Direct2D).

### What you need to build this source? ###
* MSYS2 + MinGW-W64 x86.64
* DirectX SDK(or maybe included in MinGW-W64)
* Code::Blocks IDE.
* YASM required to enable SIMD (SSE3, SSE4) for ffmpeg.
* libFFmpeg 3.x, or above. (under 3.0 not be compiled with errors)

### How do I get set up? ###
* Copy libffmpeg source codes into ffmpeg directroy.
* Check your YASM exactly runs on shell.
* Configure cfg4mingw.sh with M-SYS shell.
* Make libffmpeg.a with mkffmpeglib.sh with M-SYS shell.
* Build with Code::Blocks IDE for Windows. (This project not provides Makefile)
* Done.

### YASM ###

* Install latest yasm with pacman : `pacman -S yasm` on MSYS2 shell.

### Latest updates ###

#### Version 0.4.12.115
   - All targets are for Windows x86.64
   - Updated to using ffmpeg 3.2.x

#### Version 0.4.10.111 ####
   - Updated to support SIMD with YASM 1.3.0 win32/64.
   - Updated to using ffmpeg 3.1.1.

### Previous updates ###
#### Version 0.4.10.110 ####
   - Fixed a bug of abnormal termination at stop each viewing modes.
   - Updated to listening control port. (if it disconnected, RTSP may not work)   
   
### Imported licenses ###
* libffmpeg, LGPL v2.1+, http://ffmpeg.org
* libtinyXML, http://tinyxpath.sourceforge.net/
* Song: Magic Music (loop), http://www.orangefreesounds.com/
* Sound: Bleep, http://www.orangefreesounds.com/