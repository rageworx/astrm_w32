#!/bin/bash

COMMON_OPT="--disable-schannel --disable-iconv --disable-sdl2"

#test YASM
YASM="yasm"
YASM_TEST=`$YASM --version`
YASM_H="yasmversion.h"

#DXVA2 option
DXVA2OPT=0
DXVA2STR=""

if [ $DXVA2OPT -eq 0 ]; then
	echo "DXVA2 disabled."
	DXVA2STR="--disable-dxva2"
else
	echo "DXVA2 enabeld."
fi

if [ -z "$YASM_TEST" ]; then
	echo "YASM not found."
	echo "#define YASM_VERSION \"no-yasm\"" > $YASM_H
	./configure $COMMON_OPT --disable-yasm  $DXVA2STR
else
	echo "YASM found."
	echo "#define YASM_VERISON \"$YASM\"" > $YASM_H
	./configure $COMMON_OPT --yasmexe=$YASM $DXVA2STR
fi
