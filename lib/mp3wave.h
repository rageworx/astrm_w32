#ifndef __MP3WAVE_H__
#define __MP3WAVE_H__

#ifdef __cplusplus
    extern "C" {
#endif /// of  __cplusplus

typedef struct _mp3track_info
{
    int sample_rate;
    int channels;
    int audio_bytes;  // generated amount of audio per frame
} mp3track_info;

int convert_mp3_to_wave( const unsigned char* src, int srcsize, unsigned char** out, mp3track_info* oinfo );

#ifdef __cplusplus
    };
#endif /// of  __cplusplus

#endif
