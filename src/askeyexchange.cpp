#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "askeyexchange.h"
#include <tinyxml2.h>

#include <vector>
#include <algorithm>

using namespace std;

////////////////////////////////////////////////////////////////////////////////

#define DEF_ASKE_CONF_FILE          "keyexch.xml"

////////////////////////////////////////////////////////////////////////////////

typedef struct _as_bindmapper
{
    unsigned int    src_bind;
    unsigned int    src_val;
    unsigned int    dst_bind;
    unsigned char   dst_val;
    int             dst_param;
}as_bindmapper;

////////////////////////////////////////////////////////////////////////////////

using namespace tinyxml2;

////////////////////////////////////////////////////////////////////////////////

vector< as_bindmapper* > binds_keyboard;
vector< as_bindmapper* > binds_mouse;
vector< as_bindmapper* > binds_joystick;

XMLDocument*  keyexcDoc = NULL;

////////////////////////////////////////////////////////////////////////////////


void clearallbinds()
{
    int szkbd = binds_keyboard.size();
    int szmse = binds_mouse.size();
    int szjoy = binds_joystick.size();

    if ( szkbd > 0 )
    {
        binds_keyboard.erase( binds_keyboard.begin(),
                              binds_keyboard.begin() + binds_keyboard.size() );
        binds_keyboard.clear();
    }

    if ( szmse > 0 )
    {
        binds_mouse.erase( binds_mouse.begin(),
                              binds_mouse.begin() + binds_mouse.size() );
        binds_mouse.clear();
    }

    if ( szjoy > 0 )
    {
        binds_joystick.erase( binds_joystick.begin(),
                              binds_joystick.begin() + binds_joystick.size() );
        binds_joystick.clear();
    }

}

void initkeyexchange()
{
    if ( keyexcDoc == NULL )
    {
        keyexcDoc = new XMLDocument();
    }
}

void finalkeyexchange()
{
    clearallbinds();

    if ( keyexcDoc != NULL )
    {
        delete keyexcDoc;
        keyexcDoc = NULL;
    }
}

void addkeybind( as_bindmapper* src )
{
    if ( src != NULL )
    {
        if ( src->src_bind != AS_BIND_KEYBOARD )
            return;

        // check same type ...
        int foundidx = -1;

        for( int cnt=0; cnt<binds_keyboard.size(); cnt++ )
        {
            if ( binds_keyboard[cnt]->src_val == src->src_val )
            {
                foundidx = cnt;
                break;
            }
        }

        if ( foundidx >= 0 )
        {
            if ( binds_keyboard[foundidx]->dst_val != src->dst_val )
            {
                binds_keyboard[foundidx]->dst_val = src->dst_val;
            }
        }
        else
        {
            binds_keyboard.push_back( src );
        }
    }
}

void addmousebind( as_bindmapper* src )
{
    if ( src != NULL )
    {
        if ( src->src_bind != AS_BIND_MOUSE )
            return;

        // check same type ...
        int foundidx = -1;

        for( int cnt=0; cnt<binds_mouse.size(); cnt++ )
        {
            if ( binds_mouse[cnt]->src_val == src->src_val )
            {
                foundidx = cnt;
                break;
            }
        }

        if ( foundidx >= 0 )
        {
            if ( binds_mouse[foundidx]->dst_val != src->dst_val )
            {
                binds_mouse[foundidx]->dst_val = src->dst_val;
            }
        }
        else
        {
            binds_mouse.push_back( src );
        }
    }
}

void addjoystickbind( as_bindmapper* src )
{
    if ( src != NULL )
    {
        if ( src->src_bind != AS_BIND_JOYSTICK )
            return;

        // check same type ...
        int foundidx = -1;

        for( int cnt=0; cnt<binds_joystick.size(); cnt++ )
        {
            if ( binds_joystick[cnt]->src_val == src->src_val )
            {
                foundidx = cnt;
                break;
            }
        }

        if ( foundidx >= 0 )
        {
            if ( binds_joystick[foundidx]->dst_val != src->dst_val )
            {
                binds_joystick[foundidx]->dst_val = src->dst_val;
            }
        }
        else
        {
            binds_joystick.push_back( src );
        }
    }
}

bool loadkeyexchange( const char* fn )
{
    if ( keyexcDoc != NULL )
    {
        const char* loadfn = DEF_ASKE_CONF_FILE;

        if ( fn != NULL )
        {
            loadfn = fn;
        }

        if ( keyexcDoc->LoadFile( DEF_ASKE_CONF_FILE ) == true )
        {
            clearallbinds();

            XMLElement* node = keyexcDoc->FirstChildElement( "keyexchanges" );

            if ( node != NULL )
            {
                XMLElement* elemBind = node->FirstChildElement( "bind" );

                while ( elemBind  != NULL )
                {
                    as_bindmapper* newbind = new as_bindmapper;

                    int src_type = -1;
                    int src_k = 0;
                    int dst_type = -1;
                    int dst_k = 0;
                    int dst_p = 0;

                    src_type = elemBind->IntAttribute( "source" );
                    src_k    = elemBind->IntAttribute( "key" );
                    dst_type = elemBind->IntAttribute( "target" );
                    dst_k    = elemBind->IntAttribute( "value" );
                    dst_p    = elemBind->IntAttribute( "param" );

                    if ( ( src_type > 0 ) && ( dst_type > 0 ) && ( src_type != dst_type ) )
                    {
                        if ( (src_k >= 0 ) && ( dst_k >= 0 ) )
                        {
                            newbind->src_bind  = src_type;
                            newbind->src_val   = src_k;
                            newbind->dst_bind  = dst_type;
                            newbind->dst_val   = dst_k;
                            newbind->dst_param = dst_p;
#ifdef DEBUG
                            printf( "> load new k.bind : s.bind:%d - s.val:%d - d.bind:%d - d.val:%d - d.param:%d\n",
                                    newbind->src_bind,
                                    newbind->src_val,
                                    newbind->dst_bind,
                                    newbind->dst_val,
                                    newbind->dst_param );
#endif
                            switch( src_type )
                            {
                                case AS_BIND_KEYBOARD:
                                    addkeybind( newbind );
                                    break;

                                case AS_BIND_MOUSE:
                                    addmousebind( newbind );
                                    break;

                                case AS_BIND_JOYSTICK:
                                    addjoystickbind( newbind );
                                    break;
                            }
                        }
                    }

                    elemBind = elemBind->NextSiblingElement( "bind" );
                }

                return true;
            }

        }
    }

    return false;
}

void unloadkeyexchange()
{
    clearallbinds();
}


int keyboardbindsize()
{
    return binds_keyboard.size();
}

int mousebindsize()
{
    return binds_mouse.size();
}

int joystickbindsize()
{
    return binds_joystick.size();
}

bool getkeyboardbind( unsigned int index, unsigned int &src, unsigned int &dst_type, unsigned char &dst, int &param )
{
    if ( index < binds_keyboard.size() )
    {
        src = binds_keyboard[index]->src_val;
        dst = binds_keyboard[index]->dst_val;
        dst_type = binds_keyboard[index]->dst_bind;
        param = binds_keyboard[index]->dst_param;

        return true;
    }

    return false;
}

bool getmousebind( unsigned int index, unsigned int &src, unsigned int &dst_type, unsigned char &dst, int &param )
{
    if ( index< binds_mouse.size() )
    {
        src = binds_mouse[index]->src_val;
        dst = binds_mouse[index]->dst_val;
        dst_type = binds_mouse[index]->dst_bind;
        param = binds_mouse[index]->dst_param;

        return true;
    }

    return false;
}

bool getjoystickbind( unsigned int index, unsigned int &src, unsigned int &dst_type, unsigned char &dst, int &param )
{
    if ( index < binds_joystick.size() )
    {
        src = binds_joystick[index]->src_val;
        dst = binds_joystick[index]->dst_val;
        dst_type = binds_joystick[index]->dst_bind;
        param = binds_joystick[index]->dst_param;

        return true;
    }

    return false;
}
