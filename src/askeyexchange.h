#ifndef __ASKEYEXCHANGE_H__
#define __ASKEYEXCHANGE_H__

////////////////////////////////////////////////////////////////////////////////

typedef enum
{
    AS_BIND_NONE         = 0,
    AS_BIND_KEYBOARD,
    AS_BIND_MOUSE,
    AS_BIND_JOYSTICK,
    AS_BIND_MAX
}as_bind_type;

////////////////////////////////////////////////////////////////////////////////

void initkeyexchange();
void finalkeyexchange();

bool loadkeyexchange( const char* fn = NULL );
void unloadkeyexchange();

int keyboardbindsize();
int mousebindsize();
int joystickbindsize();

bool getkeyboardbind( unsigned int index, unsigned int &src, unsigned int &dst_type, unsigned char &dst, int &param );
bool getmousebind( unsigned int index, unsigned int &src, unsigned int &dst_type, unsigned char &dst, int &param );
bool getjoystickbind( unsigned int index, unsigned int &src, unsigned int &dst_type, unsigned char &dst, int &param );

#endif /// of __ASKEYEXCHANGE_H__
