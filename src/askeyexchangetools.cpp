#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "askeyexchangetools.h"
#include "askeyexchange.h"
#include "hidproc.h"
#include "askeymap.h"

////////////////////////////////////////////////////////////////////////////////

unsigned int askexc_kbd_len = 0;
unsigned int askexc_mse_len = 0;
unsigned int askexc_joy_len = 0;

as_kexc_data askexc_kbd_data[DEF_MAX_KEYEXCHANGE_SIZE] = {0};
as_kexc_data askexc_mse_data[DEF_MAX_KEYEXCHANGE_SIZE] = {0};
as_kexc_data askexc_joy_data[DEF_MAX_KEYEXCHANGE_SIZE] = {0};

////////////////////////////////////////////////////////////////////////////////

void proc_askexch_init()
{
    memset( &askexc_kbd_data, 0, sizeof( as_kexc_data ) * DEF_MAX_KEYEXCHANGE_SIZE );
    memset( &askexc_mse_data, 0, sizeof( as_kexc_data ) * DEF_MAX_KEYEXCHANGE_SIZE );
    memset( &askexc_joy_data, 0, sizeof( as_kexc_data ) * DEF_MAX_KEYEXCHANGE_SIZE );

    askexc_kbd_len = 0;
    askexc_mse_len = 0;
    askexc_joy_len = 0;
}

bool proc_askexch_load()
{
    // Clear data set.
    proc_askexch_init();

    if ( loadkeyexchange() == true )
    {
        askexc_kbd_len = keyboardbindsize();
        askexc_mse_len = mousebindsize();
        askexc_joy_len = joystickbindsize();

        if ( askexc_kbd_len > DEF_MAX_KEYEXCHANGE_SIZE )
        {
            askexc_kbd_len = DEF_MAX_KEYEXCHANGE_SIZE;
        }

        if ( askexc_mse_len > DEF_MAX_KEYEXCHANGE_SIZE )
        {
            askexc_mse_len = DEF_MAX_KEYEXCHANGE_SIZE;
        }

        if ( askexc_joy_len > DEF_MAX_KEYEXCHANGE_SIZE )
        {
            askexc_joy_len = DEF_MAX_KEYEXCHANGE_SIZE;
        }


        if ( askexc_kbd_len > 0 )
        {
            for( int cnt=0; cnt<askexc_kbd_len; cnt++ )
            {
                getkeyboardbind( cnt,
                                 askexc_kbd_data[cnt].src_val,
                                 askexc_kbd_data[cnt].dst_type,
                                 askexc_kbd_data[cnt].dst_val,
                                 askexc_kbd_data[cnt].dst_param );
            }
        }

        if ( askexc_mse_len > 0 )
        {
            for( int cnt=0; cnt<askexc_mse_len; cnt++ )
            {
                getmousebind( cnt,
                              askexc_mse_data[cnt].src_val,
                              askexc_mse_data[cnt].dst_type,
                              askexc_mse_data[cnt].dst_val,
                              askexc_mse_data[cnt].dst_param );
            }

        }

        if ( askexc_joy_len > 0 )
        {
            for( int cnt=0; cnt<askexc_joy_len; cnt++ )
            {
                getjoystickbind( cnt,
                                 askexc_joy_data[cnt].src_val,
                                 askexc_joy_data[cnt].dst_type,
                                 askexc_joy_data[cnt].dst_val,
                                 askexc_joy_data[cnt].dst_param );
            }
        }

        return true;
    }

    return false;
}

void proc_askexch_unload()
{
    unloadkeyexchange();
    proc_askexch_init();
}

int  get_askexch_kbd_binds()
{
    return askexc_kbd_len;
}

int  get_askexch_mse_binds()
{
    return askexc_mse_len;
}

int  get_askexch_joy_binds()
{
    return askexc_joy_len;
}

void set_mouse_t( int dstval, int dstparam, MOUSESTATE* mouses )
{
    if ( mouses == NULL )
        return;

    switch( dstval )
    {
        case 0: /// axis-x
            mouses->lX = dstparam;
            break;

        case 1: /// axis-y
            mouses->lY = dstparam;
            break;

        case 2: /// wheel-h,v
            mouses->lZ = dstparam;
            if ( mouses->lZ > 1 )
            {
                mouses->lZ = 1;
            }
            else
            if ( mouses->lZ < -1 )
            {
                mouses->lZ = -1;
            }
            break;

        case 3: /// buttons
            {
                if ( dstparam < 8 )
                {
                    mouses->buttons[ dstparam ] = 0x80;
                }
            }
            break;
    }
}

void set_joystick_t( int dstval, int dstparam, JOYSTATE* joys )
{
    if ( joys == NULL )
        return;

    switch( dstval )
    {
        case 0: /// axis-x
            joys->lX = dstparam;
            break;

        case 1: /// axis-y
            joys->lY = dstparam;
            break;

        case 2: /// axis-z
            joys->lZ = dstparam;
            break;

        case 3: /// axis-Rx
            joys->lRx = dstparam;
            break;

        case 4: /// axis-Ry
            joys->lRy = dstparam;
            break;

        case 5: /// axis-Rz
            joys->lRz = dstparam;
            break;

        case 6: /// buttons
            {
                unsigned short btns = (unsigned short)dstparam;
                unsigned short testbits = 0x0001;

                for( int cnt=0; cnt<16; cnt++ )
                {
                    if ( ( btns & testbits ) > 0 )
                    {
                        joys->rgbButtons[cnt] = 0x80;
                    }
                }
            }
            break;
    }

}

bool proc_kbd_in_binds( KEYSTATE* keys, MOUSESTATE* mouses, JOYSTATE* joys )
{
    if ( ( keys != NULL ) && ( mouses != NULL ) && ( joys != NULL ) )
    {
        bool changed = false;

        for( int keycnt=0; keycnt<keys->length; keycnt++ )
        {
            for ( int cnt=0; cnt<askexc_kbd_len; cnt++ )
            {
                if ( askexc_kbd_data[cnt].src_val  == keys->keys[keycnt] )
                {
                    switch( askexc_kbd_data[cnt].dst_type )
                    {
                        case AS_BIND_MOUSE:
                            set_mouse_t( askexc_kbd_data[cnt].dst_val,
                                         askexc_kbd_data[cnt].dst_param,
                                         mouses );
                            keys->keys[keycnt] = 0;
                            changed = true;
                            break;

                        case AS_BIND_JOYSTICK:
                            set_joystick_t( askexc_kbd_data[cnt].dst_val,
                                            askexc_kbd_data[cnt].dst_param,
                                            joys );
                            keys->keys[keycnt] = 0;
                            changed = true;
                            break;
                    }
                }
            }
        }

        return changed;
    }

    return false;
}

bool proc_joy_in_binds( JOYSTATE* joys, MOUSESTATE* mouses )
{
    bool changed = false;

    if ( ( joys != NULL ) && ( mouses != NULL ) )
    {
        for( int cnt=0; cnt<askexc_joy_len; cnt++ )
        {
            bool btn_f = false;
            int  btn_val = -1;
            int  btn_par = -1;
            bool axis_f = true;
            long axis_val = 0;

            if( askexc_joy_data[cnt].dst_type == AS_BIND_MOUSE )
            {
                switch( askexc_joy_data[cnt].src_val )
                {
                    case 0: /// axis-x
                        axis_val = joys->lX;
                        joys->lX = 0;
                        break;

                    case 1: /// axis-y
                        axis_val = joys->lY;
                        joys->lY = 0;
                        break;

                    case 2: /// axis-z
                        axis_val = joys->lZ;
                        joys->lZ = 0;
                        break;

                    case 3: /// axis-Rx
                        axis_val = joys->lRx;
                        joys->lRx = 0;
                        break;

                    case 4: /// axis-Ry
                        axis_val = joys->lRy;
                        joys->lRy = 0;
                        break;

                    case 5: /// axis-Rz
                        axis_val = joys->lRz;
                        joys->lRz = 0;
                        break;

                    case 6: /// 6+16 = 22
                    case 7:
                    case 8:
                    case 9:
                    case 10:
                    case 11:
                    case 12:
                    case 13:
                    case 14:
                    case 15:
                    case 16:
                    case 17:
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                        btn_val = askexc_joy_data[cnt].src_val - 6;
                        if ( btn_val >= 0 )
                        {
                            axis_f = false;
                            btn_par = -1;

                            if ( ( joys->rgbButtons[btn_val] & 0x80 ) > 0 )
                            {
                                joys->rgbButtons[btn_val] = 0;
                                btn_par = askexc_joy_data[cnt].dst_param;
                            }
                            else
                            {
                                btn_par = -1;
                            }
                        }
                        break;
                }

                if ( axis_f == true )
                {
                    if ( askexc_joy_data[cnt].dst_val < 3 )
                    {
                        // minimal threshold to 100/-100 ...
                        if ( ( axis_val > 100 ) || ( axis_val < -100 ) )
                        {
                            int scaledv = ( float( axis_val ) / 1000.0f ) * 127.0f;

                            set_mouse_t( askexc_joy_data[cnt].dst_val,
                                         scaledv,
                                         mouses );
                            changed = true;
                        }
                    }
                    else
                    {
                        int threshold = axis_val / 500; /// threshold = 50%

                        if ( threshold > 0 )
                        {
                            if ( btn_val >= 0 )
                            {
                                if ( btn_par >= 0 )
                                {
                                    set_mouse_t( 3,
                                                 btn_par,
                                                 mouses );
                                }

                                changed = true;
                            }
                        }
                    }
                }
                else
                {
                    if ( ( btn_val >= 0 ) && ( btn_par >= 0 ) )
                    {
                        if ( btn_par >= 0 )
                        {
                            set_mouse_t( 3,
                                         btn_par,
                                         mouses );
                        }

                        changed = true;
                    }
                }
            }
        }
    }

    // bug fix.
    if ( changed == false )
    {
        // Joystick need always return "cahnged" flag.
        changed = true;
    }

    return changed;
}

bool proc_override_mouse( MOUSESTATE* org, MOUSESTATE* overrd )
{
    if ( ( org != NULL ) && ( overrd != NULL ) )
    {
        bool changed = false;

        if ( overrd->lX != 0 )
        {
            org->lX = overrd->lX;
            changed = true;
        }

        if ( overrd->lY != 0 )
        {
            org->lY = overrd->lY;
            changed = true;
        }

        if ( overrd->lZ != 0 )
        {
            org->lZ = overrd->lZ;
            changed = true;
        }

        for( int cnt=0; cnt<8; cnt++ )
        {
            if ( overrd->buttons[cnt] != 0 )
            {
                org->buttons[cnt] = overrd->buttons[cnt];
                changed = true;
            }
        }

        return changed;
    }

    return false;
}

bool proc_override_joystick( JOYSTATE* org, JOYSTATE* overrd )
{
    if ( ( org != NULL ) && ( overrd != NULL ) )
    {
        bool changed = false;

        if ( overrd->lX != 0 )
        {
            org->lX = overrd->lX;
            changed = true;
        }

        if ( overrd->lY != 0 )
        {
            org->lY = overrd->lY;
            changed = true;
        }

        if ( overrd->lZ != 0 )
        {
            org->lZ = overrd->lZ;
            changed = true;
        }

        if ( overrd->lRx != 0 )
        {
            org->lRx = overrd->lRx;
            changed = true;
        }

        if ( overrd->lRy != 0 )
        {
            org->lRy = overrd->lRy;
            changed = true;
        }

        if ( overrd->lRz != 0 )
        {
            org->lRz = overrd->lRz;
            changed = true;
        }

        for( int cnt=0; cnt<128; cnt++ )
        {
            if ( ( overrd->rgbButtons[cnt] != 0 ) &&
                 ( org->rgbButtons[cnt] != overrd->rgbButtons[cnt] ) )
            {
                org->rgbButtons[cnt] = overrd->rgbButtons[cnt];
                changed = true;
            }
        }
    }

    return false;
}

int  proc_dxmouse2aspacket( MOUSESTATE* mstate, float adj, int* mouseup, char** packet )
{
	if ( mstate == NULL )
		return 0;

	int cur_msx = mstate->lX * adj;
	int cur_msy = mstate->lY * adj;
	int cur_msw = 0;
	int cur_msb = 0;

	if ( cur_msx > 127 )
	{
	    cur_msx = 127;
	}
	else
	if( cur_msy < -127 )
	{
	    cur_msy = 127;
	}

	if ( cur_msy > 127 )
	{
	    cur_msy = 127;
	}
	else
	if ( cur_msy < -127 )
	{
	    cur_msy = 127;
	}

	for( int cnt=0; cnt<8; cnt++ )
	{
		if ( ( mstate->buttons[cnt] & 0x80 ) > 0 )
		{
			switch( cnt )
			{
				case 0:    /// Left
					cur_msb |= 1;
					*mouseup = 0;
					break;

				case 1:    /// Right
					cur_msb |= 2;
					*mouseup = 1;
					break;

				case 2:    /// Middle
					cur_msb |= 4;
					*mouseup = 2;
					break;

				default:    /// Other
					*mouseup = cnt;
					break;
			}
		}
	}

	if ( mstate->lZ < 0 )
	{
		cur_msw = -1;
	}
	else
	if ( mstate->lZ > 0 )
	{
		cur_msw = 1;
	}

	unsigned char msbtns = char(cur_msb);

	return anystream_hid_mouse_combine( cur_msx, cur_msy, cur_msw, 0, msbtns, packet );
}
