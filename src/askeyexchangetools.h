#ifndef __ASKEYEXCHANGETOOL_H__
#define __ASKEYEXCHANGETOOL_H__

#include "dxinput.h"

////////////////////////////////////////////////////////////////////////////////

#define DEF_MAX_KEYEXCHANGE_SIZE            32

////////////////////////////////////////////////////////////////////////////////

typedef struct _as_kexc_data
{
    unsigned int    src_val;
    unsigned int    dst_type; /// among keyboard(1), mouse(2), joystick(3)
    unsigned char   dst_val;
    int             dst_param;
}as_kexc_data;

////////////////////////////////////////////////////////////////////////////////

bool proc_askexch_load();
void proc_askexch_unload();

int  get_askexch_kbd_binds();
int  get_askexch_mse_binds();
int  get_askexch_joy_binds();

bool proc_kbd_in_binds( KEYSTATE* keys, MOUSESTATE* mouses, JOYSTATE* joys );
bool proc_joy_in_binds( JOYSTATE* joys, MOUSESTATE* mouses );

bool proc_override_mouse( MOUSESTATE* org, MOUSESTATE* overrd );
bool proc_override_joystick( JOYSTATE* org, JOYSTATE* overrd );

int  proc_dxmouse2aspacket( MOUSESTATE* mstate, float adj, int* mouseup, char** packet );

////////////////////////////////////////////////////////////////////////////////

#endif /// of __ASKEYEXCHANGETOOL_H__
