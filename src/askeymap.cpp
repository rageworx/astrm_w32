#include <dinput.h>
#include "askeymap.h"

// Nogada by Raphael Kim.
// refer to document HID_USAGE_TABLES_Hut1_12v2.pdf , in start page of 53.

const unsigned char dxkeymaps[256] = {
    // 0x00 -- no key mapped.
    0x00,   /// == ASHID, Reserved, no key.

    //DIK_ESCAPE          0x01
    0x29,
    //DIK_1               0x02
    0x1E,
    //DIK_2               0x03
    0x1F,
    //DIK_3               0x04
    0x20,
    //DIK_4               0x05
    0x21,
    //DIK_5               0x06
    0x22,
    //DIK_6               0x07
    0x23,
    //DIK_7               0x08
    0x24,
    //DIK_8               0x09
    0x25,
    //DIK_9               0x0A
    0x26,
    //DIK_0               0x0B
    0x27,
    //DIK_MINUS           0x0C    /* - on main keyboard */
    0x2D,
    //DIK_EQUALS          0x0D
    0x2E,
    //DIK_BACK            0x0E    /* backspace */
    0x2A,
    //DIK_TAB             0x0F
    0x2B,
    //DIK_Q               0x10
    0x14,
    //DIK_W               0x11
    0x1A,
    //DIK_E               0x12
    0x08,
    //DIK_R               0x13
    0x15,
    //DIK_T               0x14
    0x17,
    //DIK_Y               0x15
    0x1C,
    //DIK_U               0x16
    0x18,
    //DIK_I               0x17
    0x0C,
    //DIK_O               0x18
    0x12,
    //DIK_P               0x19
    0x13,
    //DIK_LBRACKET        0x1A
    0x2F,
    //DIK_RBRACKET        0x1B
    0x30,
    //DIK_RETURN          0x1C    /* Enter on main keyboard */
    0x28,
    //DIK_LCONTROL        0x1D
    0x00, /// * This key may controlled in modifier bits section *
    //DIK_A               0x1E
    0x04,
    //DIK_S               0x1F
    0x16,
    //DIK_D               0x20
    0x07,
    //DIK_F               0x21
    0x09,
    //DIK_G               0x22
    0x0A,
    //DIK_H               0x23
    0x0B,
    //DIK_J               0x24
    0x0D,
    //DIK_K               0x25
    0x0E,
    //DIK_L               0x26
    0x0F,
    //DIK_SEMICOLON       0x27
    0x33,
    //DIK_APOSTROPHE      0x28
    0x34,
    //DIK_GRAVE           0x29    /* accent grave */
    0x35,
    //DIK_LSHIFT          0x2A
    0x00, /// * This key may controlled in modifier bits section *
    //DIK_BACKSLASH       0x2B
    0x00, /// * Not supported *
    //DIK_Z               0x2C
    0x1D,
    //DIK_X               0x2D
    0x1B,
    //DIK_C               0x2E
    0x06,
    //DIK_V               0x2F
    0x19,
    //DIK_B               0x30
    0x05,
    //DIK_N               0x31
    0x11,
    //DIK_M               0x32
    0x10,
    //DIK_COMMA           0x33    /// == ","
    0x36,
    //DIK_PERIOD          0x34    /* . on main keyboard */
    0x37,
    //DIK_SLASH           0x35    /* / on main keyboard */
    0x38,
    //DIK_RSHIFT          0x36
    0x00, /// * This key may controlled in modifier bits section *
    //DIK_MULTIPLY        0x37    /* * on numeric keypad */
    0x55,
    //DIK_LMENU           0x38    /* left Alt */
    0x00, /// * This key may controlled in modifier bits section *
    //DIK_SPACE           0x39
    0x2C,
    //DIK_CAPITAL         0x3A
    0x00, /// * Not supported *
    //DIK_F1              0x3B
    0x3A,
    //DIK_F2              0x3C
    0x3B,
    //DIK_F3              0x3D
    0x3C,
    //DIK_F4              0x3E
    0x3D,
    //DIK_F5              0x3F
    0x3E,
    //DIK_F6              0x40
    0x3F,
    //DIK_F7              0x41
    0x40,
    //DIK_F8              0x42
    0x41,
    //DIK_F9              0x43
    0x42,
    //DIK_F10             0x44
    0x43,
    //DIK_NUMLOCK         0x45
    0x53,
    //DIK_SCROLL          0x46    /* Scroll Lock */
    0x47,
    //DIK_NUMPAD7         0x47
    0x5F,
    //DIK_NUMPAD8         0x48
    0x60,
    //DIK_NUMPAD9         0x49
    0x61,
    //DIK_SUBTRACT        0x4A    /* - on numeric keypad */
    0x56,
    //DIK_NUMPAD4         0x4B
    0x5C,
    //DIK_NUMPAD5         0x4C
    0x5D,
    //DIK_NUMPAD6         0x4D
    0x5E,
    //DIK_ADD             0x4E    /* + on numeric keypad */
    0x57,
    //DIK_NUMPAD1         0x4F
    0x59,
    //DIK_NUMPAD2         0x50
    0x5A,
    //DIK_NUMPAD3         0x51
    0x5B,
    //DIK_NUMPAD0         0x52
    0x62,
    //DIK_DECIMAL         0x53    /* . on numeric keypad */

    // 0x54
    0x00, /// * Not supported *
    // 0x55
    0x00, /// * Not supported *

    0x63,
    //DIK_OEM_102         0x56    /* < > | on UK/Germany keyboards */
    0x00, /// * Not supported *
    //DIK_F11             0x57
    0x44,
    //DIK_F12             0x58
    0x45,

    // 0x59
    0x00, /// * no key
    // 0x5A
    0x00, /// * no key
    // 0x5B
    0x00, /// * no key
    // 0x5C
    0x00, /// * no key
    // 0x5D
    0x00, /// * no key
    // 0x5E
    0x00, /// * no key
    // 0x5F
    0x00, /// * no key
    // 0x60
    0x00, /// * no key
    // 0x61
    0x00, /// * no key
    // 0x62
    0x00, /// * no key
    // 0x63
    0x00, /// * no key

    //DIK_F13             0x64    /*                     (NEC PC98) */
    0x68,
    //DIK_F14             0x65    /*                     (NEC PC98) */
    0x69,
    //DIK_F15             0x66    /*                     (NEC PC98) */
    0x6A,

    // 0x67
    0x00, /// * no key
    // 0x68
    0x00, /// * no key
    // 0x69
    0x00, /// * no key
    // 0x6A
    0x00, /// * no key
    // 0x6B
    0x00, /// * no key
    // 0x6C
    0x00, /// * no key
    // 0x6D
    0x00, /// * no key
    // 0x6E
    0x00, /// * no key
    // 0x6F
    0x00, /// * no key

    //DIK_KANA            0x70    /* (Japanese keyboard)            */
    0x00, /// * Not supported *

    // 0x71
    0x00, /// * no key
    // 0x72
    0x00, /// * no key

    //DIK_ABNT_C1         0x73    /* / ? on Portugese (Brazilian) keyboards */
    0x00, /// * Not supported *

    // 0x74
    0x00, /// * no key
    // 0x75
    0x00, /// * no key
    // 0x76
    0x00, /// * no key
    // 0x77
    0x00, /// * no key
    // 0x78
    0x00, /// * no key

    //DIK_CONVERT         0x79    /* (Japanese keyboard)            */
    0x00, /// * Not supported *

    // 0x7A
    0x00, /// * no key

    //DIK_NOCONVERT       0x7B    /* (Japanese keyboard)            */
    0x00, /// * Not supported *

    // 0x7C
    0x00, /// * no key

    //DIK_YEN             0x7D    /* (Japanese keyboard)            */
    0x00, /// * Not supported *
    //DIK_ABNT_C2         0x7E    /* Numpad . on Portugese (Brazilian) keyboards */
    0x00, /// * Not supported *

    // 0x7F
    0x00, /// * no key
    // 0x80
    0x00, /// * no key
    // 0x81
    0x00, /// * no key
    // 0x82
    0x00, /// * no key
    // 0x83
    0x00, /// * no key
    // 0x84
    0x00, /// * no key
    // 0x85
    0x00, /// * no key
    // 0x86
    0x00, /// * no key
    // 0x87
    0x00, /// * no key
    // 0x88
    0x00, /// * no key
    // 0x89
    0x00, /// * no key
    // 0x8A
    0x00, /// * no key
    // 0x8B
    0x00, /// * no key
    // 0x8C
    0x00, /// * no key

    //DIK_NUMPADEQUALS    0x8D    /* = on numeric keypad (NEC PC98) */
    0x67,

    // 0x8E
    0x00, /// * no key
    // 0x8F
    0x00, /// * no key

    //DIK_CIRCUMFLEX      0x90    /* (Japanese keyboard)            */
    0x00, /// * Not supported *
    //DIK_AT              0x91    /*                     (NEC PC98) */
    0x00, /// * Not supported *
    //DIK_COLON           0x92    /*                     (NEC PC98) */
    0x00, /// * Not supported *
    //DIK_UNDERLINE       0x93    /*                     (NEC PC98) */
    0x00, /// * Not supported *
    //DIK_KANJI           0x94    /* (Japanese keyboard)            */
    0x00, /// * Not supported *
    //DIK_STOP            0x95    /*                     (NEC PC98) */
    0x00, /// * Not supported *
    //DIK_AX              0x96    /*                     (Japan AX) */
    0x00, /// * Not supported *
    //DIK_UNLABELED       0x97    /*                        (J3100) */
    0x00, /// * Not supported *

    // 0x98
    0x00, /// * no key

    //DIK_NEXTTRACK       0x99    /* Next Track */
    0x00, /// * Not supported *

    // 0x9A
    0x00, /// * no key
    // 0x9B
    0x00, /// * no key

    //DIK_NUMPADENTER     0x9C    /* Enter on numeric keypad */
    0x00, /// * Not supported *
    //DIK_RCONTROL        0x9D
    0x00, /// * This key may controlled in modifier bits section *


    // 0x9E
    0x00, /// * no key
    // 0x9F
    0x00, /// * no key

    //DIK_MUTE	          0xA0    /* Mute */
    0x7F,
    //DIK_CALCULATOR      0xA1    /* Calculator */
    0x00, /// * Not supported *
    //DIK_PLAYPAUSE       0xA2    /* Play / Pause */
    0x74, /// HID means "EXECUTE" but seems play and paused.

    // 0xA3
    0x00, /// * no key

    //DIK_MEDIASTOP       0xA4    /* Media Stop */
    0x78,

    // 0xA5
    0x00, /// * no key
    // 0xA6
    0x00, /// * no key
    // 0xA7
    0x00, /// * no key
    // 0xA8
    0x00, /// * no key
    // 0xA9
    0x00, /// * no key
    // 0xAA
    0x00, /// * no key
    // 0xAB
    0x00, /// * no key
    // 0xAC
    0x00, /// * no key
    // 0xAD
    0x00, /// * no key

    //DIK_VOLUMEDOWN      0xAE    /* Volume - */
    0x81,

    // 0xAF
    0x00, /// * no key

    //DIK_VOLUMEUP        0xB0    /* Volume + */
    0x80,

    // 0xB1
    0x00, /// * no key

    //DIK_WEBHOME         0xB2    /* Web home */
    0x00, /// * Not supported *
    //DIK_NUMPADCOMMA     0xB3    /* , on numeric keypad (NEC PC98) */
    0x85,

    // 0xB4
    0x00, /// * no key

    //DIK_DIVIDE          0xB5    /* / on numeric keypad */
    0x54,

    // 0xB6
    0x00, /// * no key

    //DIK_SYSRQ           0xB7
    0x00, /// * Not supported *
    //DIK_RMENU           0xB8    /* right Alt */
    0x00, /// * This key may controlled in modifier bits section *

    // 0xB9
    0x00, /// * no key
    // 0xBA
    0x00, /// * no key
    // 0xBB
    0x00, /// * no key
    // 0xBC
    0x00, /// * no key
    // 0xBD
    0x00, /// * no key
    // 0xBE
    0x00, /// * no key
    // 0xBF
    0x00, /// * no key
    // 0xC0
    0x00, /// * no key
    // 0xC1
    0x00, /// * no key
    // 0xC2
    0x00, /// * no key
    // 0xC3
    0x00, /// * no key
    // 0xC4
    0x00, /// * no key

    //DIK_PAUSE           0xC5    /* Pause */
    0x48,

    // 0xC6
    0x00, /// * no key

    //DIK_HOME            0xC7    /* Home on arrow keypad */
    0x4A,
    //DIK_UP              0xC8    /* UpArrow on arrow keypad */
    0x52,
    //DIK_PRIOR           0xC9    /* PgUp on arrow keypad */
    0x61,

    // 0xCA
    0x00, /// * no key

    //DIK_LEFT            0xCB    /* LeftArrow on arrow keypad */
    0x50,

    // 0xCC
    0x00, /// * no key

    //DIK_RIGHT           0xCD    /* RightArrow on arrow keypad */
    0x4F,

    // 0xCE
    0x00, /// * no key

    //DIK_END             0xCF    /* End on arrow keypad */
    0x4D,
    //DIK_DOWN            0xD0    /* DownArrow on arrow keypad */
    0x51,
    //DIK_NEXT            0xD1    /* PgDn on arrow keypad */
    0x4E,
    //DIK_INSERT          0xD2    /* Insert on arrow keypad */
    0x49,
    //DIK_DELETE          0xD3    /* Delete on arrow keypad */
    0x4C,

    // 0xD4
    0x00, /// * no key
    // 0xD5
    0x00, /// * no key
    // 0xD6
    0x00, /// * no key
    // 0xD7
    0x00, /// * no key
    // 0xD8
    0x00, /// * no key
    // 0xD9
    0x00, /// * no key
    // 0xDA
    0x00, /// * no key

    //DIK_LWIN            0xDB    /* Left Windows key */
    0x00, /// * This key may controlled in modifier bits section *
    //DIK_RWIN            0xDC    /* Right Windows key */
    0x00, /// * This key may controlled in modifier bits section *
    //DIK_APPS            0xDD    /* AppMenu key */
    0x65, /// HID Application ?
    //DIK_POWER           0xDE
    0x66,
    //DIK_SLEEP           0xDF
    0x00, /// * Not supported *

    // 0xE0
    0x00, /// * no key
    // 0xE1
    0x00, /// * no key
    // 0xE2
    0x00, /// * no key

    //DIK_WAKE            0xE3    /* System Wake */
    0x00, /// * Not supported *

    // 0xE4
    0x00, /// * no key

    //DIK_WEBSEARCH       0xE5    /* Web Search */
    0x00, /// * Not supported *
    //DIK_WEBFAVORITES    0xE6    /* Web Favorites */
    0x00, /// * Not supported *
    //DIK_WEBREFRESH      0xE7    /* Web Refresh */
    0x00, /// * Not supported *
    //DIK_WEBSTOP         0xE8    /* Web Stop */
    0x00, /// * Not supported *
    //DIK_WEBFORWARD      0xE9    /* Web Forward */
    0x00, /// * Not supported *
    //DIK_WEBBACK         0xEA    /* Web Back */
    0x00, /// * Not supported *
    //DIK_MYCOMPUTER      0xEB    /* My Computer */
    0x00, /// * Not supported *
    //DIK_MAIL            0xEC    /* Mail */
    0x00, /// * Not supported *
    //DIK_MEDIASELECT     0xED    /* Media Select */
    0x00, /// * Not supported *

    // 0xEE ~ 0xFF End others are 0
    0x00, /// * no key
};

const
unsigned char* get_keymap()
{
    return dxkeymaps;
}

unsigned char  convert_dik_to_ashidkey( const unsigned char dxkey )
{
    return dxkeymaps[ dxkey ];
}
