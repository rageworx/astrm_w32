#ifndef __ASKEYMAP_H__
#define __ASKEYMAP_H__

const
unsigned char* get_keymap();
unsigned char  convert_dik_to_ashidkey( const unsigned char dxkey );

#endif /// of __ASKEYMAP_H__
