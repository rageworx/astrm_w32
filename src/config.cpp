#include <tinyxml2.h>
#include "config.h"

////////////////////////////////////////////////////////////////////////////////
//
// config version 04-A
//
// - latest update -
//
// 2020-11-13 .. updated tinymxl to tinymxl2.
//
// 2016-05-02 .. supports key exchange (binding to other)
//
////////////////////////////////////////////////////////////////////////////////

#define CONFIG_FNAME            "config.xml"
#define DEF_MAX_WINRESS         4
#define DEF_MAX_CONWAITS        2
#define DEF_DEFAULT_FONTNAME    "tahoma"

////////////////////////////////////////////////////////////////////////////////

const char default_config_xml[] =
{"<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n"
"<license>\n"
"    <auth type=\"free\" />\n"
"</license>\n"
"<config>\n"
"    <!-- window restypes : 0(FHD), 1(HD), 2(qHD 960×540), 3(nHD 640×360 ) -->\n"
"    <window x=\"0\" y=\"0\" restype=\"1\" cleartype=\"1\" font=\"Tahoma\"//>\n"
"    <output fullscreen=\"0\" audiopt=\"0\" />\n"
"    <input kbd=\"0\" mouse=\"0\" joystick=\"0\" />\n"
"    <sensitive mouse=\"0.5\" />\n"
"    <server ip=\"192.168.0.23\" waitsec=\"5\" />\n"
"    <!-- exit control device : 0=kbd, 1=mse -->\n"
"    <control exitdev=\"1\" exitkey=\"1\" />\n"
"</config>\n"};

////////////////////////////////////////////////////////////////////////////////

typedef struct _windowResWH
{
    int width;
    int height;
}windowResWH;

////////////////////////////////////////////////////////////////////////////////

const windowResWH windowRess[DEF_MAX_WINRESS] =
{
    {1920, 1080},   /// 0, FullHD
    {1280, 720},    /// 1, HD
    {960,  540},    /// 2, qHD
    {640,  360},    /// 3, nHD
};

////////////////////////////////////////////////////////////////////////////////

using namespace tinyxml2;

////////////////////////////////////////////////////////////////////////////////


XMLDocument*  confDoc = NULL;
confDatas     confData = {0};

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

bool initConfig()
{
    confDoc = new XMLDocument();

    if ( confDoc != NULL )
    {
        if ( confDoc->LoadFile( CONFIG_FNAME ) == XML_SUCCESS )
            return true;
    }

    if ( confDoc != NULL )
    {
        delete confDoc;
        confDoc = NULL;
    }

    return false;
}

// Updating with config structure and XML data.
// It may better call these conditions:
//   1. configuration updated in program.
//   2. before write config. to XML.
void updateAll()
{
    if ( confDoc != NULL )
    {
        XMLElement* node = confDoc->FirstChildElement( "config" );

        if ( node != NULL )
        {
            XMLElement* elemWin = node->FirstChildElement( "window" );

            if ( elemWin != NULL )
            {
                elemWin->SetAttribute( "x", confData.win_x );
                elemWin->SetAttribute( "y", confData.win_y );
                elemWin->SetAttribute( "restype", confData.res_t );
                elemWin->SetAttribute( "cleartype", confData.cleartype );
                elemWin->SetAttribute( "font", confData.fontname );
            }

            XMLElement* elemOutput = node->FirstChildElement( "output" );

            if ( elemOutput != NULL )
            {
                elemOutput->SetAttribute( "fullscreen", confData.fullscreen );
                elemOutput->SetAttribute( "audiopt", confData.audio_opt );
            }

            XMLElement* elemInput = node->FirstChildElement( "input" );

            if ( elemInput != NULL )
            {
                elemInput->SetAttribute( "kbd", confData.kbd_type );
                elemInput->SetAttribute( "mouse", confData.mouse_type );
                elemInput->SetAttribute( "joystick", confData.joystick_type );
                elemInput->SetAttribute( "keyexchange", confData.loadkeybinds );
            }

            XMLElement* elemSensitive = node->FirstChildElement( "sensitive" );
            if ( elemSensitive != NULL )
            {
                double tmpdbl = double( confData.mouse_div );
                elemSensitive->DoubleAttribute( "mouse", tmpdbl );
            }

            XMLElement* elemServer = node->FirstChildElement( "server" );

            if ( elemServer != NULL )
            {
                elemServer->SetAttribute( "ip", confData.server_ip );
                elemServer->SetAttribute( "waitsec", confData.conwaits );
            }

            XMLElement* elemControl = node->FirstChildElement( "control" );

            if ( elemControl != NULL )
            {
                elemControl->SetAttribute( "exitdev", confData.ctrl_exittype );
                elemControl->SetAttribute( "exitkey", confData.ctrl_exitcode );
            }
        }
    }
}

// Reads all configuration from XML...
void reloadAll()
{
    if ( confDoc != NULL )
    {
        bool changed = false;

        XMLElement* node = confDoc->FirstChildElement( "config" );

        if ( node != NULL )
        {
            XMLElement* elemWin = node->FirstChildElement( "window" );

            if ( elemWin != NULL )
            {
                confData.win_x = elemWin->IntAttribute( "x" );
                confData.win_y = elemWin->IntAttribute( "y" );
                confData.res_t = elemWin->IntAttribute( "restype" );
                confData.cleartype = elemWin->IntAttribute( "cleartype" );
                const char* reffontname = elemWin->Attribute( "font" );

                if ( reffontname != NULL )
                {
                    strncpy( confData.fontname, reffontname, 128 );
                }

                if ( confData.res_t < DEF_MAX_WINRESS )
                {
                    confData.win_w = windowRess[ confData.res_t ].width;
                    confData.win_h = windowRess[ confData.res_t ].height;
                }
                else
                {
                    confData.res_t = DEF_MAX_WINRESS - 1;
                    confData.win_w = windowRess[ confData.res_t ].width;
                    confData.win_h = windowRess[ confData.res_t ].height;
                    changed = true;
                }

                if ( strlen( confData.fontname ) == 0 )
                {
                    strcpy( confData.fontname, DEF_DEFAULT_FONTNAME );
                }
            }

            XMLElement* elemOutput = node->FirstChildElement( "output" );

            if ( elemOutput != NULL )
            {
                confData.fullscreen = elemOutput->IntAttribute( "fullscreen" );
                confData.audio_opt = elemOutput->IntAttribute( "audiopt" );

                if ( confData.audio_opt < 0 )
                {
                    confData.audio_opt = 0;
                }
            }

            XMLElement* elemInput = node->FirstChildElement( "input" );

            if ( elemInput != NULL )
            {
                confData.kbd_type = elemInput->IntAttribute( "kbd" );
                confData.mouse_type = elemInput->IntAttribute( "mouse" );
                confData.joystick_type = elemInput->IntAttribute( "joystick" );
                confData.loadkeybinds = elemInput->IntAttribute( "keyexchange" );
            }

            XMLElement* elemSensitive = node->FirstChildElement( "sensitive" );
            if ( elemSensitive != NULL )
            {
                double tmpdbl = 0.0;
                tmpdbl = elemSensitive->DoubleAttribute( "mouse" );
                confData.mouse_div = tmpdbl;

                if ( confData.mouse_div <= 0.0f )
                {
                    confData.mouse_div = 1.0f;
                }
                else
                if ( confData.mouse_div > 3.0f )
                {
                    confData.mouse_div = 3.0f;
                }
            }

            XMLElement* elemServer = node->FirstChildElement( "server" );

            if ( elemServer != NULL )
            {
                const char* refIpStr = elemServer->Attribute("ip");
                if ( refIpStr != NULL )
                {
                    int cplen = strlen( refIpStr );
                    if ( cplen < CONF_IP_MAX_LEN )
                    {
                        strcpy( confData.server_ip, refIpStr );
                    }
                }

                confData.conwaits = elemServer->IntAttribute( "waitsec" );

                if ( confData.conwaits < 0 )
                {
                    confData.conwaits = DEF_MAX_CONWAITS;
                }
                else
                if ( confData.conwaits > 60 )
                {
                    confData.conwaits = 60;
                }
            }

            XMLElement* elemControl = node->FirstChildElement( "control" );

            if ( elemControl != NULL )
            {
                confData.ctrl_exittype = elemControl->IntAttribute( "exitdev" );
                confData.ctrl_exitcode = elemControl->IntAttribute( "exitkey" );
            }

            if ( changed == true )
            {
                updateAll();

                confDoc->SaveFile( CONFIG_FNAME );
            }

        }
    }
}

bool loadConfig()
{
    if ( confDoc != NULL )
    {
        if ( confDoc->LoadFile( CONFIG_FNAME ) == XML_SUCCESS )
        {
            reloadAll();
            return true;
        }
    }

    return false;
}

bool saveConfig()
{
    if ( confDoc != NULL )
    {
        updateAll();

        if ( confDoc->SaveFile( CONFIG_FNAME ) == XML_SUCCESS )
        {
            return true;
        }
    }

    return false;
}

void finalConfig()
{
    if ( confDoc != NULL )
    {
        delete confDoc;
        confDoc = NULL;
    }
}


confDatas* getConfig()
{
    if ( confDoc != NULL )
    {
        return &confData;
    }

    return NULL;
}

bool writeDefaultXML()
{
    FILE* fp = fopen( CONFIG_FNAME, "wb" );
    if ( fp != NULL )
    {
        int dlen = strlen( default_config_xml );
        int wlen = fwrite( default_config_xml, 1, dlen, fp );
        fclose( fp );
        if ( wlen == dlen )
        {
            return true;
        }
    }

    return false;
}

confDatas* getDefaultConfig()
{
    if ( confDoc == NULL )
    {
        // Check file exists ...
        if ( access( CONFIG_FNAME, F_OK ) == 0 )
        {
            if ( initConfig() == true )
            {
                return getConfig();
            }
        }
        else
        // Create a new XML.
        if ( writeDefaultXML() == true )
        {
            if ( initConfig() == true )
            {
                return getConfig();
            }
        }
    }

    return NULL;
}
