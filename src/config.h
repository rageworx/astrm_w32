#ifndef __CONFIG_H__
#define __CONFIG_H__

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#ifndef _Bool
    #include <stdbool.h>
#endif

#define CONF_IP_MAX_LEN                 64

typedef struct _confDatas
{
    int     win_x;
    int     win_y;
    int     win_w;
    int     win_h;
    int     res_t;
    int     kbd_type;
    int     mouse_type;
    float   mouse_div;
    int     joystick_type;
    int     fullscreen;
    int     audio_opt;
    int     cleartype;
    char    fontname[128];
    char    server_ip[CONF_IP_MAX_LEN];
    int     conwaits;
    int     loadkeybinds;
    int     ctrl_exittype;
    int     ctrl_exitcode;
}confDatas;

bool initConfig();
bool loadConfig();
bool saveConfig();
void finalConfig();

confDatas* getConfig();
confDatas* getDefaultConfig();

#endif /// of __CONFIG_H__
