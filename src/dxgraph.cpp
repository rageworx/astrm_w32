#define _USE_MATH_DEFINES
#include <math.h>

#include "dxgraph.h"
#include "saferelease.h"

using namespace D2D1;

#define DTARGET_TYPE_UNKNOWN    -1
#define DTARGET_TYPE_HWND       0
#define DTARGET_TYPE_HDC        1

////////////////////////////////////////////////////////////////////////////////

HWND                    hWndParent          = NULL;
HDC                     hDCParent           = NULL;
int                     targetType          = DTARGET_TYPE_UNKNOWN;

float                   currentDPI_x        = 0.0f;
float                   currentDPI_y        = 0.0f;

ID2D1Factory*           pD2DFactory         = NULL;
ID2D1DCRenderTarget*    pD2DTargetDC        = NULL;
ID2D1HwndRenderTarget*  pD2DTargetHWND      = NULL;
IDWriteFactory*         pWriteFactory       = NULL;
IWICImagingFactory*     pWICImageFactory    = NULL;
IDWriteTextFormat*      pTextFmtTemp        = NULL;

////////////////////////////////////////////////////////////////////////////////

D2D_POINT_2F CalcCartesianCoordinate(FLOAT fAngle, FLOAT fRadius);

////////////////////////////////////////////////////////////////////////////////

ID2D1Factory* initializeDFactory( HWND parent )
{
    if ( pD2DFactory != NULL )
        return pD2DFactory;

    HRESULT hr = E_FAIL;

    hr = D2D1CreateFactory( D2D1_FACTORY_TYPE_SINGLE_THREADED, &pD2DFactory );

    if ( hr == S_OK )
    {
        hWndParent = parent;

        pD2DFactory->GetDesktopDpi( &currentDPI_x, &currentDPI_y );

        return pD2DFactory;
    }

    return NULL;
}

ID2D1DCRenderTarget* initializeRenderTargetDC( HDC dc )
{
    if ( pD2DFactory == NULL )
        return NULL;

    if ( dc == NULL )
        return NULL;

    SetLayout( dc, LAYOUT_BITMAPORIENTATIONPRESERVED );

    D2D1_RENDER_TARGET_PROPERTIES dxProp;

    dxProp = RenderTargetProperties( D2D1_RENDER_TARGET_TYPE_DEFAULT,
                                     PixelFormat(DXGI_FORMAT_B8G8R8A8_UNORM,  D2D1_ALPHA_MODE_PREMULTIPLIED ),
                                     0,
                                     0,
                                     D2D1_RENDER_TARGET_USAGE_NONE,
                                     D2D1_FEATURE_LEVEL_DEFAULT );

    HRESULT hr = E_FAIL;

    hr = pD2DFactory->CreateDCRenderTarget( &dxProp, &pD2DTargetDC );

    if ( hr == S_OK )
    {
        targetType = DTARGET_TYPE_HDC;
        hDCParent = dc;
        return pD2DTargetDC;
    }

    return NULL;
}

ID2D1HwndRenderTarget* initializeRenderTargetHWND()
{
    if ( pD2DFactory == NULL )
        return NULL;

    if ( hWndParent == NULL )
        return NULL;

    D2D1_RENDER_TARGET_PROPERTIES dxProp;

    dxProp = RenderTargetProperties();
    dxProp.usage = D2D1_RENDER_TARGET_USAGE_GDI_COMPATIBLE;

    RECT rc;
    GetClientRect( hWndParent, &rc );
    D2D1_SIZE_U dsrcsize = SizeU( rc.right- rc.left, rc.bottom - rc.top );

    HRESULT hr = E_FAIL;

    hr = pD2DFactory->CreateHwndRenderTarget( dxProp,
                                              HwndRenderTargetProperties( hWndParent, dsrcsize ),
                                              &pD2DTargetHWND );
    if ( hr == S_OK )
    {
        targetType = DTARGET_TYPE_HWND;
        return pD2DTargetHWND;
    }

    return NULL;
}

IDWriteFactory* initializeWriteFactory()
{
    if ( pWriteFactory != NULL )
        return pWriteFactory;

    HRESULT hr = E_FAIL;

    hr = DWriteCreateFactory( DWRITE_FACTORY_TYPE_SHARED,
                              __uuidof(IDWriteFactory),
                              reinterpret_cast<IUnknown**>(&pWriteFactory) );

    if ( hr == S_OK )
    {
        return pWriteFactory;
    }

    return NULL;
}

IWICImagingFactory* initializeImageFactory()
{
    if ( pWICImageFactory != NULL )
        return pWICImageFactory;

    CoInitialize( NULL );

    HRESULT hr = E_FAIL;

    hr = CoCreateInstance( CLSID_WICImagingFactory,
                           NULL,
                           CLSCTX_INPROC_SERVER,
                           IID_PPV_ARGS( &pWICImageFactory ) );

    if ( hr == S_OK )
    {
        return pWICImageFactory;
    }

    return NULL;
}

bool loadPNGtoBitmap( const TCHAR* pngfn, ID2D1Bitmap* &out )
{
    if ( pWICImageFactory == NULL )
        return false;

    IWICBitmapDecoder*      pImgLoader   = {NULL};
    HRESULT                 hr           = S_OK;

    hr = pWICImageFactory->CreateDecoderFromFilename( pngfn,
                                                      NULL,
                                                      GENERIC_READ,
                                                      WICDecodeMetadataCacheOnDemand,
                                                      &pImgLoader );
    if ( hr == S_OK )
    {
        IWICBitmapFrameDecode* pWICFdecoder = NULL;
        hr = pImgLoader->GetFrame( 0, &pWICFdecoder );

        if ( hr == S_OK )
        {
            IWICFormatConverter *pFMTconv = NULL;

            hr = pWICImageFactory->CreateFormatConverter( &pFMTconv );
            if ( hr == S_OK )
            {
                pFMTconv->Initialize( pWICFdecoder,
                                      GUID_WICPixelFormat32bppPBGRA,
                                      WICBitmapDitherTypeNone,
                                      NULL,
                                      0.0f,
                                      WICBitmapPaletteTypeCustom );

                switch( targetType )
                {
                    case DTARGET_TYPE_HWND:
                        hr = pD2DTargetHWND->CreateBitmapFromWicBitmap( pFMTconv, NULL, &out );
                        if ( hr == S_OK )
                        {
                            SafeRelease( &pImgLoader );
                            SafeRelease( &pFMTconv );
                            SafeRelease( &pWICFdecoder );
                            return true;
                        }
                        break;

                    case DTARGET_TYPE_HDC:
                        hr = pD2DTargetDC->CreateBitmapFromWicBitmap( pFMTconv, NULL, &out );
                        if ( hr == S_OK )
                        {
                            SafeRelease( &pImgLoader );
                            SafeRelease( &pFMTconv );
                            SafeRelease( &pWICFdecoder );
                            return true;
                        }
                        break;
                }
            }
        }

        SafeRelease( &pImgLoader );
    }

    return false;
}

bool createRGBBitmapRGBX32( const unsigned char* rgbx, int w, int h, ID2D1Bitmap* &out )
{
    if ( rgbx == NULL )
        return false;

    if ( ( w <= 0 ) || ( h <= 0 ) )
        return false;

    HRESULT hr = S_FALSE;
    D2D1_SIZE_U  bmpsz = SizeU(w, h);
    int bmppt = w * 4;

    D2D1_BITMAP_PROPERTIES bmpp;
    bmpp.dpiX = currentDPI_x;
    bmpp.dpiY = currentDPI_y;
    bmpp.pixelFormat.format    = DXGI_FORMAT_B8G8R8X8_UNORM;
    bmpp.pixelFormat.alphaMode = D2D1_ALPHA_MODE_IGNORE;

    switch( targetType )
    {
        case DTARGET_TYPE_HWND:
            hr = pD2DTargetHWND->CreateBitmap( bmpsz, (void*)rgbx, bmppt, &bmpp, &out );
            if ( hr == S_OK )
            {
                return true;
            }
            break;

        case DTARGET_TYPE_HDC:
            hr = pD2DTargetDC->CreateBitmap( bmpsz, (void*)rgbx, bmppt, &bmpp, &out );
            if ( hr == S_OK )
            {
                return true;
            }
            break;
    }

    return false;
}

bool createRGBBitmapRGBA32( const unsigned char* rgba, int w, int h, ID2D1Bitmap* &out )
{
    if ( rgba == NULL )
        return false;

    if ( ( w <= 0 ) || ( h <= 0 ) )
        return false;

    HRESULT hr = S_FALSE;
    D2D1_SIZE_U  bmpsz = SizeU(w, h);
    int bmppt = w * 4;

    D2D1_BITMAP_PROPERTIES bmpp;
    bmpp.dpiX = currentDPI_x;
    bmpp.dpiY = currentDPI_y;
    bmpp.pixelFormat.format    = DXGI_FORMAT_B8G8R8A8_UNORM;
    bmpp.pixelFormat.alphaMode = D2D1_ALPHA_MODE_PREMULTIPLIED;

    switch( targetType )
    {
        case DTARGET_TYPE_HWND:
            hr = pD2DTargetHWND->CreateBitmap( bmpsz, (void*)rgba, bmppt, &bmpp, &out );
            if ( hr == S_OK )
            {
                return true;
            }
            break;

        case DTARGET_TYPE_HDC:
            hr = pD2DTargetDC->CreateBitmap( bmpsz, (void*)rgba, bmppt, &bmpp, &out );
            if ( hr == S_OK )
            {
                return true;
            }
            break;
    }

    return false;
}


void setTextFormat( IDWriteTextFormat* tfmt )
{
    pTextFmtTemp = tfmt;
}

IDWriteTextFormat* getTextFormat()
{
    return pTextFmtTemp;
}

bool drawText(int x,int y,TCHAR *str,int r,int g,int b, float a)
{
    if ( ( pD2DTargetHWND == NULL ) && ( pD2DTargetDC == NULL ) )
        return false;

    if ( pTextFmtTemp == NULL )
        return false;

    ID2D1SolidColorBrush*   color       = NULL;

    float f_r = ( float(r) / 255.0f ) * 1.0f;
    float f_g = ( float(g) / 255.0f ) * 1.0f;
    float f_b = ( float(b) / 255.0f ) * 1.0f;

    D2D1_RECT_F rectf = { float(x) , float(y), 0.0f, 0.0f };

    switch( targetType )
    {
        case DTARGET_TYPE_HWND:
            {
                RECT rc;
                GetWindowRect( hWndParent, &rc );
                rectf.right = rc.right;
                rectf.bottom = rc.bottom;
                pD2DTargetHWND->CreateSolidColorBrush( ColorF( f_r, f_g, f_b, a ), &color );
                pD2DTargetHWND->DrawText( str,
                                          _tcslen( str ),
                                          pTextFmtTemp,
                                          rectf,
                                          color );
            }
            break;

        case DTARGET_TYPE_HDC:
            {
                rectf.right = GetDeviceCaps( hDCParent, HORZRES );
                rectf.bottom = GetDeviceCaps( hDCParent, VERTRES );
                pD2DTargetDC->CreateSolidColorBrush( ColorF( f_r, f_g, f_b, a ), &color );
                pD2DTargetDC->DrawText( str,
                                        _tcslen( str ),
                                        pTextFmtTemp,
                                        rectf,
                                        color );
            }
            break;

        default:
            return false;

    }

    SafeRelease( &color );

    return true;
}

IDWriteTextFormat* createTextFormat( const TCHAR* fontname, float size, const TCHAR* locale )
{
    if ( pWriteFactory == NULL )
        return NULL;

    IDWriteTextFormat* newFormat = NULL;

    HRESULT hr = E_FAIL;

    hr = pWriteFactory->CreateTextFormat( fontname,
                                          NULL,
                                          DWRITE_FONT_WEIGHT_REGULAR,
                                          DWRITE_FONT_STYLE_NORMAL,
                                          DWRITE_FONT_STRETCH_NORMAL,
                                          size,
                                          locale,
                                          &newFormat );

    if ( hr == S_OK )
    {
        return newFormat;
    }

    return NULL;
}

HRESULT CloneBitmap(ID2D1Bitmap* src, int width, int height, ID2D1Bitmap* &out)
{
    if ( pWICImageFactory == NULL )
        return false;

    if ( ( width == 0 ) || ( height == 0 ) )
        return false;

    HRESULT hr = S_FALSE;
    D2D_SIZE_U bmpsz = SizeU( width, height );
    D2D1_BITMAP_PROPERTIES bmpp;
    bmpp.dpiX = currentDPI_x;
    bmpp.dpiY = currentDPI_y;
    bmpp.pixelFormat.format    = DXGI_FORMAT_B8G8R8X8_UNORM;
    bmpp.pixelFormat.alphaMode = D2D1_ALPHA_MODE_IGNORE;
    int bmppt = width * 4;

    switch( targetType )
    {
        case DTARGET_TYPE_HWND:
            hr = pD2DTargetHWND->CreateBitmap( bmpsz, NULL, bmppt, bmpp, &out);
            break;

        case DTARGET_TYPE_HDC:
            hr = pD2DTargetDC->CreateBitmap( bmpsz, NULL, bmppt, bmpp, &out);
            break;
    }

    if ( hr == S_OK )
    {
        D2D1_POINT_2U pt = {0, 0};
        D2D1_RECT_U   rect = { 0, 0, width, height };

        //(const D2D1_POINT_2U *destPoint, ID2D1Bitmap *bitmap, const D2D1_RECT_U *srcRect) PURE;
        return out->CopyFromBitmap( &pt, src, &rect );
    }

    return hr;
}

void drawArc( ID2D1PathGeometry *pGeometry, D2D_POINT_2F ptCenterF, FLOAT fRotationAngle, FLOAT fSweepAngle, FLOAT fRadius,
              D2D1_FILL_MODE fillMode, D2D1_FIGURE_BEGIN figureBegin, D2D1_FIGURE_END figureEnd )
{
    if ( pGeometry == NULL )
        return;

    ID2D1GeometrySink *pSink = NULL;
    HRESULT hr = S_FALSE;

    D2D_POINT_2F ptStartF = {0.0f, 0.0f};
    D2D_POINT_2F ptEndF   = {0.0f, 0.0f};

    ptStartF  = CalcCartesianCoordinate(fRotationAngle, fRadius);
    ptStartF.x += ptCenterF.x;
    ptStartF.y += ptCenterF.y;

    ptEndF = CalcCartesianCoordinate(fRotationAngle + fSweepAngle, fRadius);
    ptEndF.x += ptCenterF.x;
    ptEndF.y += ptCenterF.y;

    hr = pGeometry->Open(&pSink);

    if (SUCCEEDED(hr))
    {
        pSink->SetFillMode(fillMode);
        pSink->BeginFigure(ptCenterF, figureBegin);
        pSink->AddLine(ptStartF);
        pSink->AddArc( D2D1::ArcSegment( ptEndF,
                                        D2D1::SizeF(fRadius, fRadius),
                                        0.0f,
                                        D2D1_SWEEP_DIRECTION_CLOCKWISE,
                                        (fSweepAngle > 180.0f) ? D2D1_ARC_SIZE_LARGE : D2D1_ARC_SIZE_SMALL ) );
        pSink->EndFigure(figureEnd);

        hr = pSink->Close();

        if (SUCCEEDED(hr))
        {
            SafeRelease(&pSink);
        }
    }
}

void drawPie( ID2D1PathGeometry *pGeometry, D2D_POINT_2F ptCenterF,
              FLOAT fRotationAngle, FLOAT fSweepAngle,
              FLOAT fOuterRadius, FLOAT fInnerRadius,
              D2D1_FILL_MODE fillMode, D2D1_FIGURE_BEGIN figureBegin, D2D1_FIGURE_END figureEnd )
{
    if ( pGeometry == NULL )
        return;

	ID2D1GeometrySink *pSink = NULL;

	HRESULT hr = S_FALSE;

	D2D_POINT_2F ptInStartF = {0.0f, 0.0f};
	D2D_POINT_2F ptInEndF   = {0.0f, 0.0f};

	D2D_POINT_2F ptOutStartF = {0.0f, 0.0f};
	D2D_POINT_2F ptOutEndF   = {0.0f, 0.0f};

	ptInStartF  = CalcCartesianCoordinate(fRotationAngle, fInnerRadius);
	ptInStartF.x += ptCenterF.x;
	ptInStartF.y += ptCenterF.y;

	ptInEndF = CalcCartesianCoordinate(fRotationAngle + fSweepAngle, fInnerRadius);
	ptInEndF.x += ptCenterF.x;
	ptInEndF.y += ptCenterF.y;

	ptOutStartF  = CalcCartesianCoordinate(fRotationAngle, fOuterRadius);
	ptOutStartF.x += ptCenterF.x;
	ptOutStartF.y += ptCenterF.y;

	ptOutEndF = CalcCartesianCoordinate(fRotationAngle + fSweepAngle, fOuterRadius);
	ptOutEndF.x += ptCenterF.x;
	ptOutEndF.y += ptCenterF.y;

	hr = pGeometry->Open(&pSink);

	if ( SUCCEEDED( hr ) )
	{
		pSink->SetFillMode(fillMode);

		pSink->BeginFigure( ptInStartF, figureBegin );
		pSink->AddLine( ptOutStartF );
		pSink->AddArc( D2D1::ArcSegment( ptOutEndF,
                                         D2D1::SizeF(fOuterRadius, fOuterRadius),
                                         0.0f,
                                         D2D1_SWEEP_DIRECTION_CLOCKWISE,
                                         (fSweepAngle > 180.0f) ? D2D1_ARC_SIZE_LARGE : D2D1_ARC_SIZE_SMALL ) );
		pSink->AddLine(ptInEndF);
		pSink->AddArc( D2D1::ArcSegment( ptInStartF,
                                         D2D1::SizeF(fInnerRadius, fInnerRadius),
                                         0.0f,
                                         D2D1_SWEEP_DIRECTION_COUNTER_CLOCKWISE,
                                         (fSweepAngle > 180.0f) ? D2D1_ARC_SIZE_LARGE : D2D1_ARC_SIZE_SMALL) );
		pSink->EndFigure(figureEnd);

		hr = pSink->Close();

		if ( SUCCEEDED( hr ) )
        {
            SafeRelease( &pSink );
        }
	}
}


////////////////////////////////////////////////////////////////////////////////

D2D_POINT_2F CalcCartesianCoordinate(FLOAT fAngle, FLOAT fRadius)
{
    double dblAngleRad = 0.0f;
	FLOAT x = 0.0f, y = 0.0f;

	dblAngleRad = (M_PI / 180.0f) * (fAngle - 90.0f);

    x = (FLOAT)(fRadius * cos(dblAngleRad));
    y = (FLOAT)(fRadius * sin(dblAngleRad));

    return D2D1::Point2F(x, y);
}
