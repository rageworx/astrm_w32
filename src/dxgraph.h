#ifndef __DXGRAPH_H__
#define __DXGRAPH_H__

#include <windows.h>
#include <tchar.h>
#include <D2D1.h>
#include <dwrite.h>
#include <wincodec.h>


ID2D1Factory*           initializeDFactory( HWND parent );
ID2D1DCRenderTarget*    initializeRenderTargetDC( HDC dc );
ID2D1HwndRenderTarget*  initializeRenderTargetHWND();
IDWriteFactory*         initializeWriteFactory();
IWICImagingFactory*     initializeImageFactory();

bool                loadPNGtoBitmap( const TCHAR* pngfn, ID2D1Bitmap* &out );
bool                createRGBBitmapRGBX32( const unsigned char* rgbx, int w, int h, ID2D1Bitmap* &out );
bool                createRGBBitmapRGBA32( const unsigned char* rgba, int w, int h, ID2D1Bitmap* &out );
void                setTextFormat( IDWriteTextFormat* tfmt );
IDWriteTextFormat*  getTextFormat();
bool                drawText(int x,int y,TCHAR *str,int r,int g,int b, float a = 1.0f);
IDWriteTextFormat*  createTextFormat( const TCHAR* fontname, float size, const TCHAR* locale = TEXT("en-us") );
HRESULT             CloneBitmap(ID2D1Bitmap* src, int width, int height, ID2D1Bitmap* &out);

void drawArc( ID2D1PathGeometry *pGeometry,
               D2D_POINT_2F ptCenterF, FLOAT fRotationAngle, FLOAT fSweepAngle, FLOAT fRadius,
               D2D1_FILL_MODE fillMode = D2D1_FILL_MODE_ALTERNATE,
               D2D1_FIGURE_BEGIN figureBegin = D2D1_FIGURE_BEGIN_FILLED,
               D2D1_FIGURE_END figureEnd = D2D1_FIGURE_END_CLOSED );

void drawPie( ID2D1PathGeometry *pGeometry,
              D2D_POINT_2F ptCenterF, FLOAT fRotationAngle, FLOAT fSweepAngle,
              FLOAT fOuterRadius, FLOAT fInnerRadius,
              D2D1_FILL_MODE fillMode = D2D1_FILL_MODE_WINDING,
              D2D1_FIGURE_BEGIN figureBegin = D2D1_FIGURE_BEGIN_FILLED,
              D2D1_FIGURE_END figureEnd = D2D1_FIGURE_END_CLOSED );

#endif /// of __DXGRAPH_H__
