#include "dxsound.h"
#include "saferelease.h"
#include "winwav.h"
#include "mp3wav.h"

#ifdef DEBUG
    #include <cstdio>
    #include <cstdlib>
#endif

using namespace D2D1;

////////////////////////////////////////////////////////////////////////////////

IDirectSound* pDSnd = NULL;

////////////////////////////////////////////////////////////////////////////////

bool allocDSound( IDirectSoundBuffer* &dsbuff, WinWave* ww, DSBUFFERDESC &dsdesc );
bool allocDSoundEx( IDirectSoundBuffer* &dsbuff, mp3wav* mp3w, DSBUFFERDESC &dsdesc );

////////////////////////////////////////////////////////////////////////////////

bool initializeDSound( HWND parent )
{
    if ( pDSnd != NULL )
        return true;

    HRESULT hr = E_FAIL;

    hr = DirectSoundCreate( NULL, &pDSnd, NULL );

    if ( hr == S_OK )
    {
        pDSnd->SetCooperativeLevel( parent, DSSCL_NORMAL );

        return true;
    }
#ifdef DEBUG
    else
    {
        printf("Error: Failed to create DXsound.\n");
    }
#endif
    return false;
}

void finalizeDSound()
{
    if ( pDSnd != NULL )
    {
        SafeRelease( &pDSnd );
    }
}

IDirectSound* getDSound()
{
    return pDSnd;
}

IDirectSoundBuffer* createDSBufferWAVE( const TCHAR* wavefname, DSBUFFERDESC &desc )
{
    if ( wavefname == NULL )
    {
        return NULL;
    }

    IDirectSoundBuffer* newDSBuffer = NULL;

    WinWave* newWave = new WinWave();

    if ( newWave != NULL )
    {
        if ( newWave->LoadWaveFile( wavefname ) == S_OK )
        {
            bool retb = allocDSound( newDSBuffer, newWave, desc );

            delete newWave;
            newWave = NULL;

            if ( retb == true )
            {
                return newDSBuffer;
            }
        }
    }

    return NULL;
}

IDirectSoundBuffer* createDSBufferMP3( const TCHAR* mp3fname, DSBUFFERDESC &desc )
{
    if ( mp3fname == NULL )
    {
        return NULL;
    }

    IDirectSoundBuffer* newDSBuffer = NULL;
    mp3wav* newmp3 = new mp3wav();

    if ( newmp3 != NULL )
    {
        if ( newmp3->OpenStream( mp3fname ) == true )
        {
            bool retb = allocDSoundEx( newDSBuffer, newmp3, desc );

            delete newmp3;
            newmp3 = NULL;

            if ( retb == true )
            {
                return newDSBuffer;
            }
        }
    }

    return NULL;
}

IDirectSoundBuffer* createDSBufferRaw( const char* ref, int chnls, int rate, int size, DSBUFFERDESC &dsdesc )
{
    if ( ref == NULL )
        return NULL;

    if ( ( chnls <= 0 ) || ( rate <= 0 ) || ( size <= 0 ) )
        return NULL;

    WAVEFORMATEX wavfmt = {0};

    wavfmt.cbSize = sizeof( WAVEFORMATEX );
    wavfmt.wFormatTag = WAVE_FORMAT_PCM;
    wavfmt.wBitsPerSample = 16;
    wavfmt.nChannels = chnls;
    wavfmt.nSamplesPerSec = rate;
    wavfmt.nBlockAlign = wavfmt.nChannels * wavfmt.wBitsPerSample / 8;
    wavfmt.nAvgBytesPerSec = wavfmt.nBlockAlign * wavfmt.nSamplesPerSec;

    ZeroMemory( &dsdesc, sizeof( DSBUFFERDESC ) );
    dsdesc.dwSize = sizeof( DSBUFFERDESC );
    dsdesc.dwFlags = DSBCAPS_STATIC | DSBCAPS_CTRLPOSITIONNOTIFY;
    dsdesc.dwBufferBytes = size;
    dsdesc.lpwfxFormat = &wavfmt;

    IDirectSoundBuffer* newDSBuffer = NULL;

    HRESULT hr = pDSnd->CreateSoundBuffer( &dsdesc, &newDSBuffer, NULL );

    if ( hr == S_OK )
    {
        void* pwData1 = NULL;
        void* pwData2 = NULL;
        DWORD lenData1 = 0;
        DWORD lenData2 = 0;

        newDSBuffer->Lock( 0,
                      dsdesc.dwBufferBytes,
                      &pwData1,
                      &lenData1,
                      &pwData2,
                      &lenData2,
                      0 );

        memcpy( pwData1, ref, dsdesc.dwBufferBytes );
        newDSBuffer->Unlock( pwData1, dsdesc.dwBufferBytes, NULL, 0 );

        return newDSBuffer;
    }

    return NULL;
}

bool FillDSound( IDirectSoundBuffer* &dsbuff, const char* ref, int chnls, int rate, int size, DSBUFFERDESC &dsdesc )
{
    if ( pDSnd == NULL )
        return false;

    WAVEFORMATEX wavfmt = {0};

    wavfmt.cbSize = sizeof( WAVEFORMATEX );
    wavfmt.wFormatTag = WAVE_FORMAT_PCM;
    wavfmt.wBitsPerSample = 16;
    wavfmt.nChannels = chnls;
    wavfmt.nSamplesPerSec = rate;
    wavfmt.nBlockAlign = wavfmt.nChannels * wavfmt.wBitsPerSample / 8;
    wavfmt.nAvgBytesPerSec = wavfmt.nBlockAlign * wavfmt.nSamplesPerSec;

    ZeroMemory( &dsdesc, sizeof( DSBUFFERDESC ) );
    dsdesc.dwSize = sizeof( DSBUFFERDESC );
    dsdesc.dwFlags = DSBCAPS_STATIC | DSBCAPS_CTRLPOSITIONNOTIFY;
    dsdesc.dwBufferBytes = size;
    dsdesc.lpwfxFormat = &wavfmt;

    HRESULT hr = pDSnd->CreateSoundBuffer( &dsdesc, &dsbuff, NULL );

    if ( hr == S_OK )
    {
        void* pwData1 = NULL;
        void* pwData2 = NULL;
        DWORD lenData1 = 0;
        DWORD lenData2 = 0;

        dsbuff->Lock( 0,
                      dsdesc.dwBufferBytes,
                      &pwData1,
                      &lenData1,
                      &pwData2,
                      &lenData2,
                      0 );

        memcpy( pwData1, ref, dsdesc.dwBufferBytes );
        dsbuff->Unlock( pwData1, dsdesc.dwBufferBytes, NULL, 0 );

        return true;
    }

    return false;
}


////////////////////////////////////////////////////////////////////////////////

bool allocDSound( IDirectSoundBuffer* &dsbuff, WinWave* ww, DSBUFFERDESC &dsdesc )
{
    if ( pDSnd == NULL )
        return false;

    if ( ww == NULL )
        return false;

    ZeroMemory( &dsdesc, sizeof( DSBUFFERDESC ) );
    dsdesc.dwSize = sizeof( DSBUFFERDESC );
    dsdesc.dwFlags = DSBCAPS_STATIC | DSBCAPS_CTRLPOSITIONNOTIFY;
    dsdesc.dwBufferBytes = ww->WaveSize();
    dsdesc.lpwfxFormat = ww->WaveFormat();

    HRESULT hr = pDSnd->CreateSoundBuffer( &dsdesc, &dsbuff, NULL );

    if ( hr == S_OK )
    {
        void* pwData1 = NULL;
        void* pwData2 = NULL;
        DWORD lenData1 = 0;
        DWORD lenData2 = 0;

        dsbuff->Lock( 0,
                      dsdesc.dwBufferBytes,
                      &pwData1,
                      &lenData1,
                      &pwData2,
                      &lenData2,
                      0 );

        memcpy( pwData1, ww->WaveData(), dsdesc.dwBufferBytes );
        dsbuff->Unlock( pwData1, dsdesc.dwBufferBytes, NULL, 0 );

        return true;
    }

    return false;
}


bool allocDSoundEx( IDirectSoundBuffer* &dsbuff, mp3wav* mp3w, DSBUFFERDESC &dsdesc )
{
    if ( pDSnd == NULL )
        return false;

    if ( mp3w == NULL )
        return false;

    unsigned char* mp3decodedbuff = NULL;

    int dcdsize = mp3w->WaveSize();

    if ( dcdsize <= 0 )
        return false;

    mp3decodedbuff = new unsigned char[ dcdsize ];

    if ( mp3decodedbuff == NULL )
        return false;

    memcpy( mp3decodedbuff, mp3w->WaveData(), dcdsize );

    ZeroMemory( &dsdesc, sizeof( DSBUFFERDESC ) );
    dsdesc.dwSize = sizeof( DSBUFFERDESC );
    dsdesc.dwFlags = DSBCAPS_STATIC | DSBCAPS_CTRLPOSITIONNOTIFY;
    dsdesc.dwBufferBytes = dcdsize;
    dsdesc.lpwfxFormat = mp3w->WaveFormat();

    HRESULT hr = pDSnd->CreateSoundBuffer( &dsdesc, &dsbuff, NULL );

    if ( hr == S_OK )
    {
        void* pwData1 = NULL;
        void* pwData2 = NULL;
        DWORD lenData1 = 0;
        DWORD lenData2 = 0;

        dsbuff->Lock( 0,
                      dsdesc.dwBufferBytes,
                      &pwData1,
                      &lenData1,
                      &pwData2,
                      &lenData2,
                      0 );

        memcpy( pwData1, mp3decodedbuff, dsdesc.dwBufferBytes );
        dsbuff->Unlock( pwData1, dsdesc.dwBufferBytes, NULL, 0 );

        delete[] mp3decodedbuff;

        return true;
    }

    return false;
}


////////////////////////////////////////////////////////////////////////////////

