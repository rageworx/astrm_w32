#ifndef __DXSOUND_H__
#define __DXSOUND_H__

#include <windows.h>
#include <tchar.h>
#include <D2D1.h>
#include <wincodec.h>
#include <mmsystem.h>
#include <dsound.h>

bool                initializeDSound( HWND parent );
void                finalizeDSound();

IDirectSound*       getDSound();
IDirectSoundBuffer* createDSBufferWAVE( const TCHAR* wavefname, DSBUFFERDESC &desc );
IDirectSoundBuffer* createDSBufferMP3( const TCHAR* mp3fname, DSBUFFERDESC &desc );

// Buffer tool ---
IDirectSoundBuffer* createDSBufferRaw( const char* ref, int chnls, int rate, int size, DSBUFFERDESC &dsdesc );
bool FillDSound( IDirectSoundBuffer* &dsbuff, const char* ref, int chnls, int rate, int size, DSBUFFERDESC &dsdesc );

#endif /// of __DXSOUND_H__
