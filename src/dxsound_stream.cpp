#include "dxsound_stream.h"

void CALLBACK dxss_timercall(UINT uTimerID, UINT uMsg, DWORD dwUser, DWORD dw1, DWORD dw2)
{
    DxAudioStream *pDSS = (DxAudioStream*)dwUser;

    if ( pDSS != NULL )
    {
        pDSS->TimerCall();
    }
}

DxAudioStream::DxAudioStream( audio_refill_cb cb, void* param )
 : dsound( NULL ),
   dsoundbuff( NULL ),
   pAudioBuff( NULL ),
   pCallBack( cb ),
   pParam( param ),
   alloced( false )
{
    dsound = getDSound();

    ZeroMemory(&wfex, sizeof(WAVEFORMATEX) );

    hBFillEvent[0] = CreateEvent(NULL, FALSE, FALSE, TEXT("dxaudio_stream_fillbuffer_notify0"));
    hBFillEvent[1] = CreateEvent(NULL, FALSE, FALSE, TEXT("dxaudio_stream_fillbuffer_notify1"));

    circles[0] = 0;
    circles[1] = 0;
}

DxAudioStream::~DxAudioStream()
{
    Stop();

    if ( pAudioBuff != NULL )
    {
        delete[] pAudioBuff;
        pAudioBuff = NULL;
    }
}

void DxAudioStream::SetFormat(int samplerate, int channels)
{
    if ( dsound == NULL )
        return;

    if ( alloced == true )
        return;

    wfex.cbSize         = sizeof( WAVEFORMATEX );
    wfex.wFormatTag     = WAVE_FORMAT_PCM;
    wfex.wBitsPerSample = 16;
    wfex.nChannels      = channels;
    wfex.nSamplesPerSec = samplerate;
    wfex.nBlockAlign    = wfex.nChannels * wfex.wBitsPerSample / 8;
    wfex.nAvgBytesPerSec = wfex.nBlockAlign * wfex.nSamplesPerSec;

    DSBUFFERDESC dsbd;
    ZeroMemory(&dsbd, sizeof(dsbd));
    dsbd.dwSize = sizeof(DSBUFFERDESC);
    dsbd.dwFlags = DSBCAPS_PRIMARYBUFFER;
    dsbd.dwBufferBytes = 0;
    dsbd.lpwfxFormat = NULL;

    HRESULT hr = S_FALSE;

	LPDIRECTSOUNDBUFFER lpDSB = NULL;

	// Primary buffer ...
	hr = dsound->CreateSoundBuffer( &dsbd, &lpDSB, NULL);
    if ( hr == S_OK )
    {
        hr = lpDSB->SetFormat( &wfex );

        if ( hr == S_OK )
        {
            dsbd.dwFlags = DSBCAPS_CTRLPOSITIONNOTIFY | DSBCAPS_GLOBALFOCUS;
            dsbd.dwBufferBytes = 2 * wfex.nAvgBytesPerSec;
            dsbd.lpwfxFormat = &wfex;

            hr = dsound->CreateSoundBuffer( &dsbd, &dsoundbuff, NULL );

            if ( hr == S_OK )
            {
                LPDIRECTSOUNDNOTIFY pDSBNotify;

                hr = dsound->QueryInterface( IID_IDirectSoundNotify8, (void**)&pDSBNotify );
                if ( hr == S_OK )
                {
                    DSBPOSITIONNOTIFY pPosNotify[2];
                    pPosNotify[0].dwOffset      = wfex.nAvgBytesPerSec / 2 - 1;
                    pPosNotify[1].dwOffset      = 3 * wfex.nAvgBytesPerSec / 2 - 1;
                    pPosNotify[0].hEventNotify  = hBFillEvent[0];
                    pPosNotify[1].hEventNotify  = hBFillEvent[1];

                    hr = pDSBNotify->SetNotificationPositions( 2, pPosNotify );

                    if ( hr == S_OK )
                    {
                        pAudioBuff = new unsigned char[ wfex.nAvgBytesPerSec + 1 ];
                        if ( pAudioBuff != NULL )
                        {
                            memset( pAudioBuff, 0, wfex.nAvgBytesPerSec + 1 );
                            alloced = true;
                        }
                    }
                }
            }
        }
    }
}

void DxAudioStream::Play()
{
    if ( alloced == false )
        return;

    if ( pCallBack == NULL )
        return;

    if ( circles[0] == 0 )
    {
        DWORD retSamples = 0;
        DWORD retBytes = 0;

        pCallBack( pAudioBuff, wfex.nSamplesPerSec, retSamples, pParam );

        if ( retSamples > 0 )
        {
            retBytes = retSamples * wfex.nBlockAlign;

            void* pAudio[2] = {NULL, NULL};
            DWORD lenAudio[2] = {0, 0};

            HRESULT hr = dsoundbuff->Lock( 0,
                                           wfex.nAvgBytesPerSec,
                                           &pAudio[0],
                                           &lenAudio[0],
                                           &pAudio[1],
                                           &lenAudio[1],
                                           0 );

            if ( hr == S_OK )
            {
                if ( pAudio[0] != NULL )
                {
                    memset( pAudio[0], 0, lenAudio[0] );
                    memcpy( pAudio[0], pAudioBuff, retBytes );
                }

                if ( pAudio[1] != NULL )
                {
                    memset( pAudio[1], 0, lenAudio[1] );
                    memcpy( pAudio[1], pAudioBuff + lenAudio[0], lenAudio[1] );
                }
            }

            dsoundbuff->Unlock( pAudio[0], lenAudio[1],  pAudio[1], lenAudio[1] );
        }
    }

    dsoundbuff->Play(0, 0, 0);

    win32timerID = timeSetEvent(300, 100, dxss_timercall, (DWORD)this, TIME_PERIODIC | TIME_CALLBACK_FUNCTION);
}

void DxAudioStream::Stop()
{
    if ( alloced == false )
        return;

    dsoundbuff->Stop();
    timeKillEvent( win32timerID );

    void* pAudio1 = NULL;
    DWORD lenAudio1 = 0;

    HRESULT hr = dsoundbuff->Lock( 0,
                                   0,
                                   &pAudio1,
                                   &lenAudio1,
                                   NULL,
                                   NULL,
                                   DSBLOCK_ENTIREBUFFER );
    if ( hr == S_OK )
    {
        memset( pAudio1, 0, lenAudio1 );
        dsoundbuff->Unlock( pAudio1, lenAudio1, NULL, NULL );
        dsoundbuff->SetCurrentPosition( 0 );

        ResetEvent( hBFillEvent[0] );
        ResetEvent( hBFillEvent[1] );

        circles[0] = 0;
        circles[1] = 0;
    }
}

DWORD DxAudioStream::GetPlayedSamples()
{
    if ( alloced == false )
        return 0;

    DWORD curPos = 0;
    DWORD curSample = 0;

    dsoundbuff->GetCurrentPosition( &curPos, NULL );
    curSample = curPos / wfex.nBlockAlign;

    DWORD retSample = 0;
    if ( circles[1] < 1 )
    {
        return curSample;
    }

    retSample = ( circles[1] - 1 ) * 2 * wfex.nSamplesPerSec * 3 * wfex.nSamplesPerSec / 2;

    if ( retSample > ( 3 * wfex.nSamplesPerSec / 2 ) )
    {
        if ( circles[1] < circles[0] )
        {
            retSample = ( circles[0] - 1 ) * 2 * wfex.nSamplesPerSec + 3 * wfex.nSamplesPerSec / 2;
        }

        retSample += curSample - 3 * wfex.nSamplesPerSec / 2 + 1;
    }
    else
    {
        retSample += curSample + wfex.nSamplesPerSec / 2;
    }

    return retSample;
}

void DxAudioStream::TimerCall()
{
    if ( pCallBack == NULL )
        return;

    void* pAudio[2] = { NULL, NULL };
    DWORD lenAudio[2] = { 0, 0 };
    DWORD retSamples = 0;
    DWORD retBytes = 0;

    HRESULT hr = WaitForMultipleObjects( 2, hBFillEvent, FALSE, 0 );
    if ( hr == WAIT_OBJECT_0 )
    {
        circles[0]++;

        hr = dsoundbuff->Lock( wfex.nAvgBytesPerSec,
                               wfex.nAvgBytesPerSec,
                               &pAudio[0],
                               &lenAudio[0],
                               &pAudio[1],
                               &lenAudio[1],
                               0 );
        if ( hr != S_OK )
            return;
    }
    else
    if ( hr == WAIT_OBJECT_0 + 1 )
    {
        circles[1]++;
        hr = dsoundbuff->Lock( 0,
                               wfex.nAvgBytesPerSec,
                               &pAudio[0],
                               &lenAudio[0],
                               &pAudio[1],
                               &lenAudio[1],
                               0 );
        if ( hr != S_OK )
            return;
    }
    else
    {
        return;
    }

    pCallBack( pAudioBuff, wfex.nSamplesPerSec, retSamples, pParam );

    if ( retSamples > 0 ) /// Auto fill silence.
    {
        retBytes = retSamples * wfex.nBlockAlign;

        if ( retSamples < wfex.nSamplesPerSec )
        {
            DWORD tmpBytes = retSamples * wfex.nBlockAlign;
            memset( pAudioBuff + tmpBytes, 0, wfex.nAvgBytesPerSec - tmpBytes );
        }

        memcpy( pAudio[0], pAudioBuff, lenAudio[0] );

        if ( pAudio[1] != NULL )
        {
            memcpy( pAudio[1], pAudioBuff + lenAudio[0], lenAudio[1] );
        }

        dsoundbuff->Unlock( pAudio[0], lenAudio[0], pAudio[1], lenAudio[1] );
    }
}
