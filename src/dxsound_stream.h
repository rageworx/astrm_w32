#ifndef __DXSOUND_STREAM_H__
#define __DXSOUND_STREAM_H__

#include "dxsound.h"

typedef void (*audio_refill_cb)( unsigned char* abuff, const DWORD reqSamples, DWORD &retSamples, void* p );

class DxAudioStream
{
    public:
        DxAudioStream( audio_refill_cb cb, void* param );
        ~DxAudioStream();

    public:
        void  SetFormat(int samplerate, int channels);
        void  Play();
        void  Stop();
        DWORD GetPlayedSamples();

    public: /// Outter timer callback,
        void  TimerCall();

    protected:
        WAVEFORMATEX        wfex;
        IDirectSound*       dsound;
        IDirectSoundBuffer* dsoundbuff;
        HANDLE              hBFillEvent[2]; /// double buffering.

    protected:
        unsigned char*      pAudioBuff;
        audio_refill_cb     pCallBack;
        void*               pParam;

    protected:
        MMRESULT            win32timerID;
        DWORD               circles[2];
        int                 db;
        bool                alloced;

};

#endif /// of __DXSOUND_STREAM_H__
