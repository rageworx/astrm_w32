#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>

#include "ffprtsp.h"
#include "dxgraph.h"
#include "dxsound.h"
#include "saferelease.h"

////////////////////////////////////////////////////////////////////////////////

#define AVCODEC_MAX_AUDIO_FRAME_SIZE    192000
#define DEF_DAUDI_BUFF_MIN_SIZE         4096
#define DEF_CACHED_AUDIO_BUFF_SIZE      DEF_DAUDI_BUFF_MIN_SIZE * 20
#define DEF_DAUDI_PLAY_THRESHOLD        DEF_DAUDI_BUFF_MIN_SIZE * 10
#define AUDIO_DIFF_AVG_NB               20
#define SAMPLE_CORRECTION_PERCENT_MAX   10

#define DEF_AUD_RESAMPLE_CHANNELS       2
#define DEF_AUD_RESAMPLE_FREQUENCY      44100
#define DEF_AUD_RESAMPLE_CHNLAYOUT      AV_CH_LAYOUT_STEREO
#define DEF_AUD_RESAMPLE_FORMAT         AV_SAMPLE_FMT_S16

////////////////////////////////////////////////////////////////////////////////

HANDLE hEventFillBuffer = INVALID_HANDLE_VALUE;

static ffplayerRTSP*    rtspplayer = NULL;

AVDictionary*   filter_codec_opts( AVDictionary *opts, enum AVCodecID codec_id, AVFormatContext *s, AVStream *st, AVCodec *codec );
AVDictionary**  setup_find_stream_info_opts(AVFormatContext *s, AVDictionary *codec_opts);
int             check_stream_specifier( AVFormatContext *s, AVStream *st, const char *spec );
DWORD WINAPI    ffplaymodcb_win32( LPVOID p );

////////////////////////////////////////////////////////////////////////////////

ffplayerRTSP::ffplayerRTSP()
 : firstinit( true ),
   audioBuffSize( DEF_DAUDI_BUFF_MIN_SIZE ),
   contentTitle( NULL ),
   lockVidBuff( false ),
   lockAudBuff( false ),
   lastAVreturn( -1 ),
   streamCtx( NULL ),
   format_ops( NULL ),
   codec_opts( NULL ),
   resample_opts( NULL ),
   threadkillflag( false ),
   rdft( NULL ),
   rdft_data( NULL ),
   img_convert_ctx( NULL ),
   frameSRC( NULL ),
   frameConv( NULL ),
   dsound( NULL ),
   dsoundbuff( NULL ),
   audio_buff_cache( NULL ),
   swr_audio( NULL ),
   reconfigAudio( false ),
   streamopened( false )
{
    if ( rtspplayer != NULL )
    {
        delete rtspplayer;
    }

    rtspplayer = this;

    ffmpeg_init();
    reset_me();
}

ffplayerRTSP::~ffplayerRTSP()
{
    Stop();

    for( int cnt=0; cnt<DEF_VID_RENDER_BUFFERS; cnt++ )
    {
        if ( videoRawBuffer[cnt].buffer != NULL )
        {
            delete[] videoRawBuffer[cnt].buffer;
            videoRawBuffer[cnt].buffer = NULL;
        }
    }

    if ( dsoundbuff != NULL )
    {
        SafeRelease( &dsoundbuff );
    }

    if ( audio_buff_cache != NULL )
    {
        delete[] audio_buff_cache;
        audio_buff_cache = NULL;
    }

    if ( audio_buff1 != NULL )
    {
        delete[] audio_buff1;
        audio_buff1 = NULL;
    }

    sws_freeContext(sws_opts);
    sws_opts = NULL;

    av_dict_free(&swr_opts);
    av_dict_free(&format_ops);
    av_dict_free(&codec_opts);
    av_dict_free(&resample_opts);

    avformat_network_deinit();

    rtspplayer = NULL;
}

void ffplayerRTSP::ffmpeg_init()
{
    if ( firstinit == true )
    {
        firstinit = false;
#if CONFIG_AVDEVICE
        avdevice_register_all();
#endif
        avfilter_register_all();
        av_register_all();
        avformat_network_init();
    }

    if ( sws_opts == NULL )
    {
        sws_opts = sws_getContext( 16,
                                   16,
                                   (AVPixelFormat)0,
                                   16,
                                   16,
                                   (AVPixelFormat)0,
                                   SWS_BICUBIC,
                                   NULL,
                                   NULL,
                                   NULL );
    }

    if ( streamCtx == NULL )
    {
        streamCtx = avformat_alloc_context();
    }
}

void ffplayerRTSP::reset_me()
{
    if ( dsound == NULL )
    {
        dsound = getDSound();
    }

    for( int cnt=0; cnt<DEF_VID_RENDER_BUFFERS; cnt++ )
    {
        videoRawBuffer[cnt].buffer = NULL;
        videoRawBuffer[cnt].size = 0;
        videoRawBuffer[cnt].lock = -1;
    }

    videoRawQue = 0;
    videoRawQue_r = 0;

    for( int cnt=0; cnt<AVMEDIA_TYPE_NB; cnt++)
    {
        streamIndex[cnt] = -1;
    }

    checksVideoBuff = false;
    checksAudioBuff = false;

    if ( audio_buff_cache == NULL )
    {
        audio_buff_cache = new unsigned char[ DEF_CACHED_AUDIO_BUFF_SIZE ];

        if ( audio_buff_cache != NULL )
        {
            memset( audio_buff_cache, 0, DEF_CACHED_AUDIO_BUFF_SIZE );
        }
    }

    audio_buff_que_w = 0;

    threadkillflag = false;
}

bool ffplayerRTSP::OpenURL( const char* url )
{
    if ( streamopened == true )
    {
#ifdef DEBUG
        printf("\n\n#FFRTSP::ERROR -> duplicated open !\n\n");
#endif
        return false;
    }

    streamopened = true;

    if ( streamCtx == NULL )
    {
        // in case of streamCtx NULL, maybe reopen.
        ffmpeg_init();
        reset_me();

        if ( streamCtx == NULL )
        {
            streamopened = false;
            return false;
        }
    }

    lastAVreturn = avformat_open_input( (AVFormatContext**)&streamCtx,
                                        url,
                                        streamFormat,
                                        (AVDictionary**)&format_ops );

    if ( lastAVreturn < 0 )
    {
        streamopened = false;
        return false;
    }

    AVDictionaryEntry* t = av_dict_get( format_ops, "", NULL, AV_DICT_IGNORE_SUFFIX );
    AVDictionary** opts  = setup_find_stream_info_opts( streamCtx, codec_opts );

    lastAVreturn = avformat_find_stream_info( streamCtx, opts );
    if ( lastAVreturn < 0 )
    {
        avformat_close_input( &streamCtx );
        streamCtx = NULL;
        streamopened = false;
        return false;
    }

    for( int cnt=0; cnt< streamCtx->nb_streams; cnt++ )
    {
        av_dict_free( &opts[cnt] );
    }

    av_freep( &opts );

    if ( streamCtx->pb != NULL )
    {
        streamCtx->pb->eof_reached = 0;
    }

    maxDuration = ( streamCtx->iformat->flags & AVFMT_TS_DISCONT ) ? 10.0 : 3600.0;

    if ( !contentTitle && (t = av_dict_get(streamCtx->metadata, "title", NULL, 0)) )
    {
        contentTitle = av_asprintf("%s - %s", t->value, url );
    }

    streamIndex[AVMEDIA_TYPE_VIDEO] = av_find_best_stream( streamCtx,
                                                           AVMEDIA_TYPE_VIDEO,
                                                           -1,
                                                           -1,
                                                           NULL,
                                                           0 );


    streamIndex[AVMEDIA_TYPE_AUDIO] = av_find_best_stream( streamCtx,
                                                           AVMEDIA_TYPE_AUDIO,
                                                           -1,
                                                           streamIndex[AVMEDIA_TYPE_VIDEO],
                                                           NULL,
                                                           0 );


    if ( streamIndex[AVMEDIA_TYPE_VIDEO] >= 0 )
    {
        AVStream* st = streamCtx->streams[ streamIndex[AVMEDIA_TYPE_VIDEO] ];
        AVCodecContext* avctx = st->codec;

        vidWidth  = avctx->width;
        vidHeight = avctx->height;
        av_sar    = av_guess_sample_aspect_ratio( streamCtx, st, NULL );
    }

    if ( streamIndex[AVMEDIA_TYPE_AUDIO] >= 0 )
    {
        lastAVreturn = stream_component_open( streamCtx, streamIndex[AVMEDIA_TYPE_AUDIO] );
        if ( lastAVreturn < 0 )
        {
#ifdef DEBUG
            printf("Error: Finding codec failed for audio.\n");
#endif
            streamopened = false;
            return false;
        }
    }

    if ( streamIndex[AVMEDIA_TYPE_VIDEO] >= 0 )
    {
        lastAVreturn = stream_component_open( streamCtx, streamIndex[AVMEDIA_TYPE_VIDEO] );
        if ( lastAVreturn < 0 )
        {
#ifdef DEBUG
            printf("Error: Finding codec failed for video.\n");
#endif
            streamopened = false;
            return false;
        }
    }

    if ( video_stream_index < 0 )
    {
        if ( audio_stream_index >= 0 )
        {
            stream_component_close( streamCtx, audio_stream_index );
        }

        if ( video_stream_index >= 0 )
        {
            stream_component_close( streamCtx, video_stream_index );
        }

        if ( streamCtx != NULL )
        {
            avformat_close_input( &streamCtx );
            streamCtx = NULL;
        }

        streamopened = false;

        return false;
    }

    // Maybe it need ?
    if ( hThreadRead != INVALID_HANDLE_VALUE )
    {
        threadkillflag = true;
        WaitForSingleObject( hThreadRead, INFINITE );
        CloseHandle( hThreadRead );
        hThreadRead = INVALID_HANDLE_VALUE;
        threadkillflag = false;
    }

    hThreadRead = CreateThread( NULL, 0, ffplaymodcb_win32, this, 0, NULL );

    if ( hThreadRead == INVALID_HANDLE_VALUE )
    {
        streamopened = false;
        return false;
    }

    return true;
}

void ffplayerRTSP::Stop()
{
    if ( ( threadkillflag == true ) || ( streamopened == false ) )
        return;

    threadkillflag = true;

    if ( dsoundbuff != NULL )
    {
        dsoundbuff->Stop();
        SafeRelease( &dsoundbuff );
    }

    if ( hThreadRead != INVALID_HANDLE_VALUE )
    {
        // Wait for thread ends...
        WaitForSingleObject( hThreadRead, INFINITE );
        CloseHandle( hThreadRead );
        hThreadRead = INVALID_HANDLE_VALUE;
    }

    lockVidBuff = false;
    lockAudBuff = false;

    if ( streamCtx != NULL )
    {
        stream_component_close( streamCtx, streamIndex[AVMEDIA_TYPE_AUDIO] );
        stream_component_close( streamCtx, streamIndex[AVMEDIA_TYPE_VIDEO] );

        if ( streamCtx->pb != NULL )
        {
            avio_close( streamCtx->pb );
        }

        avformat_close_input( (AVFormatContext**)&streamCtx );
        avformat_free_context( streamCtx );
        streamCtx = NULL;
    }

    av_packet_unref( &avpacket );

    if ( swr_audio != NULL )
    {
        swr_free( &swr_audio );
        swr_audio = NULL;
    }

    if ( img_convert_ctx != NULL )
    {
        sws_freeContext( img_convert_ctx );
        img_convert_ctx = NULL;
    }

    if ( frameConv != NULL )
    {
        //avcodec_free_frame( &frameConv );
        av_frame_unref( frameConv );
        frameConv = NULL;
    }

    if ( frameSRC != NULL )
    {
        //avcodec_free_frame( &frameSRC );
        av_frame_unref( frameSRC );
        frameSRC = NULL;
    }

    for( int cnt=0; cnt<DEF_VID_RENDER_BUFFERS; cnt++ )
    {
        if ( videoRawBuffer[cnt].buffer != NULL )
        {
            videoRawBuffer[cnt].lock = -1;
            delete[] videoRawBuffer[cnt].buffer;
            videoRawBuffer[cnt].buffer = NULL;
            videoRawBuffer[cnt].size = 0;
        }
    }

    streamopened = false;
}

bool ffplayerRTSP::TestConnection( const char* ip, unsigned wsec )
{
    if ( wsec == 0 )
        return false;

    SOCKET      hSockTest = NULL;
    SOCKADDR_IN addrSvr;
    ULONG       flag = 1;
    fd_set      fdset;
    struct
    timeval     tv;

    hSockTest = socket( AF_INET, SOCK_STREAM, IPPROTO_TCP );

    if ( hSockTest > 0 )
    {
        bool sockConn = false;

        setsockopt( hSockTest, SOL_SOCKET, SO_REUSEADDR, (char*)&flag, sizeof(int) );

        // Set to non-block.
        ioctlsocket( hSockTest, FIONBIO, &flag );

        FD_ZERO( &fdset );
        FD_SET( hSockTest, &fdset );
        tv.tv_sec  = wsec;
        tv.tv_usec = 0;

        memset( &addrSvr, 0, sizeof(SOCKADDR_IN) );

        addrSvr.sin_family = AF_INET;
        addrSvr.sin_addr.s_addr = inet_addr( ip );
        addrSvr.sin_port = htons( 554 );    /// RTSP port is 554 (TCP)

        int reti = connect( hSockTest, (SOCKADDR*)&addrSvr, sizeof(addrSvr) );

        if ( reti == SOCKET_ERROR )
        {
            int sop = 0;
            socklen_t soplen = sizeof( sop );

            int ret = getsockopt( hSockTest,
                                  SOL_SOCKET,
                                  SO_ERROR,
                                  (char*)&sop,
                                  &soplen );
            if ( ret >= 0 )
            {
                if ( sop == 0 )
                {
                    sockConn = true;
                }
            }
        }
        else
        {
            sockConn = true;
        }

        if ( sockConn == true )
        {
            select( hSockTest + 1, NULL, &fdset, NULL, &tv );

            int retset = FD_ISSET( hSockTest, &fdset );
#ifdef DEBUG
            printf(" FD_ISSET returned %d\n", retset );
#endif
            if (  retset == 0 )
            {
                closesocket( hSockTest );
                return false;
            }

        }

        closesocket( hSockTest );

        return true;
    }

    return false;
}

ID2D1Bitmap* ffplayerRTSP::getRenderBuffer()
{
    // Fixed a bug in stopping stream listen.
    if ( threadkillflag == true )
        return NULL;

    if ( videoRawQue == videoRawQue_r )
    {
        videoRawQue_r = videoRawQue - 1;
        if ( videoRawQue_r < 0 )
        {
            videoRawQue_r = DEF_VID_RENDER_BUFFERS - 1;
        }
    }

    unsigned char* refBuff = videoRawBuffer[videoRawQue_r].buffer;

    if ( refBuff != NULL )
    {
        if ( videoRawBuffer[videoRawQue_r].lock == 0 )
        {
            ID2D1Bitmap* newDBmp = NULL;

            if ( interlaced == true )
            {
                // need buffer be muxed.
                int secondscene_y = vidHeight / 2;
                int buffmaxsz     = videoRawBuffer[videoRawQue_r].size;

                unsigned char* newbuff = new unsigned char[ buffmaxsz ];
                if ( newbuff != NULL )
                {
                    //int bufflinesz = avpicture_get_size( AV_PIX_FMT_BGR32, vidWidth, 1 );
                    int bufflinesz = vidWidth * 4;
                    int buffque_w  = 0;
                    int buffque_o  = 0;
                    int buffque_e  = secondscene_y * bufflinesz;

                    for( int cnt=0; cnt<vidHeight; cnt++ )
                    {
                        if ( cnt % 2 == 0 )
                        {
                            memcpy( &newbuff[buffque_w], &refBuff[buffque_o], bufflinesz );
                            buffque_o += bufflinesz;
                        }
                        else
                        {
                            memcpy( &newbuff[buffque_w], &refBuff[buffque_e], bufflinesz );
                            buffque_e += bufflinesz;
                        }

                        buffque_w += bufflinesz;

                        if ( buffque_w >= buffmaxsz )
                        {
                            break;
                        }
                    }

                    if ( createRGBBitmapRGBX32( newbuff , vidWidth, vidHeight, newDBmp ) == true )
                    {
                        lockVidBuff = false;

                        videoRawQue_r++;

                        if ( videoRawQue_r >= DEF_VID_RENDER_BUFFERS )
                        {
                            videoRawQue_r = 0;
                        }

                        delete[] newbuff;

                        return newDBmp;
                    }
                    else
                    {
                        if ( newbuff != NULL )
                        {
                            delete[] newbuff;
                        }
                    }
                }
            }
            else
            {
                if ( createRGBBitmapRGBX32( refBuff , vidWidth, vidHeight, newDBmp ) == true )
                {
                    lockVidBuff = false;

                    videoRawQue_r++;

                    if ( videoRawQue_r >= DEF_VID_RENDER_BUFFERS )
                    {
                        videoRawQue_r = 0;
                    }

                    return newDBmp;
                }
            }
        }
    }

    return NULL;
}

int ffplayerRTSP::GetVideoWidth()
{
    return vidWidth;
}

int ffplayerRTSP::GetVideoHeight()
{
    return vidHeight;
}

void ffplayerRTSP::SetReconfigAudio()
{
    reconfigAudio = true;
}

void ffplayerRTSP::SetInterlaced( bool m )
{
    if ( interlaced != m )
    {
        interlaced = m;
    }
}

void ffplayerRTSP::decode_video( AVPacket* pkt )
{
    // Checks abnormal memory references ....
    if ( pkt == NULL )
        return;

    if ( video_strm == NULL )
    {
        //av_free_packet( pkt );
        av_packet_unref( pkt );
        return;
    }

    if ( video_strm->codec == NULL )
    {
        //av_free_packet( pkt );
        av_packet_unref( pkt );
        return;
    }
    // --------------------------------------

    // read video ...
    if ( videoRawBuffer[videoRawQue].buffer == NULL )
    {
        int video_conv_size = avpicture_get_size( AV_PIX_FMT_BGR32, vidWidth, vidHeight );

        if ( video_conv_size > 0 )
        {
            videoRawBuffer[videoRawQue].buffer = new unsigned char[ video_conv_size ];
            videoRawBuffer[videoRawQue].size   = video_conv_size;
            videoRawBuffer[videoRawQue].lock   = 0;

            if ( videoRawBuffer[videoRawQue].buffer == NULL )
            {
                videoRawBuffer[videoRawQue].size = 0;
                videoRawBuffer[videoRawQue].lock = 0;

                //av_free_packet( pkt );
                av_packet_unref( pkt );
                return;
            }
        }
    }

    int got_picture;

    try
    {
        lastAVreturn = avcodec_decode_video2( video_strm->codec, frameSRC, &got_picture, pkt );
    }
    catch( ... )
    {
        //av_free_packet( pkt );
        av_packet_unref( pkt );
        return;
    }

    if ( got_picture > 0 )
    {
        if ( frameConv == NULL )
        {
            //frameConv = avcodec_alloc_frame();
            frameConv = av_frame_alloc();
        }

        videoRawBuffer[videoRawQue].lock = 1;

        avpicture_fill( (AVPicture*)frameConv,
                        videoRawBuffer[videoRawQue].buffer,
                        AV_PIX_FMT_RGB32,
                        vidWidth,
                        vidHeight);

        if ( img_convert_ctx == NULL )
        {
            img_convert_ctx = sws_getCachedContext( img_convert_ctx,
                                                    vidWidth,
                                                    vidHeight,
                                                    (AVPixelFormat)frameSRC->format,
                                                    vidWidth,
                                                    vidHeight,
                                                    AV_PIX_FMT_RGB32,
                                                    SWS_BICUBIC,
                                                    NULL,
                                                    NULL,
                                                    NULL);
        }

        if ( img_convert_ctx != NULL )
        {
            lastAVreturn = sws_scale( img_convert_ctx,
                                      frameSRC->data,
                                      frameSRC->linesize,
                                      0,
                                      vidHeight,
                                      frameConv->data,
                                      frameConv->linesize );

            if ( lastAVreturn >= 0 )
            {
                memcpy( videoRawBuffer[videoRawQue].buffer,
                        frameConv->data[0],
                        videoRawBuffer[videoRawQue].size );

                videoRawBuffer[videoRawQue].lock = 0;
                videoRawQue ++;

                if ( videoRawQue >= DEF_VID_RENDER_BUFFERS )
                {
                    videoRawQue = 0;
                }
            }
#ifdef DEBUG
            else
            {
                printf("Failed to sws_scale()\n");
            }
#endif
        }

        //avcodec_free_frame( &frameConv );
        av_frame_unref( frameConv );
        frameConv = NULL;
    }

    av_frame_unref( frameSRC );
    //av_free_packet( pkt );
    av_packet_unref( pkt );
}

void ffplayerRTSP::decode_audio( AVPacket* pkt )
{
    if ( pkt == NULL )
        return;

    // NULL check ----
    if ( audio_strm == NULL )
    {
        //av_free_packet( pkt );
        av_packet_unref( pkt );
        return;
    }
    else
    if ( audio_strm->codec == NULL )
    {
        //av_free_packet( pkt );
        av_packet_unref( pkt );
        return;
    }
    // NULL check ----

    if ( audio_buff1 == NULL )
    {
        audio_buff1_size = AVCODEC_MAX_AUDIO_FRAME_SIZE + FF_INPUT_BUFFER_PADDING_SIZE;
        audio_buff1 = new unsigned char[ audio_buff1_size ];

        if ( audio_buff1 == NULL )
        {
            audio_buff1_size = 0;

            //av_free_packet( pkt );
            av_packet_unref( pkt );
            return;
        }
    }

    AVCodecContext* ctx = audio_strm->codec;
    int got_audio = 0;

    int audioLen  = avcodec_decode_audio4( ctx, frameSRC, &got_audio, pkt );

    if ( ( audioLen > 0 ) && ( got_audio > 0 ) )
    {
#ifdef AUDIO_DEBUG
        static int audio_flip_count = 0;
#endif
        static int rem_ch_lout = 0;
        static int rem_chs     = 0;
        static int rem_srate   = 0;
        static int rem_sfmt    = 0;

        // Check re-audio config
        if ( reconfigAudio == true )
        {
            reconfigAudio = false;
            if ( swr_audio != NULL )
            {
                swr_free( &swr_audio );
                swr_audio = NULL;
            }
        }

        // Check source is different.
        if ( ( rem_ch_lout != ctx->channel_layout ) ||
             ( rem_chs     != ctx->channels ) ||
             ( rem_sfmt    != ctx->sample_fmt ) )
        {
#ifdef AUDIO_DEBUG
            printf(">>> new convert audio context!\n");
#endif
            if ( swr_audio != NULL )
            {
                swr_free( &swr_audio );
                swr_audio = NULL;
            }
        }

        if ( swr_audio == NULL )
        {
            swr_audio = swr_alloc();

            rem_ch_lout = ctx->channel_layout;
            rem_chs     = ctx->channels;
            rem_srate   = ctx->sample_rate;
            rem_sfmt    = ctx->sample_fmt;

            av_opt_set_int(swr_audio, "in_channel_layout",     ctx->channel_layout, 0);
            av_opt_set_int(swr_audio, "out_channel_layout",    DEF_AUD_RESAMPLE_CHNLAYOUT,  0);

            av_opt_set_int(swr_audio, "in_channes",            ctx->channels, 0 );
            av_opt_set_int(swr_audio, "out_channels",          DEF_AUD_RESAMPLE_CHANNELS, 0 );

            av_opt_set_int(swr_audio, "in_sample_rate",        ctx->sample_rate, 0);
            av_opt_set_int(swr_audio, "out_sample_rate",       DEF_AUD_RESAMPLE_FREQUENCY, 0);

            av_opt_set_sample_fmt(swr_audio, "in_sample_fmt",  ctx->sample_fmt, 0);
            av_opt_set_sample_fmt(swr_audio, "out_sample_fmt", DEF_AUD_RESAMPLE_FORMAT,  0);
        }

        swr_init( swr_audio );

        int real_len = frameSRC->nb_samples;

        int retConv = swr_convert( swr_audio,
                                   (uint8_t**)&audio_buff1,
                                   real_len,
                                   (const uint8_t **)frameSRC->extended_data,
                                   frameSRC->nb_samples );

        if ( retConv > 0 )
        {
            int plane_size = 0;
            int lenConv = av_samples_get_buffer_size( &plane_size,
                                                      DEF_AUD_RESAMPLE_CHANNELS,
                                                      retConv,
                                                      DEF_AUD_RESAMPLE_FORMAT,
                                                      1 );

#ifdef AUDIO_DEBUG
            printf( " plane_size = %d , lenConv = %d , audio_flip = %d     \r",
                    plane_size, lenConv, audio_flip_count );
#endif
            // Let make audio buffer for just little bit be buffered for smooth play.
            memcpy( &audio_buff_cache[ audio_buff_que_w ], audio_buff1, lenConv );
            audio_buff_que_w += lenConv;

            // Check audio cached buffer over limit.
            if ( audio_buff_que_w > DEF_DAUDI_PLAY_THRESHOLD )
            {
                IDirectSoundBuffer* beremovedbuffer =  NULL;

                if ( dsoundbuff != NULL )
                {
                    beremovedbuffer = dsoundbuff;
                }

                DSBUFFERDESC tmpDSC;

#ifndef AUDIOD_DECODE_AFTER_EFFECT
                dsoundbuff = createDSBufferRaw( (char*)audio_buff_cache,
                                                DEF_AUD_RESAMPLE_CHANNELS,
                                                DEF_AUD_RESAMPLE_FREQUENCY,
                                                audio_buff_que_w,
                                                tmpDSC );

#ifdef AUDIO_PCM_DEBUG
                FILE* fp = fopen( "test.pcm", "a+" );

                if ( fp!= NULL )
                {
                    fseek( fp, 0, SEEK_END );
                    fwrite( audio_buff_cache, 1, audio_buff_que_w, fp );
                    fclose( fp );
                }
#endif /// of AUDIO_PCM_DEBUG
#else /// of AUDIOD_DECODE_AFTER_EFFECT
                // Make buffer length little bit larger for make it anti clickering..
                int tailbuffsz = audio_buff_que_w + ( retConv * 2 );
                if ( tailbuffsz >= DEF_CACHED_AUDIO_BUFF_SIZE )
                {
                    tailbuffsz = DEF_CACHED_AUDIO_BUFF_SIZE;
                }
                else
                {
                    memcpy( &audio_buff_cache[audio_buff_que_w],
                            &audio_buff_cache[audio_buff_que_w - retConv * 2 ],
                            retConv * 2 );
                }

                dsoundbuff = createDSBufferRaw( (char*)audio_buff_cache,
                                                DEF_AUD_RESAMPLE_CHANNELS,
                                                DEF_AUD_RESAMPLE_FREQUENCY,
                                                tailbuffsz,
                                                tmpDSC );
#endif /// of AUDIOD_DECODE_AFTER_EFFECT

#ifdef AUDIO_DEBUG
                audio_flip_count++;
#endif
                if ( dsoundbuff != NULL )
                {
                    dsoundbuff->Play(0, 0, 0);

                    memset( audio_buff_cache, 0, DEF_CACHED_AUDIO_BUFF_SIZE );
                    audio_buff_que_w = 0;
                }

                if ( beremovedbuffer != NULL )
                {
                    //beremovedbuffer->Stop();
                    SafeRelease( &beremovedbuffer );
                }

            }
        }
#ifdef AUDIO_DEBUG
        else
        {
            printf("****** swr_convert failed: %d\n", retConv );
        }
#endif
    }
    else
    {
#ifdef DEBUG
        printf("----- audio decode failure !\n");
#endif
        pkt->size = 0;
        pkt->data = NULL;
    }

    av_frame_unref( frameSRC );
    //av_free_packet( pkt );
    av_packet_unref( pkt );
}

int ffplayerRTSP::stream_component_open( AVFormatContext *ctx, int stream_index )
{
    AVCodecContext*     avctx;
    AVCodec*            codec;
    const char*         forced_codec_name = NULL;
    AVDictionary*       opts;
    AVDictionaryEntry*  t = NULL;
    int                 stream_lowres = 0;

    if ( stream_index < 0 || stream_index >= streamCtx->nb_streams )
        return -1;

    avctx = ctx->streams[stream_index]->codec;
    codec = avcodec_find_decoder(avctx->codec_id);

    switch( avctx->codec_type )
    {
        case AVMEDIA_TYPE_AUDIO:
            last_audio_stream_index = stream_index;
            break;

        case AVMEDIA_TYPE_VIDEO:
            last_video_stream_index = stream_index;
            break;

    }

    if ( codec == NULL )
    {
        return -1;
    }

    avctx->codec_id        = codec->id;
    avctx->workaround_bugs = 1;

    if( stream_lowres > av_codec_get_max_lowres( codec ) )
    {
        stream_lowres = av_codec_get_max_lowres( codec );
    }

    av_codec_set_lowres( avctx, stream_lowres );
    avctx->error_concealment = 3;

    if( stream_lowres > 0 )
        avctx->flags |= CODEC_FLAG_EMU_EDGE;

    // Set CODEC as fast !
    avctx->flags2 |= CODEC_FLAG2_FAST;

    if( codec->capabilities & CODEC_CAP_DR1 )
        avctx->flags |= CODEC_FLAG_EMU_EDGE;

    opts = filter_codec_opts( codec_opts,
                              avctx->codec_id,
                              ctx,
                              ctx->streams[stream_index],
                              codec );

    if ( av_dict_get(opts, "threads", NULL, 0) == NULL )
    {
        av_dict_set(&opts, "threads", "auto", 0);
    }

    if ( stream_lowres > 0 )
    {
        av_dict_set(&opts, "lowres", av_asprintf("%d", stream_lowres), AV_DICT_DONT_STRDUP_VAL);
    }

    if ( avctx->codec_type == AVMEDIA_TYPE_VIDEO || avctx->codec_type == AVMEDIA_TYPE_AUDIO )
    {
        av_dict_set(&opts, "refcounted_frames", "1", 0);
    }

    if ( avcodec_open2(avctx, codec, &opts) < 0 )
    {
        return -1;
    }

    ctx->streams[stream_index]->discard = AVDISCARD_DEFAULT;

    switch ( avctx->codec_type )
    {
        case AVMEDIA_TYPE_AUDIO:
#ifdef DEBUG
            printf("AUDIO INFO:\n");
            printf("\tSampling rate: %d\n", avctx->sample_rate);
            printf("\tChannels     : %d\n", avctx->channels);
            printf("\tChannels lay : %d\n", avctx->channel_layout);
#endif
            audioSampleRate = avctx->sample_rate;
            audioChannels   = avctx->channels;
            audioChannelLO  = avctx->channel_layout;
            audioFormat     = AV_SAMPLE_FMT_S16;

            /* prepare audio output */
            if ( audio_open() == true )
            {
                audio_stream_index   = stream_index;
                audio_strm           = ctx->streams[stream_index];

                checksAudioBuff = true;
            }
            break;

        case AVMEDIA_TYPE_VIDEO:
            video_stream_index = stream_index;
            video_strm         = ctx->streams[stream_index];
            checksVideoBuff    = true;
            break;

    }

    return 0;
}

void ffplayerRTSP::stream_component_close( AVFormatContext *ctx, int stream_index)
{
    if ( ctx == NULL )
        return;

    if ( stream_index < 0 || stream_index >= ctx->nb_streams )
        return;

    AVCodecContext *avctx = ctx->streams[stream_index]->codec;

    switch ( avctx->codec_type )
    {
        case AVMEDIA_TYPE_AUDIO:
            checksAudioBuff = false;

            audio_close();

            swr_free(&swr_ctx);
            av_freep(&audio_buff1);
            audio_buff1_size = 0;
            audio_buff = NULL;

            if ( frameSRC != NULL )
            {
                //av_frame_free(&frameSRC);
                av_frame_unref( frameSRC );
                frameSRC = NULL;
            }

            if ( rdft != NULL )
            {
                av_rdft_end( rdft );
                av_freep( &rdft_data );
                rdft = NULL;
                rdft_bits = 0;
            }

            break;

        case AVMEDIA_TYPE_VIDEO:
            checksVideoBuff = false;

            break;

        default:
            break;
    }

    ctx->streams[stream_index]->discard = AVDISCARD_ALL;
    avcodec_close( avctx );

    switch ( avctx->codec_type )
    {
        case AVMEDIA_TYPE_AUDIO:
            audio_strm = NULL;
            audio_stream_index = -1;
            break;

        case AVMEDIA_TYPE_VIDEO:
            video_strm = NULL;
            video_stream_index = -1;
            break;

    }
}

AVDictionary* ffplayerRTSP::filter_codec_opts( AVDictionary *opts, enum AVCodecID codec_id, AVFormatContext *s, AVStream *st, AVCodec *codec )
{
    AVDictionary    *ret = NULL;
    AVDictionaryEntry *t = NULL;
    int            flags = s->oformat ? AV_OPT_FLAG_ENCODING_PARAM
                                      : AV_OPT_FLAG_DECODING_PARAM;
    char          prefix = 0;
    const AVClass    *cc = avcodec_get_class();

    if (!codec)
        codec            = s->oformat ? avcodec_find_encoder(codec_id)
                                      : avcodec_find_decoder(codec_id);

    switch (st->codec->codec_type)
    {
        case AVMEDIA_TYPE_VIDEO:
            prefix  = 'v';
            flags  |= AV_OPT_FLAG_VIDEO_PARAM;
            break;

        case AVMEDIA_TYPE_AUDIO:
            prefix  = 'a';
            flags  |= AV_OPT_FLAG_AUDIO_PARAM;
            break;
    }

    while (t = av_dict_get(opts, "", t, AV_DICT_IGNORE_SUFFIX))
    {
        char *p = strchr(t->key, ':');

        /* check stream specification in opt name */
        if (p)
            switch (check_stream_specifier(s, st, p + 1)) {
            case  1: *p = 0; break;
            case  0:         continue;
            default:         return NULL;
            }

        if (av_opt_find(&cc, t->key, NULL, flags, AV_OPT_SEARCH_FAKE_OBJ) ||
            (codec && codec->priv_class &&
             av_opt_find(&codec->priv_class, t->key, NULL, flags,
                         AV_OPT_SEARCH_FAKE_OBJ)))
            av_dict_set(&ret, t->key, t->value, 0);
        else if (t->key[0] == prefix &&
                 av_opt_find(&cc, t->key + 1, NULL, flags,
                             AV_OPT_SEARCH_FAKE_OBJ))
            av_dict_set(&ret, t->key + 1, t->value, 0);

        if (p)
            *p = ':';
    }
    return ret;
}

bool ffplayerRTSP::audio_open()
{
#ifdef DEBUG
    printf("ffprtsp::audio_open()\n");
#endif
    return true;
}

void ffplayerRTSP::audio_close()
{
#ifdef DEBUG
    printf("ffprtsp::audio_close()\n");
#endif
    if ( dsoundbuff != NULL )
    {
        dsoundbuff->Stop();

        SafeRelease( &dsoundbuff );
    }
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

DWORD ffplayerRTSP::ThreadLoop()
{
    if ( streamCtx == NULL )
        return 0;

#ifdef DEBUG
    printf("ThreadLoop() enter >>>>\n");
#endif

    av_read_play( streamCtx );


    while( threadkillflag == false )
    {
        decodedframecount = av_read_frame( streamCtx, &avpacket );

        if ( decodedframecount < 0 )
        {
            //av_free_packet( &avpacket );
            av_packet_unref( &avpacket );
            continue;
        }

        if ( frameSRC == NULL )
        {
            //frameSRC = avcodec_alloc_frame();
            frameSRC = av_frame_alloc();
        }

        // -----------------------[AUDIO]---------------------------------------
        if ( avpacket.stream_index == audio_stream_index )
        {
            decode_audio( &avpacket );
        }
        else
        // -----------------------[VIDEO]---------------------------------------
        if ( avpacket.stream_index == video_stream_index )
        {
            decode_video( &avpacket );
        } /// video stream
        else
        {
            // Or ? discard packet.
            // av_free_packet( &avpacket );
            av_packet_unref( &avpacket );
        }
    }

#ifdef DEBUG
    printf("<<<<<<<< ThreadLoop() out\n");
#endif

    return 0;
}

////////////////////////////////////////////////////////////////////////////////

AVDictionary *filter_codec_opts( AVDictionary *opts, enum AVCodecID codec_id, AVFormatContext *s, AVStream *st, AVCodec *codec )
{
    AVDictionary    *ret = NULL;
    AVDictionaryEntry *t = NULL;
    int            flags = s->oformat ? AV_OPT_FLAG_ENCODING_PARAM
                                      : AV_OPT_FLAG_DECODING_PARAM;
    char          prefix = 0;
    const AVClass    *cc = avcodec_get_class();

    if (!codec)
        codec            = s->oformat ? avcodec_find_encoder(codec_id)
                                      : avcodec_find_decoder(codec_id);

    switch (st->codec->codec_type)
    {
        case AVMEDIA_TYPE_VIDEO:
            prefix  = 'v';
            flags  |= AV_OPT_FLAG_VIDEO_PARAM;
            break;

        case AVMEDIA_TYPE_AUDIO:
            prefix  = 'a';
            flags  |= AV_OPT_FLAG_AUDIO_PARAM;
            break;

        case AVMEDIA_TYPE_SUBTITLE:
            prefix  = 's';
            flags  |= AV_OPT_FLAG_SUBTITLE_PARAM;
            break;
    }

    while (t = av_dict_get(opts, "", t, AV_DICT_IGNORE_SUFFIX))
    {
        char *p = strchr(t->key, ':');

        /* check stream specification in opt name */
        if (p)
        {
            switch (check_stream_specifier(s, st, p + 1))
            {
                case  1: *p = 0; break;
                case  0:         continue;
                default:         return NULL;
            }
        }

        if (av_opt_find(&cc, t->key, NULL, flags, AV_OPT_SEARCH_FAKE_OBJ) ||
            (codec && codec->priv_class &&
             av_opt_find(&codec->priv_class, t->key, NULL, flags,
                         AV_OPT_SEARCH_FAKE_OBJ)))
            av_dict_set(&ret, t->key, t->value, 0);
        else if (t->key[0] == prefix &&
                 av_opt_find(&cc, t->key + 1, NULL, flags,
                             AV_OPT_SEARCH_FAKE_OBJ))
            av_dict_set(&ret, t->key + 1, t->value, 0);

        if (p)
            *p = ':';
    }

    return ret;
}

AVDictionary **setup_find_stream_info_opts(AVFormatContext *s, AVDictionary *codec_opts)
{
    int i;
    AVDictionary** opts;

    if (!s->nb_streams)
        return NULL;

    opts = (AVDictionary**)av_mallocz(s->nb_streams * sizeof(*opts));

    if (!opts)
    {
        return NULL;
    }

    for (i = 0; i < s->nb_streams; i++)
    {
        opts[i] = filter_codec_opts( codec_opts, s->streams[i]->codec->codec_id,
                                     s, s->streams[i], NULL);

    }

    return opts;
}

int check_stream_specifier( AVFormatContext *s, AVStream *st, const char *spec )
{
    return avformat_match_stream_specifier(s, st, spec);
}

////////////////////////////////////////////////////////////////////////////////

DWORD WINAPI ffplaymodcb_win32( LPVOID p )
{
    if ( p != NULL )
    {
        ffplayerRTSP* pp = (ffplayerRTSP*)p;

        return pp->ThreadLoop();
    }
}
