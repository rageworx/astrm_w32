#ifndef __FFPRTSP_H__
#define __FFPRTSP_H__

#include <windows.h>
#include <tchar.h>

// Now I using ffmpeg 3.x
// It moved to ffmpeg directory, no more existed in src dir.
// please proceed run ./mkffmepglib.sh in ffmpeg dir.
#include "ffconfig.h"

extern "C" {
#include "libavutil/avstring.h"
#include "libavutil/colorspace.h"
#include "libavutil/mathematics.h"
#include "libavutil/pixdesc.h"
#include "libavutil/imgutils.h"
#include "libavutil/dict.h"
#include "libavutil/parseutils.h"
#include "libavutil/samplefmt.h"
#include "libavutil/avassert.h"
#include "libavutil/time.h"
#include "libavformat/avformat.h"
#include "libavdevice/avdevice.h"
#include "libswscale/swscale.h"
#include "libavutil/opt.h"
#include "libavutil/log.h"
#include "libavcodec/avfft.h"
#include "libswresample/swresample.h"


#include "libavfilter/avfilter.h"
#include "libavfilter/buffersink.h"
#include "libavfilter/buffersrc.h"
}

#include "dxsound.h"

#define DEF_VID_RENDER_BUFFERS          2

typedef struct
{
    int            lock;
    unsigned char* buffer;
    int            size;
}videoRaw;

class ffplayerRTSP
{
    public:
        ffplayerRTSP();
        ~ffplayerRTSP();

    public:
        bool TestConnection( const char* ip, unsigned wsec = 3 );
        bool OpenURL( const char* url );
        bool Update();
        void Stop();

    public:
        int  GetVideoWidth();
        int  GetVideoHeight();
        void SetReconfigAudio();
        void SetInterlaced( bool m = true );

    public:
        DWORD ThreadLoop();

    public:
        ID2D1Bitmap*    getRenderBuffer();

    protected:
        void ffmpeg_init();
        void reset_me();
        int  stream_component_open( AVFormatContext *ctx, int stream_index );
        void stream_component_close( AVFormatContext *ctx, int stream_index );
        AVDictionary* filter_codec_opts( AVDictionary *opts, enum AVCodecID codec_id, AVFormatContext *s, AVStream *st, AVCodec *codec );
        bool audio_open();
        void audio_close();
        void decode_video( AVPacket* pkt );
        void decode_audio( AVPacket* pkt );

    protected:
        HANDLE          hThreadRead;

    protected:  /// videos ---------------
        videoRaw        videoRawBuffer[DEF_VID_RENDER_BUFFERS];
        int             videoRawQue;
        int             videoRawQue_r;
        int             videoRawWidth;
        int             videoRawHeight;

    protected:  /// audios ---------------
        IDirectSound*   dsound;
        IDirectSoundBuffer* dsoundbuff;
        unsigned char*  audio_buff_cache;
        int             audio_buff_que_w;
        unsigned char*  audio_buff;
        int             audio_buff_size;
        unsigned char*  audio_buff1;
        int             audio_buff1_size;
        unsigned        audioRawMS;
        DSBUFFERDESC    audioBufferDescription;
        int             audioChannels;
        int             audioSampleRate; // == Freq rate
        enum
        AVSampleFormat  audioFormat;
        int             audioFrameSize;
        int             audioBPS;
        int             audioChannelLO;

    protected:  /// FFmpeg
        AVFormatContext*streamCtx;
        AVInputFormat*  streamFormat;
        AVDictionary*   format_ops;
        AVDictionary*   codec_opts;
        AVDictionary*   resample_opts;
        int             decodedframecount;
        int             lastAVreturn;
        double          maxDuration;
        char*           contentTitle;
        int             streamIndex[ AVMEDIA_TYPE_NB ];
        AVRational      av_sar;
        AVPacket        avpacket;
        int             audio_stream_index;
        int             video_stream_index;
        int             last_audio_stream_index;
        int             last_video_stream_index;
        AVStream*       video_strm;
        AVStream*       audio_strm;
        RDFTContext*    rdft;
        int             rdft_bits;
        FFTSample*      rdft_data;
        AVFrame*        frameSRC;
        AVFrame*        frameConv;
        AVDictionary*   swr_opts;
        SwrContext*     swr_audio;
        // --- structs ---
        struct
        SwsContext*     sws_opts;
        struct
        SwrContext*     swr_ctx;
        struct
        SwsContext*     img_convert_ctx;

    protected:
        bool            firstinit;
        bool            threadkillflag;
        bool            checksVideoBuff;
        bool            checksAudioBuff;
        bool            lockVidBuff;
        bool            lockAudBuff;
        bool            reconfigAudio;
        bool            streamopened;
        bool            interlaced;
        int             vidWidth;
        int             vidHeight;
        int             audioBuffSize;
};

#endif /// of __FFPRTSP_H__
