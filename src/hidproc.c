#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define _CONVX( _a_, _b_ )      uatoh( _a_, _b_ )
#define _MEMSET                 memset
#define _MEMCPY                 memcpy
#define _STRLEN                 strlen
#define _ATOI                   atoi

#include "hidproc.h"

////////////////////////////////////////////////////////////////////////////////

#define ANYSTREAM_PACKET_MAGIC_KEYBOARD     'k'
#define ANYSTREAM_PACKET_MAGIC_MOUSE        'm'
#define ANYSTREAM_PACKET_MAGIC_JOYSTICK     'j'

#define ANYSTREAM_PACKET_KBD_MAX_LEN        34 // == 8 * 4 + 2

////////////////////////////////////////////////////////////////////////////////

short numberic_cutoff( short n )
{
    if ( n > 0 )
    {
        if ( n > ANYSTREAM_HID_MAX_POSITIVE )
            return ANYSTREAM_HID_MAX_POSITIVE;
    }
    else
    if ( n < 0 )
    {
        if ( n < ANYSTREAM_HID_MAX_NEGATIVE )
            return ANYSTREAM_HID_MAX_NEGATIVE;
    }

    return n;
}

int  upow( int n, int m )
{
    int num = n;
    int cnt = 0;

    if ( m == 0 )
        return 0;

    for( cnt=0; cnt<m-1; cnt++ )
    {
        num *= n;
    }

    return num;
}

void uatoh( const char* src, unsigned int* out )
{
    char cnt;
    char c_ch;
    char c_que=0;
    char c_buf[10]={0};

    if ( src != NULL )
    {
        while( *src )
        {
            c_ch = *src;
            if ( c_ch == ' ' )
                break;

            switch ( c_ch )
            {
                case '0': c_ch = 0; break;
                case '1': c_ch = 1; break;
                case '2': c_ch = 2; break;
                case '3': c_ch = 3; break;
                case '4': c_ch = 4; break;
                case '5': c_ch = 5; break;
                case '6': c_ch = 6; break;
                case '7': c_ch = 7; break;
                case '8': c_ch = 8; break;
                case '9': c_ch = 9; break;
                case 'a':
                case 'A': c_ch = 10; break;
                case 'b':
                case 'B': c_ch = 11; break;
                case 'c':
                case 'C': c_ch = 12; break;
                case 'd':
                case 'D': c_ch = 13; break;
                case 'e':
                case 'E': c_ch = 14; break;
                case 'f':
                case 'F': c_ch = 15; break;
            }

            c_buf[c_que] = c_ch;
            c_que++;
            if ( c_que > 4 )
                return;
            src++;
        }

        *out = 0;


        for(cnt=0;cnt<c_que-1;cnt++)
        {
            *out += c_buf[cnt] * upow( 16 , c_que - cnt - 1 );
        }
        *out += c_buf[cnt];

    }
}

unsigned short atox( const char* s )
{
    unsigned int tox = 0;
    _CONVX( s, &tox);
    return (unsigned short)tox;
}


char* dtoa( char *a, int i )
{
    int num    = i;
    int deg    = 1;
    int cntmax = 0;
    int cnt    = 0;

    if ( a == NULL )
        return NULL;

    if ( num < 0 )
    {
        *a   = '-';
        num *= -1;
        a++;
    }

    if ( num != 0 )
    {
        while( 1 )
        {
            if ( ( num / deg ) > 0 )
                cntmax++;
            else
                break;

            deg *= 10;
        }

        deg /= 10;

        for( cnt=0; cnt<cntmax; cnt++ )
        {
            *a   = ( num / deg ) + '0';
            num -= ( num / deg ) * deg;
            deg /= 10;
            a++;
        }
    }
    else
    {
        *a = '0';
        a++;
    }

    *a = '\0';

	return a;
}

const char dtox_matrix[] = "0123456789ABCDEF";

char* dtox( char *a, int i )
{
	int   sign = 0;
	char  buf[16] = {0};
	char* ptr = buf;

	if( i > 0 )
	{
		/* make string in reverse form */
		if( i < 0 )
        {
            i = ~i + 1;
            sign++;
        }

		while( i )
        {
            *ptr++ = dtox_matrix[ i % 16 ];
            i = i / 16;
        }

		if( sign )
			*ptr++ = '-';

		*ptr = '\0';

		/* copy reverse order */
		for( i=0; i < _STRLEN(buf); i++ )
        {
            *a++ = buf[_STRLEN(buf)-i-1];
        }
	}
	else
		*a++ = '0';

	return a;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int convert_fltk_key_combines( const unsigned short* keybuff, int keybufflen, unsigned char* outbuff )
{
    int cnt;
    int que = 0;

    for( cnt=0; cnt<keybufflen; cnt++ )
    {
        if ( keybuff[cnt] > 0x00FF )
        {
            switch( keybuff[cnt] )
            {
                case 0xEF11:    /// MM-Key : Sound-Volume-Down
                    outbuff[que++] = 0x81;
                    break;

                case 0xEF12:    /// MM-Key : Sound-Mute
                    outbuff[que++] = 0x7F;
                    break;

                case 0xEF13:    /// MM-Key : Sound-Volume-Up
                    outbuff[que++] = 0x80;
                    break;

                case 0xEF14:    /// MM-Key : Sound-Play-Pause -> Maybe STOP ?
                    outbuff[que++] = 0x78;
                    break;

                case 0xEF16:    /// MM-Key : Sound-Rewind -> Meybe Undo ????
                    outbuff[que++] = 0x7A;
                    break;

                case 0xEF17:    /// MM-Key : Sound-Forward -> Maybe Again ????
                    outbuff[que++] = 0x79;
                    break;

                // ------------------------------------------------------------------
                // Special keys ?

                case 0xFFE3:    /// Left CTRL
                    outbuff[que++] = 0xE0;
                    break;

                case 0xFFE7:    /// Left Windows/Meta Key
                    outbuff[que++] = 0xE3;
                    break;

                case 0x0FFE8:   /// Right Windows/Meta Key
                    outbuff[que++] = 0xE7;
                    break;

                case 0xFFE9:    /// Left ALT
                    outbuff[que++] = 0xE2;
                    break;

                case 0xFFEA:    /// Right ALT
                    outbuff[que++] = 0xE6;
                    break;

                case 0xFFE4:    /// Right CTRL
                    outbuff[que++] = 0xE4;
                    break;

                case 0xFFE1:    /// SHIFT (L)
                    outbuff[que++] = 0xE1;
                    break;

                case 0xFFE2:    /// SHIFT (R)
                    outbuff[que++] = 0x0E5;

                // ------------------------------------------------------------------

                case 0xFF08:    /// Back Space ( Delete forward )
                    outbuff[que++] = 0x2A;
                    break;

                case 0xFF09:    /// TAB
                    outbuff[que++] = 0x2B;
                    break;

                case 0xFF0D:    /// ENTER
                    outbuff[que++] = 0x28;
                    break;

                case 0xFF13:    /// Pause
                    outbuff[que++] = 0x48;
                    break;

                case 0xFF14:    /// Scroll Lock
                    outbuff[que++] = 0x47;

                case 0xFF1B:    /// ESC
                    outbuff[que++] = 0x29;
                    break;

                case 0xFF50:    /// Home
                    outbuff[que++] = 0x4A;
                    break;

                case 0xFF51:    /// Keyboard LEFT arrow
                    outbuff[que++] = 0x50;
                    break;

                case 0xFF52:    /// Keyboard UP arrow
                    outbuff[que++] = 0x52;
                    break;

                case 0xFF53:    /// Keyboard RIGHT arrow
                    outbuff[que++] = 0x4F;
                    break;

                case 0xFF54:    /// Keyboard DOWN arraw
                    outbuff[que++] = 0x51;
                    break;

                case 0xFF55:    /// Page Up
                    outbuff[que++] = 0x4B;
                    break;

                case 0xFF56:    /// Page Down
                    outbuff[que++] = 0x4E;
                    break;

                case 0xFF57:    /// End
                    outbuff[que++] = 0x4D;
                    break;

                case 0xFF61:    /// Print Screen
                    outbuff[que++] = 0x46;
                    break;

                case 0xFF63:    /// Insert
                    outbuff[que++] = 0x49;
                    break;

                case 0xFF67:    /// Right MENU:
                    outbuff[que++] = 0x76;
                    break;

                case 0xFFBE:    /// F1
                case 0xFFBF:    /// F2
                case 0xFFC0:    /// F3
                case 0xFFC1:    /// F4
                case 0xFFC2:    /// F5
                case 0xFFC3:    /// F6
                case 0xFFC4:    /// F7
                case 0xFFC5:    /// F8
                case 0xFFC6:    /// F9
                case 0xFFC7:    /// F10
                case 0xFFC8:    /// F11
                case 0xFFC9:    /// F12
                    outbuff[que++] = 0x3A - (unsigned char)( keybuff[cnt] - 0xFFBE );
                    break;

                case 0xFFE5:    /// Caps Lock
                    outbuff[que++] = 0x39;
                    break;

                case 0xFFFF:    /// Delete
                    outbuff[que++] = 0x4C;
                    break;

            }
        }
        else
        {
            if ( ( keybuff[cnt] >= 0x0061 ) && ( keybuff[cnt] <= 0x007A ) ) /// A ... Z
            {
                outbuff[que++] = (unsigned char)( keybuff[cnt] - 0x0061 + 0x04 );
            }
            else
            if ( ( keybuff[cnt] >= 0x0031 ) && ( keybuff[cnt] <= 0x0039 ) ) /// 1(!) .. 9(()
            {
                outbuff[que++] = (unsigned char)( keybuff[cnt] - 0x0031 + 0x1E );
            }
            else
            {
                switch( keybuff[cnt] )
                {
                    case 0x0018:    /// Hangeul convert ?
                        outbuff[que++] = 0x90;

                    case 0x0019:    /// Hanja convert
                        outbuff[que++] = 0x91;   /// Not declared --
                        break;

                    case 0x0020:    /// Space bar
                        outbuff[que++] = 0x2C;
                        break;

                    case 0x002D:    /// - & _
                        outbuff[que++] = 0x2D;
                        break;

                    case 0x0027:    /// ' & "
                        outbuff[que++] = 0x34;
                        break;

                    case 0x002C:    /// , & <
                        outbuff[que++] = 0x36;
                        break;

                    case 0x002E:    /// . & >
                        outbuff[que++] = 0x37;
                        break;

                    case 0x002F:    /// ? & /
                        outbuff[que++] = 0x38;
                        break;

                    case 0x0030:    /// 0 ())
                        outbuff[que++] = 0x27;
                        break;

                    case 0x003B:    /// ; & :
                        outbuff[que++] = 0x33;
                        break;

                    case 0x003D:    /// = & +
                        outbuff[que++] = 0x2E;
                        break;

                    case 0x005B:    /// [ & {
                        outbuff[que++] = 0x2F;
                        break;

                    case 0x005C:    /// \ & |
                        outbuff[que++] = 0x31;
                        break;

                    case 0x005D:    /// ] & }
                        outbuff[que++] = 0x30;
                        break;

                    default:
                        outbuff[que++] = (unsigned char)( keybuff[cnt] & 0x00FF );
                        break;
                }
            }
        }
    }

    return que;
}

int anystream_hid_keyboard_combine( const unsigned char modkey, const unsigned char* keybuff, int keybufflen, char** packet )
{
    int             count           = 0;
    static char     _packet[ ANYSTREAM_PACKET_KBD_MAX_LEN ];
    int             conv_len        = 0;
    int             left_len        = 0;
    char            conv_buff[10]   = {0};

    if ( keybufflen > ANYSTREAM_MAX_KEYS )
        return -1;

    _MEMSET( _packet, 0, ANYSTREAM_PACKET_KBD_MAX_LEN );

    sprintf(_packet, "%c %d 0", ANYSTREAM_PACKET_MAGIC_KEYBOARD, modkey );

    conv_len = keybufflen;

    if ( conv_len > 6 )
        conv_len = 6;

    left_len = 6 - conv_len;

    for ( count=0; count<conv_len; count++ )
    {
        sprintf(conv_buff, " %d", keybuff[count] );
        strcat(_packet, conv_buff);
    }

    if ( left_len > 0 )
    {
        for ( count=0; count<left_len; count++ )
        {
            strcat( _packet, " 0" );
        }
    }

    _packet[ strlen(_packet) ] = ANYSTREAM_HID_BYTE_TERMINATOR;

    *packet = strdup( _packet );

    return strlen(_packet);
}

int anystream_hid_mouse_combine( short vec_x, short vec_y, short wheel_x, short wheel_y, unsigned char buttons, char** packet )
{
    int          _packet_que  = 0;
    int          _packet_size = 0;
    static char  _packet[40]  = {0};
    short        v_x          = numberic_cutoff(vec_x);
    short        v_y          = numberic_cutoff(vec_y);

    _MEMSET( _packet, 0, 40 );

    sprintf( _packet,
             "%c %d %d %d %d %d %c",
             ANYSTREAM_PACKET_MAGIC_MOUSE,
             v_x,
             v_y,
             wheel_x,
             wheel_y,
             buttons,
             ANYSTREAM_HID_BYTE_TERMINATOR );


    *packet = strdup( _packet );

    return strlen( _packet );
}

int  anystream_hid_joystick_combine( short vx, short vy, short vz, short vrx, short vry, short vrz, unsigned short buttons, char** packet )
{
    int          _packet_que  = 0;
    int          _packet_size = 0;
    static char  _packet[80]  = {0};
    short        v_x          = numberic_cutoff(vx);
    short        v_y          = numberic_cutoff(vy);
    short        v_z          = numberic_cutoff(vz);
    short        v_rx         = numberic_cutoff(vrx);
    short        v_ry         = numberic_cutoff(vry);
    short        v_rz         = numberic_cutoff(vrz);

    _MEMSET( _packet, 0, 80 );

    sprintf( _packet,
             "%c %d %d %d %d %d %d %d %d %c",
             ANYSTREAM_PACKET_MAGIC_JOYSTICK,
             v_x,
             v_y,
             v_rx,
             v_ry,
             v_z,
             v_rz,
             buttons & 0x00FF,
             buttons >> 8,
             ANYSTREAM_HID_BYTE_TERMINATOR );


    *packet = strdup( _packet );

    return strlen( _packet );
}

void anystream_hid_keyboard_uncombine( const char* packet, unsigned char* modkey, unsigned char* keybuff )
{
    if ( packet == NULL )
        return;

    if ( strlen( packet ) > 16 )
    {
        if ( packet[0] != ANYSTREAM_PACKET_MAGIC_KEYBOARD )
            return;

        if ( keybuff == NULL )
            return;

        int   cnt = 0;
        char* tmp = strdup( &packet[2] );
        char* stk = strtok( tmp, " " );

        while( stk != NULL )
        {
            switch ( cnt )
            {
                case 0:
                    *modkey = atoi( stk );
                    break;

                case 1:
                    break;

                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                    keybuff[ cnt - 2 ] = atoi( stk );
                    break;

            }

            cnt++;
            stk = strtok( NULL, " " );
        }
    }
}

void anystream_hid_mouse_uncombine( const char* packet, short* vec_x, short* vec_y, short* wheel_x, short* wheel_y, unsigned char* buttons )
{
    if ( packet == NULL )
        return;

    if ( packet[0] != ANYSTREAM_PACKET_MAGIC_MOUSE )
        return;

    if ( strlen( packet ) > 13 )
    {
        int   cnt = 0;
        char* tmp = strdup( &packet[2] );
        char* stk = strtok( tmp, " " );

        while( stk != NULL )
        {
            switch( cnt )
            {
                case 0:
                    *vec_x = atoi( stk );
                    break;

                case 1:
                    *vec_y = atoi( stk );
                    break;

                case 2:
                    *wheel_x = atoi( stk );
                    break;

                case 3:
                    *wheel_y = atoi( stk );
                    break;

                case 4:
                    *buttons = atoi( stk );

                default:
                    break;
            }

            stk = strtok( NULL, " " );
            cnt++;
        }

        free( tmp );
    }
}

void anystream_hid_joystick_uncombine( const char* packet, short* vx, short* vy, short* vz, short* vrx, short* vry, short* vrz, unsigned short* buttons )
{
    if ( packet == NULL )
        return;

    if ( packet[0] != ANYSTREAM_PACKET_MAGIC_JOYSTICK )
        return;

    if ( strlen( packet ) > 13 )
    {
        unsigned char btns_l = 0;
        unsigned char btns_h = 0;

        int   cnt = 0;
        char* tmp = strdup( &packet[2] );
        char* stk = strtok( tmp, " " );

        while( stk != NULL )
        {
            switch( cnt )
            {
                case 0:
                    *vx = atoi( stk );
                    break;

                case 1:
                    *vy = atoi( stk );
                    break;

                case 2:
                    *vrx = atoi( stk );
                    break;

                case 3:
                    *vry = atoi( stk );
                    break;

                case 4:
                    *vz = atoi( stk );
                    break;

                case 5:
                    *vrz = atoi( stk );
                    break;

                case 6:
                    btns_l = atoi( stk );
                    break;

                case 7:
                    btns_h = atoi( stk );
                    break;

                default:
                    break;

            }

            stk = strtok( NULL, " " );
            cnt++;
        }

        free( tmp );

        *buttons = ( btns_l ) | ( btns_h << 8 );
    }
}

void anystream_hid_destroy_packet( char** packet )
{
    if ( *packet != NULL )
    {
        free( *packet );
        *packet = NULL;
    }
}
