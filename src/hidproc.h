#ifndef __HIDPROC_H__
#define __HIDPROC_H__
/*******************************************************************************
** AnyStream HID common control APIs
** ---------------------------------
** Keep, limited in STD-C/ISO-C
** (C)Copyright 2015, 3Iware - Raphael Kim
**
** Update in 2015-07-22
** - packet definition updated for keyboard, mouse, joystick.
** - packet structure updated.
**
** Update in 2015-11-05
** - joystick packet updated for supporting X-Y-Z, Rx-Ry-Rz & 10 more buttons.
**
** Update in 2015-12-09
** - joystick packet updated for supporting 16 buttons.
**
** Update in 2016-04-19
** - packet uncombine functions included.
**
*******************************************************************************/

#ifdef __cplusplus
extern "C" {
#endif /// of __cplusplus

/**
  AnyStream HID definitions :
      AnyStream HID controller limited to combine signals in ranged.

**/
#define ANYSTREAM_HID_BYTE_TERMINATOR       '@'

#define ANYSTREAM_HID_MAX_POSITIVE          127
#define ANYSTREAM_HID_MAX_NEGATIVE          -127
#define ANYSTREAM_HID_PACKET_MIN_SIZE       5

#define ANYSTREAM_RATE_SCAN_KEYBOARD        100         /// scale = ms.
#define ANYSTREAM_MAX_KEYS                  6

#define ANYSTREAM_KEYMASK_LEFT_CTRL         0x01    /// 0000:0001
#define ANYSTREAM_KEYMASK_LEFT_SHIFT        0x02    /// 0000:0010
#define ANYSTREAM_KEYMASK_LEFT_ALT          0x04    /// 0000:0100
#define ANYSTREAM_KEYMASK_LEFT_GUI          0x08    /// 0000:1000
#define ANYSTREAM_KEYMASK_RIGHT_CTRL        0x10    /// 0001:0000
#define ANYSTREAM_KEYMASK_RIGHT_SHIFT       0x20    /// 0010:0000
#define ANYSTREAM_KEYMASK_RIGHT_ALT         0x40    /// 0100:0000
#define ANYSTREAM_KEYMASK_RIGHT_GUI         0x80    /// 1000:0000

#define ANYSTREAM_MASK_BUTTON1              0x0001      /// 0000-0000-0000-0001
#define ANYSTREAM_MASK_BUTTON2              0x0002      /// 0000-0000-0000-0010
#define ANYSTREAM_MASK_BUTTON3              0x0004      /// 0000-0000-0000-0100
#define ANYSTREAM_MASK_BUTTON4              0x0008      /// 0000-0000-0000-1000
#define ANYSTREAM_MASK_BUTTON5              0x0010      /// 0000-0000-0001-0000
#define ANYSTREAM_MASK_BUTTON6              0x0020      /// 0000-0000-0010-0000
#define ANYSTREAM_MASK_BUTTON7              0x0040      /// 0000-0000-0100-0000
#define ANYSTREAM_MASK_BUTTON8              0x0080      /// 0000-0000-1000-0000
#define ANYSTREAM_MASK_BUTTON_EX9           0x0100      /// 0000-0001-0000-0000
#define ANYSTREAM_MASK_BUTTON_EX10          0x0200      /// 0000-0010-0000-0000
#define ANYSTREAM_MASK_BUTTON_EX11          0x0400      /// 0000-0100-0000-0000
#define ANYSTREAM_MASK_BUTTON_EX12          0x0800      /// 0000-1000-0000-0000
#define ANYSTREAM_MASK_BUTTON_EX13          0x1000      /// 0001-0000-0000-0000
#define ANYSTREAM_MASK_BUTTON_EX14          0x2800      /// 0010-0000-0000-0000
#define ANYSTREAM_MASK_BUTTON_EX15          0x4800      /// 0100-0000-0000-0000
#define ANYSTREAM_MASK_BUTTON_EX16          0x0800      /// 1000-0000-0000-0000

int  anystream_hid_keyboard_combine( const unsigned char modkey, const unsigned char* keybuff, int keybufflen, char** packet );
int  anystream_hid_mouse_combine( short vec_x, short vec_y, short wheel_x, short wheel_y, unsigned char buttons, char** packet );
int  anystream_hid_joystick_combine( short vx, short vy, short vz, short vrx, short vry, short vrz, unsigned short buttons, char** packet );

void anystream_hid_keyboard_uncombine( const char* packet, unsigned char* modkey, unsigned char* keybuff );
void anystream_hid_mouse_uncombine( const char* packet, short* vec_x, short* vec_y, short* wheel_x, short* wheel_y, unsigned char* buttons );
void anystream_hid_joystick_uncombine( const char* packet, short* vx, short* vy, short* vz, short* vrx, short* vry, short* vrz, unsigned short* buttons );

void anystream_hid_destroy_packet( char** packet );

#ifdef __cplusplus
}
#endif /// of __cplusplus

#endif /// of __HIDPROC_H__
