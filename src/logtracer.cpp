#include <unistd.h>
#include <time.h>

#include <cstdio>
#include <cstdlib>
#include <cstdarg>

#include "logtracer.h"

#define DEF_MAX_LOGFNAME_LEN        512

LogTracer::LogTracer( const TCHAR* logfn, bool stdprt )
 : fopened( false ),
   fptr( NULL ),
   print2stdout( stdprt )
{
    logfilename = new TCHAR[ DEF_MAX_LOGFNAME_LEN ];
    if ( logfilename != NULL )
    {
        memset( &logfilename[0], 0, sizeof(TCHAR) * DEF_MAX_LOGFNAME_LEN );
    }

    time_t current_time;
    struct tm* current_tm;

    current_time = time( NULL );
    current_tm = localtime( &current_time );

    if( logfn != NULL )
    {
        _tcscpy_s( logfilename, DEF_MAX_LOGFNAME_LEN, logfn );
    }
    else
    {
        // No log file decided, let define itself with time balue...

        _stprintf( logfilename,
                   __T("aswin32log_%04d%02d%02d%02d%02d%02d.log"),
                   current_tm->tm_year + 1900,
                   current_tm->tm_mon + 1,
                   current_tm->tm_mday,
                   current_tm->tm_hour,
                   current_tm->tm_min,
                   current_tm->tm_sec );
    }

    // It doesn't care file exists .
    fptr = _tfopen( logfilename, __T("w+") );
    if( fptr != NULL )
    {
        fopened = true;
    }

    // Let write
    write( __T("=========================================================================\n") );
    write( __T("Log started at : %04d.%02d.%02d-%02d:%02d:%02d\n"),
           current_tm->tm_year + 1900,
           current_tm->tm_mon + 1,
           current_tm->tm_mday,
           current_tm->tm_hour,
           current_tm->tm_min,
           current_tm->tm_sec );
    write( __T("=========================================================================\n") );
    write( __T("\n") );
}

LogTracer::~LogTracer()
{
    if ( fptr != NULL )
    {
        fclose( fptr );
        fptr = NULL;
    }

    fopened = false;

    if ( logfilename != NULL )
    {
        delete[] logfilename;
    }
}

LogTracer::write( const wchar_t* args, ... )
{
    if ( fopened == true )
    {
        if ( fptr != NULL )
        {
            wchar_t tmpbuff[ DEF_MAX_LOGFNAME_LEN ] = {0};
            va_list vadata;
            va_start( vadata, args );
            vswprintf( tmpbuff, DEF_MAX_LOGFNAME_LEN, args, vadata );
            va_end( vadata );

            char convbuff[ DEF_MAX_LOGFNAME_LEN ] = {0};
            wcstombs( convbuff, tmpbuff, DEF_MAX_LOGFNAME_LEN );

            fwrite( convbuff, 1, strlen( convbuff ), fptr );
            fflush( fptr );

            if ( print2stdout == true )
            {
                fputws( tmpbuff, stdout );
            }
        }
    }
}

LogTracer::write( const char* args, ... )
{
    if ( fopened == true )
    {
        if ( fptr != NULL )
        {
            char tmpbuff[ DEF_MAX_LOGFNAME_LEN ] = {0};
            va_list vadata;
            va_start( vadata, args );
            vsprintf( tmpbuff, args, vadata );
            va_end( vadata );

            fwrite( tmpbuff, 1, strlen( tmpbuff), fptr );
            fflush( fptr );

            if ( print2stdout == true )
            {
                fputs( tmpbuff, stdout );
            }
        }
    }
}
