#ifndef __LOGTRACER_H__
#define __LOGTRACER_H__

#include <tchar.h>

class LogTracer
{
    public:
        LogTracer( const TCHAR* logfn = NULL, bool stdprt = false );
        ~LogTracer();

    public:
        const TCHAR* filename() { return logfilename; }

    public:
        write( const wchar_t* args, ... );
        write( const char* args, ... );

    private:
        TCHAR*      logfilename;
        bool        fopened;
        bool        print2stdout;
        FILE*       fptr;
};

#endif /// of __LOGTRACER_H__
