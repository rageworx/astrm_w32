////////////////////////////////////////////////////////////////////////////////
// A-streaming client DX/Win32 model.
////////////////////////////////////////////////////////////////////////////////

#include "main.h"

#ifdef _WIN32_WINNT
    #undef _WIN32_WINNT
#endif

#define _WIN32_WINNT  0x0600

#include <winsock2.h>
#include <ws2tcpip.h>

#include "dxgraph.h"
#include "dxsound.h"
#include "dxinput.h"
#include "hidproc.h"
#include "askeymap.h"
#include "saferelease.h"
#include "config.h"
#include "ffprtsp.h"
#include "libavutil/ffversion.h"
#include "yasmversion.h"
#include "logtracer.h"
#include "winethinfo.h"
#include "askeyexchange.h"
#include "askeyexchangetools.h"

#include "resource.h"

////////////////////////////////////////////////////////////////////////////////

using namespace D2D1;

////////////////////////////////////////////////////////////////////////////////

#define DEF_MOUSE_VIEWMODE_HIDE_COUNT       100
#define DEF_MUTEX_NAME                      "astreamingwin32mtx"
#define DEF_WINDOW_CLASSNAME                "ASWIN32CL"

////////////////////////////////////////////////////////////////////////////////

typedef struct _as_bps_item
{
    unsigned int    bps;
    TCHAR*          text;
}as_bps_item;

////////////////////////////////////////////////////////////////////////////////

const unsigned char as_source_bps_size = 8;       /// Actual support bps limited to 8 items.

const as_bps_item as_source_bps_table[] =
{
    { 12000000, TEXT("12 Mbps") },
    { 8000000,  TEXT("8 Mbps ") },
    { 4000000,  TEXT("4 Mbps ") },
    { 2000000,  TEXT("2 Mbps ") },
    { 1500000,  TEXT("1.5 Mbps ") },
    { 1000000,  TEXT("1 Mbps ") },
    { 768000,   TEXT("768 Kbps ") },
    { 512000,   TEXT("512 Kbps ") },
};

const unsigned char as_bps_q = 7; /// Fixed to good.

////////////////////////////////////////////////////////////////////////////////

ID2D1Factory*                   pDXFactory = NULL;

#ifdef USING_DC
ID2D1DCRenderTarget*            pDXTarget   = NULL;
HDC                             dxDC;
#else
ID2D1HwndRenderTarget*          pDXTarget   = NULL;
#endif

IWICImagingFactory*             pImgFactory = NULL;
IDWriteFactory*                 pWFactory   = NULL;

ID2D1SolidColorBrush*           pBlackBrush = NULL;
ID2D1SolidColorBrush*           pWhiteBrush = NULL;
ID2D1SolidColorBrush*           pMouseBrush = NULL;
ID2D1SolidColorBrush*           pLoadingBrush = NULL;
ID2D1SolidColorBrush*           pBackBrush  = NULL;
ID2D1LinearGradientBrush*       pTitleBrush  = NULL;
ID2D1GradientStopCollection*    pGradientStops = NULL;

#ifdef DEBUG
ID2D1SolidColorBrush*           pTraceBrush = NULL;
#endif /// of DEBUG

IDWriteTextFormat*              pTextFmtS = NULL;
IDWriteTextFormat*              pTextFmtM = NULL;
IDWriteTextFormat*              pTextFmtL = NULL;

float                           textszS   = 0;
float                           textszM   = 0;
float                           textszL   = 0;
float                           mouseRad  = 10.0f;
float                           mouseBrd  = 2.0f;

ID2D1Bitmap*                    pIbuttons[4] = {NULL};
ID2D1Bitmap*                    pItitlemenu[2] = {NULL};
ID2D1Bitmap*                    pIlogo = NULL;
ID2D1Bitmap*                    pIgear = NULL;
ID2D1Bitmap*                    pIclose = NULL;

ID2D1Bitmap*                    videodrawbuff = NULL;
ID2D1Bitmap*                    videodrawbuff_reload = NULL;
bool                            videodrawbuff_lock = false;

D2D1_RECT_F                     videobuffputrc = {0};

IDirectSoundBuffer*             pDSSndBuffBack;
IDirectSoundBuffer*             pDSSndBuffSelect;

DSBUFFERDESC                    dsBuffDescBack;
DSBUFFERDESC                    dsBuffDescSelect;
DSBUFFERDESC                    dsBuffStream[2];

HANDLE                          hDThread = INVALID_HANDLE_VALUE;
HANDLE                          hSThread = INVALID_HANDLE_VALUE;
HANDLE                          hCThread = INVALID_HANDLE_VALUE;
HANDLE                          hXThread = INVALID_HANDLE_VALUE;

RECT                            winRect = {0};
POINT                           msdragpt = {0};
POINT                           remmousepos = {0};

DWORD                           desktopWidth = 0;
DWORD                           desktopHeight = 0;
bool                            msdragging = false;
bool                            flagStopDThread = false;
bool                            flagStopSThread = false;
bool                            flagStopXThread = false;
bool                            gamemode = false;
bool                            viewmode = false;
bool                            videoBuffered = false;
bool                            rtspLastMode = -1;
bool                            rtspNeedReload = false;
bool                            rtspNeedReloadDisp = false;
bool                            rtspForceClosed = false;
int                             rtspConnected = 0;
bool                            ctrlConnected = false;
bool                            hidConnected = false;
float                           titlebarheight = 0;

ffplayerRTSP                    rtspp;
char                            rtsp_url[512] = {0};

DWORD                           dxinputDevices = DXINPUT_INIT_RESULT_NONE;

WSADATA                         wasD;
bool                            socket_ready = false;

LogTracer*                      logger = NULL;

#ifdef DEBUG
bool                            forceStopDebug = false;
bool                            drawTraceLines = false;
#endif /// of DEBUG

////////////////////////////////////////////////////////////////////////////////

int         msx         = 0;    /// GUI Mouse Coordination X
int         msy         = 0;    /// GUI Mouse Coordination Y
int         mslb        = 0;    /// GUI Left Mouse Button.
int         msmb        = 0;    /// GUI Middle Mouse button.
int         msrb        = 0;    /// GUI Right Mouse Button.

int         lastkeyup    = -1;  /// monitor key code for exit PLAY mode.
int         lastmouseup  = -1;  /// monitor mouse code for exit PLAY mode.

int         drawState   = 0;
int         menuSelect  = -1;
bool        rsvdQuit    = false;
bool        asipfound   = false;
char        asipnew[64];
bool        cfgSchIpSfx = false;
char        asipsfx[64];
bool        keycapmode  = false;

HINSTANCE   g_hinst     = NULL;
HWND        g_hwnd      = NULL;
HWND        g_hwndSubF  = NULL;
HWND        g_hwndConf  = NULL;
HWND        g_hwndSeach = NULL;

HANDLE      g_hMutex    = NULL;
HHOOK       hHook       = NULL;

TCHAR       g_myFName[MAX_PATH] = {0};

confDatas*  configuration = NULL;

////////////////////////////////////////////////////////////////////////////////

LRESULT CALLBACK DlgMsgLoop(HWND hwnd,UINT message,WPARAM wParam,LPARAM lParam);
LRESULT CALLBACK DlgConfProc(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK DlgSearchProc(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);

////////////////////////////////////////////////////////////////////////////////

bool CreateMainDlg(int x, int y, int w,int h,TCHAR *ClassName,void* DlgMsgLoop,void* hInst);

void DDDestroy();
void DDFlipBack(bool vw);
bool LoadPNG2DDBitmap( const TCHAR* pngfn, ID2D1Bitmap* &out );
void ClearD2Display(unsigned long Color);

////////////////////////////////////////////////////////////////////////////////

void killAllThreads();

DWORD WINAPI drawThread( LPVOID p );
DWORD WINAPI rtspconnectThread( LPVOID p );
DWORD WINAPI HIDsendThread( LPVOID p );
DWORD WINAPI controlThread( LPVOID p );
DWORD WINAPI findingipThread( LPVOID p );

////////////////////////////////////////////////////////////////////////////////

void scene_draw_loading( float angle );
void scene_menu_reset();
void scene_menu( int menustate );
void scene_sub( int menustate );
void scene_config();

////////////////////////////////////////////////////////////////////////////////

void rememberMouse2ProgCeter();

////////////////////////////////////////////////////////////////////////////////

void SetupWSA()
{
	if( WSAStartup( MAKEWORD(2, 2), &wasD ) == 0 )
	{
	    socket_ready = true;
	}
}

void CloseWSA()
{
    if ( socket_ready == true )
    {
        WSACleanup();
        socket_ready = false;
    }
}

bool DDCreate(HWND hwnd,int w,int h)
{
    pDXFactory = initializeDFactory( hwnd );

    if ( pDXFactory != NULL )
    {
        // Write Factory -------------------------------------------------------
        pWFactory = initializeWriteFactory();
        if ( pWFactory != NULL )
        {
            if ( configuration->fullscreen == 0 )
            {
                textszS = configuration->win_h / 60.0f;
                textszM = configuration->win_h / 50.0f;
                textszL = configuration->win_h / 35.0f;
                mouseRad = configuration->win_h / 72.0f;
            }
            else
            {
                textszS = desktopHeight / 60.0f;
                textszM = desktopHeight / 50.0f;
                textszL = desktopHeight / 35.0f;
                mouseRad = desktopHeight / 72.0f;
            }

            mouseBrd = mouseRad / 5.0f;

            if ( mouseBrd < 1.0f )
            {
                mouseBrd = 1.0f;
            }

            titlebarheight = textszS * 2.0f;

            TCHAR fontfacename[128] = {0};

            mbstowcs( fontfacename, configuration->fontname, 128 );

            pTextFmtS = createTextFormat( fontfacename, textszS );
            pTextFmtM = createTextFormat( fontfacename, textszM );
            pTextFmtL = createTextFormat( fontfacename, textszL );

            setTextFormat( pTextFmtS );
        }
#ifdef DEBUG
        else
        {
            if ( logger != NULL )
            {
                logger->write("Error: failed to create Write Factory\n");
            }
        }
#endif

        // DirectX target ------------------------------------------------------
#ifdef USING_DC
        dxDC = GetDC( hwnd );
        pDXTarget = initializeRenderTargetDC( dxDC );
#else
        pDXTarget = initializeRenderTargetHWND();
#endif
        if ( pDXTarget != NULL )
        {
#ifdef USING_DC
            RECT rc;
            GetClientRect( g_hwnd, &rc );
            pDXTarget->BindDC( dxDC, &rc );
#endif
            if ( configuration->cleartype > 0 )
            {
                pDXTarget->SetTextAntialiasMode( D2D1_TEXT_ANTIALIAS_MODE_CLEARTYPE );
            }
            else
            {
                pDXTarget->SetTextAntialiasMode( D2D1_TEXT_ANTIALIAS_MODE_GRAYSCALE );
            }

            pDXTarget->CreateSolidColorBrush( ColorF(ColorF::Black), &pBlackBrush );
            pDXTarget->CreateSolidColorBrush( ColorF(ColorF::White), &pWhiteBrush );
            pDXTarget->CreateSolidColorBrush( ColorF(1.0f,1.0f,1.0f,0.8f), &pMouseBrush );
            pDXTarget->CreateSolidColorBrush( ColorF(1.0f,1.0f,1.0f,0.6f), &pLoadingBrush );

#ifdef DEBUG
            pDXTarget->CreateSolidColorBrush( ColorF(ColorF::Red), &pTraceBrush );
#endif /// of DEBUG

            int refWidth = configuration->win_w;
            int refHeight = configuration->win_h;

            if ( configuration->fullscreen > 0 )
            {
                refWidth = desktopWidth;
                refHeight = desktopHeight;
            }

            //pBackBrush
            pDXTarget->CreateSolidColorBrush( ColorF(0.199f, 0.199f, 0.199f), &pBackBrush );

            //pTitleBrush
            D2D1_GRADIENT_STOP gradientStops[2];
            gradientStops[0].color = ColorF( ColorF::White, 0.5f);
            gradientStops[0].position = 0.0f;
            gradientStops[1].color = ColorF( ColorF::White, 0.0f);
            gradientStops[1].position = 1.0f;
            pDXTarget->CreateGradientStopCollection( gradientStops,
                                                     2,
                                                     D2D1_GAMMA_2_2,
                                                     D2D1_EXTEND_MODE_CLAMP,
                                                     &pGradientStops );

            pDXTarget->CreateLinearGradientBrush( LinearGradientBrushProperties( Point2F( refWidth / 2, 0),
                                                                                 Point2F( refWidth / 2, titlebarheight) ),
                                                  pGradientStops,
                                                  &pTitleBrush );
        }

        pImgFactory = initializeImageFactory();
        if ( pImgFactory != NULL )
        {
            loadPNGtoBitmap( TEXT("./res/menu1.png"), pIbuttons[0] );
            loadPNGtoBitmap( TEXT("./res/menu2.png"), pIbuttons[1] );
            loadPNGtoBitmap( TEXT("./res/menu3.png"), pIbuttons[2] );
            loadPNGtoBitmap( TEXT("./res/menu4.png"), pIbuttons[3] );
            loadPNGtoBitmap( TEXT("./res/play.png"), pItitlemenu[0] );
            loadPNGtoBitmap( TEXT("./res/view.png"), pItitlemenu[1] );
            loadPNGtoBitmap( TEXT("./res/a-streming.png"), pIlogo );
            loadPNGtoBitmap( TEXT("./res/quit.png"), pIclose );
            loadPNGtoBitmap( TEXT("./res/back-rotator.png"), pIgear );
        }

        // ---------------------------------------------------------------------
        // Ok, next : Sound !
        if ( initializeDSound( hwnd ) == true )
        {
            // Background music from : http://www.orangefreesounds.com/magic-music-loop/
            dsBuffDescBack.dwFlags = DSBCAPS_STATIC | DSBCAPS_LOCSOFTWARE;
            pDSSndBuffBack   = createDSBufferMP3( TEXT("./res/Magic-music-loop.mp3"), dsBuffDescBack );
            // select sound from: http://www.orangefreesounds.com/bleep-sound/
            pDSSndBuffSelect = createDSBufferMP3( TEXT("./res/Bleep-sound.mp3"), dsBuffDescSelect );

            if ( pDSSndBuffBack != NULL )
            {
                pDSSndBuffBack->Play( 0, 0, DSBPLAY_LOOPING );
            }
        }
        else
        {
            if ( logger != NULL )
            {
                logger->write("Error: DirectX sound initialize failure !\n");
            }
        }

        if ( logger != NULL )
        {
            logger->write("Initializing Direct X inputs ...\n");
        }

        bool useXinput = false;

        if ( configuration->joystick_type > 0 )
        {
            useXinput = true;

            if ( logger != NULL )
            {
                logger->write("#Notice,\n");
                logger->write("\tconfiguration set to use X-Input.\n");
            }
        }

        dxinputDevices = InitDirectInput( g_hwnd, useXinput );

        if ( logger != NULL )
        {
            if ( dxinputDevices == DXINPUT_INIT_RESULT_ALL )
            {
                logger->write(" All devices Ok.\n");
            }
            else
            {
                logger->write(" dxinput Keyboard =  %d\n", dxinputDevices & DXINPUT_INIT_RESULT_KEYBOARD );
                logger->write(" dxinput Mouse    =  %d\n", dxinputDevices & DXINPUT_INIT_RESULT_MOUSE );
                logger->write(" dxinput Joystick =  %d\n", dxinputDevices & DXINPUT_INIT_RESULT_JOYSTICK );
            }
        }

        return true;
    }

    return false;
}

void DDDestroy()
{
    // ------------
    // dxinput ...
    FreeDirectInput();

    // ------------
    // dxaudio ....
    if ( pDSSndBuffBack != NULL )
    {
        pDSSndBuffBack->Stop();
        SafeRelease( &pDSSndBuffBack );
    }

    if ( pDSSndBuffSelect != NULL )
    {
        pDSSndBuffSelect->Stop();
        SafeRelease( &pDSSndBuffSelect );
    }

    finalizeDSound();

    // ------------
    // images ....
    for( int cnt=0; cnt<4; cnt++ )
    {
        SafeRelease( &pIbuttons[cnt] );
    }

    for( int cnt=0; cnt<2; cnt++ )
    {
        SafeRelease( &pItitlemenu[cnt] );
    }

    SafeRelease( &pIlogo );
    SafeRelease( &pIgear );
    SafeRelease( &pIclose );

    // ------------
    // Brushes...
    SafeRelease( &pBlackBrush );
    SafeRelease( &pWhiteBrush );
    SafeRelease( &pMouseBrush );
    SafeRelease( &pLoadingBrush );
    SafeRelease( &pBackBrush );
    SafeRelease( &pTitleBrush );
    SafeRelease( &pGradientStops );

    // ------------
    // DxWrite ...
    SafeRelease( &pTextFmtS );
    SafeRelease( &pTextFmtM );
    SafeRelease( &pTextFmtL );

    // ------------
    // DxFactory ..
    if ( pDXFactory != NULL )
    {
        SafeRelease( &pDXFactory );
    }

}

void ClearD2Display( int state )
{
    if ( pDXTarget != NULL )
    {
        RECT rc;
        GetWindowRect( g_hwnd, &rc );
        D2D1_RECT_F frc = {0, 0, float( rc.right ), float( rc.bottom ) };

        if ( videoBuffered == true )
        {
            pDXTarget->FillRectangle( &frc, pBlackBrush );
        }
        else
        {
            pDXTarget->FillRectangle( &frc, pBackBrush );
        }
    }
}

void FinalizeMe()
{
    killAllThreads();
    DDDestroy();
    CloseWSA();
    ShowCursor(TRUE);

    saveConfig();
    finalConfig();

    if ( logger != NULL )
    {
        delete logger;
        logger = NULL;
    }

    ReleaseMutex( g_hMutex );

    // Finalizing askeyexchange
    finalkeyexchange();
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,LPSTR lpCmdLine,int nCmdShow)
{
#ifdef DEBUG
    printf("A-streaming Win32 client for DirectX 9&10, debug build | Ver.\n", VERSION_S);
    printf("(C)Copyleft 2016 rageworx@gmail.com\n");
    printf("\n");
    printf(" # parameters = %s\n", lpCmdLine );
#endif

    char* pOpt      = NULL;
    char* pOptArg   = NULL;

    // Get itself name.
    GetModuleFileName( NULL, g_myFName, MAX_PATH );

    // Check parameter "restart" ---
    pOpt = strstr( lpCmdLine, "--restart" );
    if ( pOpt != NULL )
    {
#ifdef DEBUG
        printf("-- kill me : restart flag on.\n");
#endif
        HWND hFound = FindWindow( TEXT(DEF_WINDOW_CLASSNAME), NULL );

        if ( hFound != NULL )
        {
            DWORD   procID = 0;
            HANDLE  hProc  = NULL;

            GetWindowThreadProcessId( hFound , &procID );
            if ( procID > 0 )
            {
                DWORD errID = 0;

                hProc = OpenProcess( PROCESS_TERMINATE, FALSE, procID );
                if ( hProc == NULL )
                {
                    // Failed to kill original process.
                    return 0;
                }
                TerminateProcess( hProc, TRUE );
                CloseHandle( hProc );

                hFound = FindWindow( TEXT(DEF_WINDOW_CLASSNAME), NULL );
                // Ok, Wait for it going to die...
                while( hFound != NULL )
                {
                    Sleep( 500 );
                    hFound = FindWindow( TEXT(DEF_WINDOW_CLASSNAME), NULL );
                }
            }
        }
    }

    // -------------------------------------------------------------------------
    // Create mutex.
    g_hMutex = CreateMutex( NULL, FALSE, TEXT(DEF_MUTEX_NAME) );

    if ( GetLastError() == ERROR_ALREADY_EXISTS )
    {
        CloseHandle( g_hMutex );

        HWND hFound = FindWindow( TEXT(DEF_WINDOW_CLASSNAME), NULL );

        if ( hFound != NULL )
        {
            SendMessage( hFound, WM_SHOWWINDOW, 0, 0 );
            BringWindowToTop( hFound );
            SendMessage( hFound, WM_SETFOCUS, 0, 0 );
        }

        return 0;
    }

    // check parameter in CmdLine ...
    pOpt = strstr( lpCmdLine, "--log" );


    if ( pOpt != NULL )
    {
        if ( strlen( pOpt ) > 5 )
        {
            if ( pOpt[5] == ':' )
            {
                pOptArg = &pOpt[6];
            }
        }

        if ( pOptArg != NULL )
        {
#ifdef UNICODE
            wchar_t tmpOptArgs[512] = {0};
            mbstowcs( tmpOptArgs, pOptArg, 512 );
            logger = new LogTracer( tmpOptArgs );
#else
            logger = new LogTracer( pOptArg );
#endif
        }
        else
        {
            logger = new LogTracer( NULL );
        }
    }

#ifdef DEBUG
    if ( logger == NULL )
    {
        logger = new LogTracer( __T("ascwin32_debug.log"), true );
    }
#endif

    MSG msg;

    desktopWidth  = GetSystemMetrics(SM_CXSCREEN);
    desktopHeight = GetSystemMetrics(SM_CYSCREEN);

    bool configloaded = false;

    if ( initConfig() == true )
    {
        if ( loadConfig() == true )
        {
            configuration = getConfig();

            if ( configuration != NULL )
            {
                if ( logger != NULL )
                {
                    logger->write("# Configuration loaded !\n");
                }

                configloaded = true;
            }
        }
        else
        {
            configuration = getDefaultConfig();
        }
    }

    if ( configloaded == false )
    {
        MessageBox( NULL,
                    TEXT("Configuration load failure, check file of config.xml in same folder.\nProgram not be able to executed."),
                    TEXT("Config load failure"),
                    MB_ICONERROR );
        return -1;
    }

    // Initializing key exchanges ...
    initkeyexchange();

    // Calcualting screen size ...

    if ( ( configuration->win_w > desktopWidth ) || ( configuration->win_h > desktopHeight ) )
    {
        configuration->win_x = 0;
        configuration->win_y = 0;
        configuration->win_h = desktopHeight;
        configuration->win_w = desktopWidth;
    }

    CreateMainDlg( configuration->win_x,
                   configuration->win_y,
                   configuration->win_w,
                   configuration->win_h,
                   TEXT(DEF_WINDOW_CLASSNAME),
                   (void*)DlgMsgLoop,
                   hInstance );

    if( g_hwnd == NULL )
    {
        MessageBox( NULL,
                    TEXT("Creating a main window not possible in this time,\nCheck your Windows system."),
                    TEXT("Error"),
                    MB_ICONERROR );
        return -2;
    }

    g_hinst = hInstance;

    SetupWSA();

    GetWindowRect( g_hwnd, &winRect );

    if ( DDCreate( g_hwnd, configuration->win_w, configuration->win_h ) == false )
    {
        MessageBox( g_hwnd,
                    TEXT("Failed to create DirectX 2D surface.\nCheck your system supports DirectX 10 or above."),
                    TEXT("DirectX error."),
                    MB_ICONERROR );
        return -1;
    }

    hDThread = CreateThread( NULL, 0, drawThread, NULL, 0, NULL );

    if ( hDThread == INVALID_HANDLE_VALUE )
    {
        MessageBox( g_hwnd,
                    TEXT("Application could not started to a processing for rendering."),
                    TEXT("Error"),
                    MB_ICONERROR );

        return -2;
    }

    while(1)
    {
        if(PeekMessage(&msg,NULL,0,0,PM_NOREMOVE))
        {
            if(!GetMessage(&msg,NULL,0,0)) return msg.wParam;
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
        else
        {
            WaitMessage();
        }
    }

    FinalizeMe();

    return 1;
}

bool CreateMainDlg(int x, int y, int w,int h,TCHAR *ClassName,void* DlgMsgLoop,void* hInst)
{
    WNDCLASS wc;

    if ( configuration->fullscreen == 0 )
    {
        wc.style = CS_HREDRAW | CS_VREDRAW | CS_DROPSHADOW;
    }
    else
    {
        wc.style = CS_HREDRAW | CS_VREDRAW;
    }

    //wc.lpfnWndProc      = (long (__stdcall *)(struct HWND__ *,unsigned int,unsigned int,long))DlgMsgLoop;
    wc.lpfnWndProc      = (WNDPROC)DlgMsgLoop;
    wc.cbClsExtra       = 0;
    wc.cbWndExtra       = 0;
    wc.hInstance        = (HINSTANCE)hInst;
    wc.hIcon            = LoadIcon((HINSTANCE)hInst,(const TCHAR *)IDI_APPLICATION);
    wc.hCursor          = LoadCursor(NULL,IDC_ARROW);
    wc.hbrBackground    = NULL;
    wc.lpszClassName    = ClassName;
    wc.lpszMenuName     = NULL;

    RegisterClass(&wc);

    int override_x = x;
    int override_y = y;
    int override_w = w;
    int override_h = h;

    if ( configuration->fullscreen > 0 )
    {
        override_x = 0;
        override_y = 0;
        override_w = desktopWidth;
        override_h = desktopHeight;
    }

    // direct2D dosen't using standard window GUI components,
    // I just open borderless window.
    g_hwnd = CreateWindowEx( WS_EX_APPWINDOW,
                             ClassName,
                             ClassName,
                             WS_POPUP,
                             override_x,
                             override_y,
                             override_w,
                             override_h,
                             NULL,
                             NULL,
                             (struct HINSTANCE__ *)hInst,
                             NULL );

    if( g_hwnd == NULL )
        return FALSE;

    ShowWindow( g_hwnd, TRUE );
    UpdateWindow( g_hwnd );
    SetFocus( g_hwnd );
    ShowCursor( FALSE );

    if ( configuration->fullscreen > 0 )
    {
        //Make window to TOPMOST !
        g_hwndSubF = HWND_TOPMOST;

    }

    // Check window position...
    if ( configuration->fullscreen == 0 )
    {
        if ( ( override_x + override_w ) > desktopWidth )
        {
            override_x = 0;
            configuration->win_x = 0;
        }

        if ( ( override_y + override_h ) > desktopHeight )
        {
            override_y = 0;
            configuration->win_y = 0;
        }
    }

    SetWindowPos( g_hwnd,
                  g_hwndSubF,
                  override_x,
                  override_y,
                  override_w,
                  override_h,
                  0 );

    // Let mouse cursor to center.
    rememberMouse2ProgCeter();

    return TRUE;
}


/////////////////////////////////////////////////////////////////

void DlgMouseDrag( bool enable )
{
    if ( configuration->fullscreen > 0 )
        return;

    if ( enable == true )
    {
        GetWindowRect( g_hwnd, &winRect );
        GetCursorPos( &msdragpt );
        SetCapture( g_hwnd );
        msdragging = true;
    }
    else
    {
        if ( msdragging == true )
        {
            msdragging = false;
            ReleaseCapture();
        }
    }
}

LRESULT CALLBACK DlgMsgLoop(HWND hwnd,UINT message,WPARAM wParam,LPARAM lParam)
{
    switch(message)
    {
        case WM_SIZE:
            {
                if ( configuration->fullscreen == 0 )
                {
                    UINT width = LOWORD(lParam);
                    UINT height = HIWORD(lParam);

                    if ( pDXTarget != NULL )
                    {
                        D2D1_SIZE_U size = SizeU( width, height );
#ifndef USING_DC
                        pDXTarget->Resize( size );
#endif
                    }

                    if ( msdragging == false )
                    {
                        SetWindowPos( hwnd,
                                      g_hwndSubF,
                                      winRect.left ,
                                      winRect.top,
                                      width,
                                      height,
                                      0 );
                    }
                }

                InvalidateRect(hwnd, NULL, FALSE);

                return 0;
            }
            break;

        case WM_DISPLAYCHANGE:
            InvalidateRect(hwnd, NULL, FALSE);
            return 0;
            break;

        case WM_MOUSEMOVE:
            msx = LOWORD(lParam);
            msy = HIWORD(lParam);

            if ( configuration->fullscreen == 0 )
            {
                if ( ( mslb == 0 ) && ( msdragging == true ) )
                {
                    POINT mspt;
                    GetCursorPos( &mspt );

                    int newx = winRect.left + mspt.x - msdragpt.x;
                    int newy = winRect.top + mspt.y - msdragpt.y;

                    SetWindowPos( hwnd,
                                  g_hwndSubF,
                                  newx,
                                  newy,
                                  configuration->win_w,
                                  configuration->win_h,
                                  0 );

                    configuration->win_x = newx;
                    configuration->win_y = newy;
                }
            }
            break;

        case WM_LBUTTONDOWN:
            {
                mslb = 1;
                if ( configuration->fullscreen == 0 )
                {
                    bool dragf = false;

                    if ( viewmode == true )
                    {
                        if ( msx < ( configuration->win_w * 0.9f ) )
                        {
                            dragf = true;
                        }
                    }
                    else
                    if ( msy < titlebarheight )
                    {
                        dragf = true;
                    }

                    if ( dragf == true )
                    {
                        mslb = 0;
                        DlgMouseDrag( true );
                    }
                }
            }
            break;

        case WM_RBUTTONDOWN:
            msrb = 1;
            DlgMouseDrag( false );
            break;

        case WM_MBUTTONDOWN:
            msmb = 1;
            DlgMouseDrag( false );
            break;

        case WM_LBUTTONUP:
            mslb = 0;
            DlgMouseDrag( false );
            break;

        case WM_RBUTTONUP:
            msrb = 0;
            DlgMouseDrag( false );
            break;

        case WM_MBUTTONUP:
            msmb = 0;
            DlgMouseDrag( false );
            break;

        case WM_KEYUP:
            lastkeyup = wParam;
#ifdef DEBUG
            if ( wParam == 'L' )
            {
                if ( drawTraceLines == false )
                {
                    drawTraceLines = true;
                }
                else
                {
                    drawTraceLines = false;
                }
            }
            else
            if ( wParam == 'R' )
            {
                if ( rtspConnected > 0 )
                {
                    // Check conditions ...

                    if ( ( rtspNeedReload == false ) &&
                         ( videoBuffered == true ) )
                    {
                        if ( videodrawbuff != NULL )
                        {
                            videodrawbuff_lock = true;

                            if ( videodrawbuff_reload != NULL )
                            {
                                SafeRelease( &videodrawbuff_reload );
                            }

                            int width  = rtspp.GetVideoWidth();
                            int height = rtspp.GetVideoHeight();

                            CloneBitmap( videodrawbuff,
                                         width,
                                         height,
                                         videodrawbuff_reload );

                            videodrawbuff_lock = false;

                            rtspNeedReload = true;
                        }

                        rtspNeedReload = true;
                    }
                }

            }
            else
            if ( wParam == 'Q' )
            {
                if ( rtspConnected >= 0 )
                {
                    if ( forceStopDebug == false )
                    {
                        forceStopDebug = true;
                    }
                }
            }
#endif /// of DEBUG
            break;

        case WM_DESTROY:
            {
                FinalizeMe();

                PostQuitMessage(0);
            }
            break;
    }

    return DefWindowProc(hwnd,message,wParam,lParam);
}

void calcimgputrc( D2D1_RECT_F* rcf, int x, int y, int w, int h )
{
    if ( rcf != NULL )
    {
        rcf->left   = float( x );
        rcf->top    = float( y );
        rcf->right  = rcf->left + float( w );
        rcf->bottom = rcf->top  + float( h );
    }
}

bool checkmouseinrc( D2D1_RECT_F* rcf )
{
    float msxf = float( msx );
    float msyf = float( msy );

    if ( ( msxf > rcf->left ) && ( msxf < rcf->right ) )
        if ( ( msyf > rcf->top ) && ( msyf < rcf->bottom) )
            return true;

    return false;
}

int dxkeystoanystreamkeys(KEYSTATE* ks, unsigned char* mods, unsigned char* keys)
{
    if ( ks == NULL )
        return 0;

    int     keyque = 0;


    for ( int cnt=0; cnt<ks->length; cnt++ )
    {
        switch( ks->keys[cnt] )
        {
            // ---------------
            // Mods keys (not normal keys for HID)
            case DIK_LCONTROL:
                *mods |= ANYSTREAM_KEYMASK_LEFT_CTRL;
                break;

            case DIK_LSHIFT:
                *mods |= ANYSTREAM_KEYMASK_LEFT_SHIFT;
                break;

            case DIK_LMENU:
                *mods |= ANYSTREAM_KEYMASK_LEFT_ALT;
                break;

            case DIK_LWIN:
                *mods |= ANYSTREAM_KEYMASK_LEFT_GUI;
                break;

            case DIK_RCONTROL:
                *mods |= ANYSTREAM_KEYMASK_RIGHT_CTRL;
                break;

            case DIK_RSHIFT:
                *mods |= ANYSTREAM_KEYMASK_RIGHT_SHIFT;
                break;

            case DIK_RMENU:
                *mods |= ANYSTREAM_KEYMASK_RIGHT_ALT;
                break;

            case DIK_RWIN:
                *mods |= ANYSTREAM_KEYMASK_RIGHT_GUI;
                break;

            default:
                if ( keyque < 6 )   /// A-streaming` HID can not control over 6 keys.
                {
                    keys[keyque] = convert_dik_to_ashidkey( ks->keys[cnt] );
                    keyque++;
                }
                break;
        }
    }

    return keyque;
}

void rememberMouse2ProgCeter()
{
    RECT wrc = {0};
    GetWindowRect( g_hwnd, &wrc );

    int new_msx = wrc.left + ( ( wrc.right - wrc.left ) / 2 );
    int new_msy = wrc.top  + ( ( wrc.bottom - wrc.top ) / 2 );

    SetCursorPos( new_msx, new_msy );

    // Let remember current mouse pointer position.
    GetCursorPos( &remmousepos );
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

void killAllThreads()
{
    if ( hCThread != INVALID_HANDLE_VALUE )
    {
        TerminateThread( hCThread, 0 );
        WaitForSingleObject( hCThread, 5000 );
        CloseHandle( hCThread );
        hCThread = INVALID_HANDLE_VALUE;
    }

    flagStopDThread = true;
    flagStopSThread = true;

    // Just wait for thread flag applied.
    Sleep(500);

    if ( hDThread != INVALID_HANDLE_VALUE )
    {
        TerminateThread( hDThread, 0 );
        CloseHandle( hDThread );
        hDThread = INVALID_HANDLE_VALUE;
    }

    if ( hSThread != INVALID_HANDLE_VALUE )
    {
        TerminateThread( hSThread, 0 );
        CloseHandle( hSThread );
        hSThread = INVALID_HANDLE_VALUE;
    }
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

static int      putwh  = 700;
static int      dstwh  = 200;
static float    oppa   = 0.0f;
static int      putgap = 30;
static int      cornergap = 10;

static int      dstx  = 0;
static int      dsty  = 0;
static int      putx  = 0;
static int      puty  = 0;

static int      dstx_logo  = 0;
static int      dsty_logo  = 0;
static int      putwh_logo = 200;

static int      dstx_close = 0;
static int      dsty_close = 0;
static int      putwh_close = 100;

static int      dstx_titlethumb = 0;
static int      dsty_titlethumb = 0;
static int      putwh_titlethumb = 100;

static int      loadring_thick = 10;

////////////////////////////////////////////////////////////////////////////////

DWORD WINAPI drawThread( LPVOID p )
{
    scene_menu_reset();

    while( flagStopDThread == false )
    {

        bool wait4DrawSync = false;

        if ( pDXTarget != NULL )
        {
            pDXTarget->BeginDraw();

            ClearD2Display( drawState );

            // Draw ...
            switch ( drawState )
            {
                case 0:
                    scene_menu( 0 );
                    wait4DrawSync = true;
                    break;

                case 1:
                    scene_menu( 1 );
                    wait4DrawSync = true;
                    break;

                case 2:
                    if ( pDSSndBuffBack != NULL )
                    {
                        pDSSndBuffBack->Stop();
                    }
                    scene_sub( menuSelect );
                    break;

                case 3:
                    scene_config();
                    break;

                case 4:
                    break;

                case 999:   /// Quit;
                    break;

                default:
                    break;
            }

            // Check Menu selected...
            if ( ( drawState == 0 ) && ( menuSelect >= 0 ) )
            {
                switch( menuSelect )
                {
                    case 0: /// Game mode.
                        drawState = 1;
                        break;

                    case 1: /// View mode
                        drawState = 1;
                        break;

                    case 2:
                        drawState = 3;
                        break;

                    case 3:
                        rsvdQuit = true;
                        drawState = 1;
                        break;

                }
            }

            // putting base...
            int p_x = configuration->win_w / 320;
            int p_y = configuration->win_h / 200;
            if ( configuration->fullscreen > 0 )
            {
                p_x = desktopWidth / 320;
                p_y = desktopHeight / 200;
            }

            // Draw Title ...
            int d_w = configuration->win_w;
            int d_h = configuration->win_h;
            if ( configuration->fullscreen > 0 )
            {
                d_w = desktopWidth;
                d_h = desktopHeight;
            }

            if ( ( viewmode == false) && ( gamemode == false ) )
            {
                D2D1_RECT_F tmp_rc = {0, 0, d_w , titlebarheight };
                pDXTarget->FillRectangle( &tmp_rc, pTitleBrush );

                drawText( p_x, p_y,
                          TEXT("A-streaming Client - Win32 / DirectX / MinGW - Ver." VERSION_S "[" VERSION_P "]" ),
                          255,255,255, oppa );

#ifdef DEBUG
                if ( drawTraceLines == true )
                {
                    drawText( p_x, p_y + textszS, TEXT("-- DEBUG DRAWING TRACE ON --"), 255, 0, 0, 1.0f );
                }
#endif /// of DEBUG

                int putxty = d_h -  textszS * 5;

                drawText( p_x, putxty,
                         TEXT("libffmpeg is a part of open source, https://www.ffmpeg.org/, used version "FFMPEG_VERSION" with "YASM_VERISON),
                         255, 255, 255, oppa * 0.4f );

                putxty += textszS;

                drawText( p_x, putxty,
                          TEXT("libTinyXML is a part of open source, http://tinyxpath.sourceforge.net/, used version 2.6.2"),
                          255, 255, 255, oppa * 0.4f );

                putxty += textszS;

                drawText( p_x, putxty,
                          TEXT("Used song & sound copyrighted to each, Magic Music (Alexander Blu), Bleep sound (Orange Free Sounds) from http://www.orangefreesounds.com/"),
                          255, 255, 255, oppa * 0.4f );

                putxty += textszS;

                drawText( p_x, putxty,
                          TEXT("(C)Copyrleft 2016 Private build for A-streaming by rageworx@gmail.com"),
                          255, 255, 255, oppa * 0.4f );
            }

            // Draw Mouse Pointer ..
            if ( gamemode == false )
            {
                static int prev_msx = 0;
                static int prev_msy = 0;
                static unsigned msx_hide_cnt = 0;

                bool drawMouse = true;

                if ( viewmode == true )
                {
                    if ( ( prev_msx == msx ) && ( prev_msy == msy ) )
                    {
                        msx_hide_cnt++;
                    }
                    else
                    {
                        prev_msx = msx;
                        prev_msy = msy;
                        msx_hide_cnt = 0;
                    }

                    if ( msx_hide_cnt > DEF_MOUSE_VIEWMODE_HIDE_COUNT )
                    {
                        drawMouse = false;
                    }
                }
                else
                {
                    prev_msx = msx;
                    prev_msy = msy;
                    msx_hide_cnt = 0;
                }

                if ( drawMouse == true )
                {
                    pDXTarget->SetTransform( Matrix3x2F::Identity() );
                    pDXTarget->DrawEllipse( Ellipse( Point2F( float( msx ), float( msy ) ),
                                                             mouseRad,
                                                             mouseRad),
                                                             pMouseBrush,
                                                             mouseBrd );
                }
            }

            pDXTarget->EndDraw();

            // Check reload !
            if ( ( drawState == 0 ) && ( rtspNeedReload == true ) )
            {
                if ( rtspLastMode > 0 )
                {
                    bool redo = false;

                    // Check threads ...
                    if ( ( hXThread == INVALID_HANDLE_VALUE ) &&
                         ( hSThread == INVALID_HANDLE_VALUE ) &&
                         ( hCThread == INVALID_HANDLE_VALUE ) )
                    {
                        redo = true;
                    }

                    if ( redo == true  )
                    {
                        rtspNeedReloadDisp = true;
                        // It must be rest about 1 sec.
                        // I don't know why, but stream open gap need this .
                        menuSelect = rtspLastMode;
                        rtspLastMode = -1;
                    }
                }
            }
        }

        // Give thread sleep 0 for tasking to other window.
#ifdef USING_DC
    if ( wait4DrawSync == true )
    {
        Sleep(10);
    }
    else
#endif
        Sleep(1);
    }

    return 0;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

void scene_menu_reset()
{
    oppa  = 0.0f;

    int total_width  = configuration->win_w;
    int total_height = configuration->win_h;

    if ( configuration->fullscreen > 0 )
    {
        total_width = desktopWidth;
        total_height = desktopHeight;
    }

    putgap = total_width / 43.0f;
    cornergap = total_width / 128.0f;

    putwh = total_height * 0.8;
    dstwh = total_height * 0.2;

    dstx  = ( total_width - ( ( dstwh + putgap ) * 4 ) - ( putgap * 1.5 ) ) / 4 ;
    dsty  = ( total_height - dstwh ) / 2;

    putwh_logo  = total_height * 0.2;
    putwh_close = total_height * 0.05;

    dstx_logo  = ( total_width - putwh_logo ) / 2;
    dsty_logo  = ( ( total_height - putwh_logo ) / 2 - putwh_logo ) - ( putwh_logo * 0.2f ) - putgap;

    dstx_close = total_width - putwh_close  - cornergap;
    dsty_close = cornergap;

    putwh_titlethumb = total_height * 0.1f;

    dstx_titlethumb = cornergap;
    dsty_titlethumb = cornergap;
    loadring_thick  = cornergap * 4.0f;

    putx = ( dstx * 2 );
    puty = - ( dsty * 2 );
}

void scene_draw_loading( float angle )
{
    // Draw a text 'Reloading' if state mean it.
    if ( rtspNeedReloadDisp == true )
    {
        if ( ( videodrawbuff_reload != NULL ) && ( videodrawbuff_lock == false ) )
        {
            pDXTarget->DrawBitmap( videodrawbuff_reload,
                                   videobuffputrc,
                                   0.5f,
                                   D2D1_BITMAP_INTERPOLATION_MODE_LINEAR,
                                   NULL );
        }
    }


    float reangle = angle;

    float dst_w = float( configuration->win_w );
    float dst_h = float( configuration->win_h );

    if ( configuration->fullscreen > 0 )
    {
        dst_w = desktopWidth;
        dst_h = desktopHeight;
    }

    float height = dst_h / 10.0f;
    float put_w = dst_w - height - 10;
    float put_h = dst_h - height - 10;

    ID2D1PathGeometry* geomArc = NULL;

    pDXFactory->CreatePathGeometry( &geomArc );

    drawPie( geomArc,
             Point2F( put_w, put_h ),
             reangle,
             100.0f,
             height,
             height - loadring_thick );

    pDXTarget->FillGeometry( geomArc, pLoadingBrush );

    SafeRelease( &geomArc );

}

void scene_menu( int menustate )
{
    int startx = putx;
    int starty = puty;

    float oppa_bonus = 0.0f;
#ifdef DEBUG
    float total_w = configuration->win_w;
    float total_h = configuration->win_h;
#endif /// of DEBUG

    static int   gearput   = configuration->win_h * 1.2;

    if ( configuration->fullscreen > 0 )
    {
        gearput = desktopHeight * 1.2;
#ifdef DEBUG
        total_w = desktopWidth;
        total_h = desktopHeight;
#endif /// of DEBUG
    }

    static float gearangle = 0.0f;
    static D2D1_RECT_F gearrc = { 0, 0, gearput, gearput };

    D2D1_RECT_F imgputrc = {0};

    pDXTarget->SetTransform( Matrix3x2F::Translation(- (gearput / 2 ), - (gearput/2) ) *
                             Matrix3x2F::Rotation( gearangle ) *
                             Matrix3x2F::Translation( gearput/3, gearput/3 ) );
    pDXTarget->DrawBitmap( pIgear, gearrc, oppa / 3, D2D1_BITMAP_INTERPOLATION_MODE_LINEAR, NULL );
    gearangle += 0.1f;
    if ( gearangle > 360.0f )
    {
        gearangle = 0.0f;
    }
    pDXTarget->SetTransform( Matrix3x2F::Rotation( 0 ) );

    calcimgputrc( &imgputrc, dstx_logo, dsty_logo, putwh_logo, putwh_logo );
    if ( pIlogo != NULL)
    {
        pDXTarget->DrawBitmap( pIlogo,
                               imgputrc,
                               oppa * 1.2f,
                               D2D1_BITMAP_INTERPOLATION_MODE_LINEAR, NULL );
    }

    calcimgputrc( &imgputrc, startx, starty, putwh, putwh );
    if ( pIbuttons[0] != NULL )
    {
        if ( checkmouseinrc( &imgputrc ) == true )
        {
            oppa_bonus += 0.2f;

            if ( ( mslb == 1 ) && ( menustate == 0 ) )
            {
                // Play mode
                menuSelect = 0;
                rtspLastMode = 1;

                if ( pDSSndBuffSelect != NULL )
                {
                    pDSSndBuffSelect->Play(0,0,0);
                }
            }
        }

        pDXTarget->DrawBitmap( pIbuttons[0],
                               imgputrc,
                               oppa + oppa_bonus,
                               D2D1_BITMAP_INTERPOLATION_MODE_LINEAR,
                               NULL );

        if ( oppa_bonus > 0.0f )
        {
            oppa_bonus = 0.0f;
        }

#ifdef DEBUG
        if ( drawTraceLines == true )
        {
            pDXTarget->DrawRectangle( imgputrc, pTraceBrush );
        }
#endif /// of DEBUG
    }

    startx += putwh + putgap;

    calcimgputrc( &imgputrc, startx, starty, putwh, putwh );
    if ( pIbuttons[1] != NULL )
    {
        if ( checkmouseinrc( &imgputrc ) == true )
        {
            oppa_bonus += 0.2f;

            if ( ( mslb == 1 ) && ( menustate == 0 ) )
            {
                // view mode.
                menuSelect = 1;
                rtspLastMode = 2;

                if ( pDSSndBuffSelect != NULL )
                {
                    pDSSndBuffSelect->Play(0,0,0);
                }
            }
        }

        pDXTarget->DrawBitmap( pIbuttons[1],
                               imgputrc,
                               oppa + oppa_bonus,
                               D2D1_BITMAP_INTERPOLATION_MODE_LINEAR,
                               NULL );

        if ( oppa_bonus > 0.0f )
        {
            oppa_bonus = 0.0f;
        }
#ifdef DEBUG
        if ( drawTraceLines == true )
        {
            pDXTarget->DrawRectangle( imgputrc, pTraceBrush );
        }
#endif /// of DEBUG
    }

    startx += putwh + putgap;

    calcimgputrc( &imgputrc, startx, starty, putwh, putwh );
    if ( pIbuttons[2] != NULL )
    {
        if ( checkmouseinrc( &imgputrc ) == true )
        {
            oppa_bonus += 0.2f;

            if ( ( mslb == 1 ) && ( menustate == 0 ) )
            {
                menuSelect = 2;
                rtspLastMode = -1;

                if ( pDSSndBuffSelect != NULL )
                {
                    pDSSndBuffSelect->Play(0,0,0);
                }
            }
        }

        pDXTarget->DrawBitmap( pIbuttons[2],
                               imgputrc,
                               oppa + oppa_bonus,
                               D2D1_BITMAP_INTERPOLATION_MODE_LINEAR,
                               NULL );

        if ( oppa_bonus > 0.0f )
        {
            oppa_bonus = 0.0f;
        }
#ifdef DEBUG
        if ( drawTraceLines == true )
        {
            pDXTarget->DrawRectangle( imgputrc, pTraceBrush );
        }
#endif /// of DEBUG
    }

    startx += putwh + putgap;

    calcimgputrc( &imgputrc, startx, starty, putwh, putwh );
    if ( pIbuttons[3] != NULL )
    {
        if ( checkmouseinrc( &imgputrc ) == true )
        {
            oppa_bonus += 0.2f;

            if ( ( mslb == 1 ) && ( menustate == 0 ) )
            {
                menuSelect = 3;
                rtspLastMode = -1;

                if ( pDSSndBuffSelect != NULL )
                {
                    pDSSndBuffSelect->Play(0,0,0);
                }
            }
        }

        pDXTarget->DrawBitmap( pIbuttons[3],
                               imgputrc,
                               oppa + oppa_bonus,
                               D2D1_BITMAP_INTERPOLATION_MODE_LINEAR,
                               NULL );

        if ( oppa_bonus > 0.0f )
        {
            oppa_bonus = 0.0f;
        }
#ifdef DEBUG
        if ( drawTraceLines == true )
        {
            pDXTarget->DrawRectangle( imgputrc, pTraceBrush );
        }
#endif /// of DEBUG
    }

    if ( putwh > dstwh )
    {
        putwh -= ( putwh - dstwh ) / 20;
        if ( putwh < dstwh )
        {
            putwh = dstwh;
        }

    }

#ifdef DEBUG
    if ( drawTraceLines == true )
    {
        D2D_POINT_2F ptStart = { total_w / 2.0f, 0 };
        D2D_POINT_2F ptEnd   = { total_w / 2.0f, total_h };

        pDXTarget->DrawLine( ptStart, ptEnd, pTraceBrush );
    }
#endif /// of DEBUG


    if ( menustate == 0 )
    {
        if ( oppa < 0.8f )
        {
            oppa += 0.01f;
        }

        if ( putx < dstx )
        {
            if ( ( dstx - putx ) > 0 )
                putx += ( dstx - putx ) / 10;
            else
                putx -= ( dstx - putx ) / 10;
        }
        else
        if ( putx > dstx )
        {
            putx -=  ( dstx - dstx ) / 10;
        }

        if ( puty < dsty )
        {
            puty += ( dsty - puty ) / 10;
        }
        else
        if ( puty > dsty )
        {
            puty -= ( puty - dstx ) / 10;
        }
    }
    else
    {
        if ( oppa > 0.0f )
        {
            oppa -= 0.1f;
            if ( oppa < 0.0f )
            {
                oppa = 0.0f;
            }
        }
        else
        {
            drawState++;

            if ( rsvdQuit == true )
            {
                SendMessage( g_hwnd, WM_CLOSE, 0, 0 );
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////

#define DEF_VIDEO_TH_CAP        0.02f

void scene_sub( int menustate )
{
    static bool     scene_sub_exit = false;
    static int      scene_sub_count = 0;
    static unsigned countout = 0;
    static float    imgtransp = 0.0f;
    static int      scene_sub_flag = 0;
    static float    loading_ang = 0.0f;
    static float    thumb_oppa = 1.0f;

    if ( ( menustate == 0 ) || ( menustate == 1 ) )
    {
        bool needNewControlSockThread = false;

        if ( ( menustate == 0 ) &&
             ( scene_sub_exit == false ) &&
             ( gamemode == false ) )
        {
            gamemode = true;

            setTextFormat( pTextFmtL );

            // Set mouse position to window center if gamemode.
            rememberMouse2ProgCeter();

            flagStopSThread = false;
            hSThread = CreateThread( NULL, 0, HIDsendThread, NULL, 0, NULL );
            if ( hSThread == INVALID_HANDLE_VALUE )
            {
                ShowCursor( TRUE );
                MessageBox( g_hwnd,
                            TEXT("Application could not started to a processing for control."),
                            TEXT("Error"),
                            MB_ICONERROR );
                ShowCursor( FALSE );
            }

            needNewControlSockThread = true;
        }
        else
        if ( ( menustate == 1 ) &&
             ( scene_sub_exit == false ) &&
             ( viewmode == false ) )
        {
            viewmode = true;

            setTextFormat( pTextFmtL );

            needNewControlSockThread = true;
        }

        if ( needNewControlSockThread == true )
        {
            flagStopXThread = false;
            hXThread = CreateThread( NULL, 0, controlThread, NULL, 0, NULL );
            if ( hXThread == INVALID_HANDLE_VALUE )
            {
                ShowCursor( TRUE );
                MessageBox( g_hwnd,
                            TEXT("A-streaming control part is not allowed.\n Check system !"),
                            TEXT("Error"),
                            MB_ICONERROR );
                ShowCursor( FALSE );
            }
        }

        switch( scene_sub_count )
        {
            case 0:
                if ( scene_sub_flag == 0 )
                {
                    if ( rtspNeedReload == false )
                    {
                        if ( rtspNeedReloadDisp == true )
                        {
                            rtspNeedReloadDisp = false;
                        }
                    }

                    rtspConnected = 0;
                    scene_sub_flag = 1;

                    if ( hCThread == INVALID_HANDLE_VALUE )
                    {
                        hCThread = CreateThread( NULL, 0, rtspconnectThread, NULL, 0, NULL );
                    }
                }

                if( rtspConnected > 0 )
                {
                    scene_sub_count++;
                }
                else
                {
                    loading_ang += 2.5f;

                    if ( loading_ang >= 360.0f )
                    {
                        loading_ang = 0.0f;
                    }

                    scene_draw_loading( loading_ang );

                    countout++;
                }

                if ( rtspConnected < 0 )
                {
                    /*
                    ShowCursor( TRUE );
                    MessageBox( g_hwnd,
                                TEXT("Can not reach to A-streamin, Check network."),
                                TEXT("Error"),
                                MB_ICONERROR );
                    ShowCursor( FALSE );
                    */
                    scene_sub_exit = true;
                }
                break;

            case 1:
                if ( scene_sub_flag != 2 )
                {
                    scene_sub_flag = 2;
                }

                // Check stream reload command camed ...
                if ( rtspNeedReload == true )
                {
                    scene_sub_exit = true;
                    rtspNeedReloadDisp = true;
                    break;
                }

                // Check connection closed by server
                if ( rtspForceClosed == true )
                {
                    scene_sub_exit = true;
                    break;
                }

                ID2D1Bitmap* newdrawbuff = NULL;

                if ( ctrlConnected == true )
                {
                    newdrawbuff = rtspp.getRenderBuffer();
                }

                if ( newdrawbuff != NULL )
                {
                    if ( videodrawbuff_lock == false )
                    {
                        if ( videodrawbuff != NULL )
                        {
                            SafeRelease( &videodrawbuff );
                        }

                        videodrawbuff = newdrawbuff;

                        if ( videoBuffered == false )
                        {
                            videoBuffered = true;
                        }

                        if ( imgtransp < 1.0f )
                        {
                            imgtransp += DEF_VIDEO_TH_CAP;
                        }

                        countout = 0;
                    }
                }
                else
                {
                    loading_ang += 2.5f;

                    if ( loading_ang >= 360.0f )
                    {
                        loading_ang = 0.0f;
                    }

                    scene_draw_loading( loading_ang );

                    countout++;

                    if ( imgtransp < 0.5f )
                    {
                        imgtransp -= DEF_VIDEO_TH_CAP;
                    }
                }

                if ( videodrawbuff != NULL )
                {
                    if ( gamemode == true )
                    {
                        if ( thumb_oppa > 0.1f )
                        {
                            thumb_oppa -= 0.01f;
                        }
                    }
                    else
                    {
                        if ( thumb_oppa > 0.0f )
                        {
                            thumb_oppa -= 0.01f;
                        }
                    }

                    videobuffputrc.left   = 0;
                    videobuffputrc.top    = 0;
                    videobuffputrc.right  = configuration->win_w;
                    videobuffputrc.bottom = configuration->win_h;

                    if ( configuration->fullscreen > 0 )
                    {
                        videobuffputrc.right  = desktopWidth;
                        videobuffputrc.bottom = desktopHeight;
                    }

                    double max_w = videobuffputrc.right;
                    double max_h = videobuffputrc.bottom;

                    // let calc keep ratio of video ...
                    float img_width  = float( rtspp.GetVideoWidth() );
                    float img_height = float( rtspp.GetVideoHeight() );

                    double ratio = 0.0f;

                    if ( img_width != max_w )
                    {
                        ratio = max_w / img_width;
                        if ( ratio > 0.0f )
                        {
                            videobuffputrc.right  = img_width * ratio;
                            videobuffputrc.bottom = img_height * ratio;
                        }
                    }

                    // and ... center alignment !

                    if ( videobuffputrc.right < max_w )
                    {
                        videobuffputrc.left = ( max_w - videobuffputrc.right ) / 2.0f;
                        videobuffputrc.right += videobuffputrc.left;
                    }

                    if ( videobuffputrc.bottom < max_h )
                    {
                        videobuffputrc.top = ( max_h - videobuffputrc.bottom ) / 2.0f;
                        videobuffputrc.bottom += videobuffputrc.top;
                    }

                    if ( videodrawbuff_lock == false )
                    {
                        pDXTarget->DrawBitmap( videodrawbuff, videobuffputrc, imgtransp, D2D1_BITMAP_INTERPOLATION_MODE_LINEAR, NULL );

                        Sleep(1);
                    }
                }

#ifdef DEBUG
                if ( ( countout > ( configuration->conwaits * 1000 ) ) || ( forceStopDebug == true ) )
#else
                if ( countout > ( configuration->conwaits * 1000 ) )
#endif // DEBUG
                {
                    rtspp.Stop();

                    if ( gamemode == true )
                    {
                        gamemode = false;
                    }

                    ShowCursor( TRUE );
                    MessageBox( g_hwnd,
                                TEXT("A-streaming data stream lost"),
                                TEXT("Error"),
                                MB_ICONERROR );
                    ShowCursor( FALSE );

                    if ( rtspNeedReload == true )
                    {
                        rtspNeedReload = false;
                    }

                    if ( rtspNeedReloadDisp == true )
                    {
                        rtspNeedReloadDisp = false;
                    }

                    if ( videodrawbuff_reload != NULL )
                    {
                        while( videodrawbuff_lock == true )
                        {
                            // hold on while buffer unlocked ...
                            Sleep( 1 );
                        }

                        videodrawbuff_lock = true;

                        SafeRelease( &videodrawbuff_reload );

                        videodrawbuff_lock = false;
                    }

                    scene_sub_exit = true;
                }

                break;
        }

        // Draw some UIs ...

        if ( gamemode == true )
        {
            D2D1_RECT_F imgputrc = {0};

            calcimgputrc( &imgputrc, dstx_titlethumb, dsty_titlethumb, putwh_titlethumb, putwh_titlethumb );
            pDXTarget->DrawBitmap( pItitlemenu[0], imgputrc, thumb_oppa, D2D1_BITMAP_INTERPOLATION_MODE_LINEAR, NULL );

            int put_y = cornergap;

            drawText( dstx_titlethumb + putwh_titlethumb + cornergap , put_y,
                      TEXT("Press special key to return to the main menu ..."),
                      180, 180, 180, thumb_oppa );

            if ( rtspNeedReloadDisp == true )
            {
                put_y += 30;
                drawText( dstx_titlethumb + putwh_titlethumb + cornergap , put_y,
                          TEXT("Stream is reloading ...."),
                          180, 180, 180, thumb_oppa );
            }

            // Check exit key pressed ....
            if ( scene_sub_flag != 1 )
            {
                if ( configuration->ctrl_exittype == 0 ) /// Keyboard ....
                {
                    if ( configuration->ctrl_exitcode == lastkeyup )
                    {
                        lastkeyup = -1;
                        scene_sub_exit  = true;
                    }
                }
                else
                if ( configuration->ctrl_exittype == 1 ) /// Mouse ....
                {
                    if ( configuration->ctrl_exitcode == lastmouseup )
                    {
                        lastmouseup = -1;
                        scene_sub_exit  = true;
                    }
#ifdef DEBUG
                    if ( msmb > 0  )
                    {
                        lastmouseup = -1;
                        scene_sub_exit  = true;
                    }
#endif
                }
            }
        }
        else
        if ( viewmode == true )
        {
            D2D1_RECT_F imgputrc = {0};

            calcimgputrc( &imgputrc, dstx_titlethumb, dsty_titlethumb, putwh_titlethumb, putwh_titlethumb );
            pDXTarget->DrawBitmap( pItitlemenu[1], imgputrc, thumb_oppa, D2D1_BITMAP_INTERPOLATION_MODE_LINEAR, NULL );


            if ( rtspNeedReloadDisp == true )
            {
                int put_y = cornergap;

                drawText( dstx_titlethumb + putwh_titlethumb + cornergap , put_y,
                          TEXT("Stream is reloading ...."),
                          180, 180, 180, thumb_oppa );
            }


            float quit_oppa = thumb_oppa * 0.8f;
            if ( quit_oppa < 0.4f )
            {
                quit_oppa = 0.4f;
            }

            calcimgputrc( &imgputrc, dstx_close, dsty_close, putwh_close, putwh_close );

            if ( checkmouseinrc( &imgputrc ) == true )
            {
                quit_oppa = 1.0f;
            }

            pDXTarget->DrawBitmap( pIclose, imgputrc, quit_oppa, D2D1_BITMAP_INTERPOLATION_MODE_LINEAR, NULL );

            if ( ( mslb == 1 ) && ( checkmouseinrc( &imgputrc ) == true ) && ( scene_sub_flag != 1 ) )
            {
                scene_sub_exit = true;
                mslb = 0;
            }
        }

    }
    else
    if ( menustate == 2 )
    {
        scene_sub_exit = true;
    }

    if ( scene_sub_exit == true )
    {
        rtspp.Stop();

        if ( videodrawbuff != NULL )
        {
            SafeRelease( &videodrawbuff );
        }

        menuSelect = -1;
        scene_menu_reset();
        scene_sub_exit = false;
        scene_sub_count = 0;
        countout = 0;
        drawState = 0;
        gamemode = false;
        viewmode = false;
        videoBuffered = false;
        imgtransp = 0.0f;
        scene_sub_flag = 0;
        rtspConnected = -1;
        ctrlConnected = false;
        thumb_oppa = 1.0f;
        setTextFormat( pTextFmtS );

#ifdef DEBUG
        forceStopDebug = false;
#endif // DEBUG

        flagStopSThread = true;
        flagStopXThread = true;

        Sleep( 500 ); /// Need to wait each thread be quit.

        if ( hSThread != INVALID_HANDLE_VALUE )
        {
            TerminateThread( hSThread, 0 );
            WaitForSingleObject( hSThread, 5000 );
            CloseHandle( hSThread );
            hSThread = INVALID_HANDLE_VALUE;
        }

        if ( hXThread != INVALID_HANDLE_VALUE )
        {
            TerminateThread( hXThread, 0 );
            WaitForSingleObject( hXThread, 5000 );
            CloseHandle( hXThread );
            hXThread = INVALID_HANDLE_VALUE;
        }

        if ( rtspNeedReload == false )
        {
            pDSSndBuffBack->Play(0,0, DSBPLAY_LOOPING);
        }
    }
}

void scene_config()
{
    if ( g_hwndConf != NULL )
        return;

    mslb = 0;
    msmb = 0;
    msrb = 0;

    int reti = DialogBox( g_hinst,
                          MAKEINTRESOURCE( IDD_DLG_CONF ),
                          g_hwnd,
                          (DLGPROC)DlgConfProc );

    scene_menu_reset();

    menuSelect = -1;
    drawState = 0;
    gamemode = false;
    viewmode = false;

    mslb = 0;
    msrb = 0;
    msmb = 0;
}

void EnDisable_CfgDlgComps( BOOL enabled )
{
    if ( g_hwndConf != INVALID_HANDLE_VALUE )
    {
        EnableWindow( GetDlgItem( g_hwndConf, IDC_COMBO_AS_IP ), enabled );
        EnableWindow( GetDlgItem( g_hwndConf, IDC_CHECK_USE_CUR_IP_SFX ), enabled );
        EnableWindow( GetDlgItem( g_hwndConf, IDC_BUTTON_AS_SEARCH ), enabled );
        EnableWindow( GetDlgItem( g_hwndConf, IDC_CHECK_USE_CLEAR_TTF ), enabled );
        EnableWindow( GetDlgItem( g_hwndConf, IDC_COMBO_AS_RES_DST ), enabled );
        EnableWindow( GetDlgItem( g_hwndConf, IDC_CHECK_USE_FULLSCREEN ), enabled );
        EnableWindow( GetDlgItem( g_hwndConf, IDC_BUTTON_AS_CHANGE_EXIT_KEY ), enabled );
        EnableWindow( GetDlgItem( g_hwndConf, IDC_RADIO_JOY_NORMAL ), enabled );
        EnableWindow( GetDlgItem( g_hwndConf, IDC_RADIO_JOY_XINPUT ), enabled );
        EnableWindow( GetDlgItem( g_hwndConf, IDC_CHECK_LOAD_KEYEXCHANGE ), enabled );
        EnableWindow( GetDlgItem( g_hwndConf, IDC_BUTTON_AS_SAVE ), enabled  );
        EnableWindow( GetDlgItem( g_hwndConf, IDC_BUTTON_AS_CANCEL ), enabled  );
    }
}

////////////////////////////////////////////////////////////////////////////////

// Hooking dialog for Config.
LRESULT CALLBACK MsgHookDialogProc(int nCode, WPARAM wParam, LPARAM lParam)
{
    MSG* lpmsg = (MSG*)lParam;

    //if ( ( lpmsg->hwnd == g_hwndConf ) && ( nCode == MSGF_DIALOGBOX ) )
    if ( nCode == MSGF_DIALOGBOX )
    {
        if ( keycapmode == false )
        {
            switch( lpmsg->message )
            {
                case WM_KEYDOWN:
                case WM_KEYUP:
                    SendMessage( g_hwndConf,
                                 WM_KEYDOWN,
                                 lpmsg->wParam,
                                 lpmsg->lParam );
                    break;
            }
        }
    }

    return ( CallNextHookEx( hHook, nCode, wParam, lParam ) );
}

LRESULT CALLBACK DlgConfProc(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    static int      scl_y_pos = 0;
    static int      scl_y_height = 0;
    static int      scl_y_max = 0;
    static int      dlg_height = 0;
    static bool     dlg_need_scroll = false;
    static bool     stop_scroll = false;
    static bool     needRestart = false;
    static int      tmp_joy_t   = -1;
    static int      tmp_exit_t  = -1;
    static int      tmp_exit_c  = -1;

    bool xbtn_flag = false;

    switch( uMsg )
    {
        case WM_INITDIALOG:
            {
                ShowCursor( TRUE );

                scl_y_pos = 0;
                scl_y_height = 0;
                scl_y_max = 0;
                dlg_height = 0;
                dlg_need_scroll = false;

                g_hwndConf = hwndDlg;

                SetWindowText( hwndDlg, TEXT("A-streaming Config.") );

                // Set Hook control for getting keys.
                hHook = SetWindowsHookEx( WH_MSGFILTER, MsgHookDialogProc, 0, GetCurrentThreadId());

                // Set window position to host.
                if ( configuration->fullscreen == 0 )
                {
                    SetWindowPos( hwndDlg,
                                  g_hwnd,
                                  configuration->win_x,
                                  configuration->win_y,
                                  configuration->win_w,
                                  configuration->win_h,
                                  0 );
                    dlg_height = configuration->win_h;
                }
                else
                {
                    SetWindowPos( hwndDlg,
                                  g_hwnd,
                                  0,
                                  0,
                                  desktopWidth,
                                  desktopHeight,
                                  0 );
                    dlg_height = desktopHeight;
                }

                // Check scroll needed.
                RECT winrc = {0};
                GetWindowRect( hwndDlg, &winrc );

                RECT tmprc = {0};
                GetWindowRect( GetDlgItem( hwndDlg, IDC_BUTTON_AS_CANCEL ), &tmprc );

                scl_y_height = tmprc.top + ( tmprc.bottom - tmprc.top ) - winrc.top ;

                int win_height = winrc.bottom - winrc.top;

                if ( scl_y_height > win_height )
                {
                    dlg_need_scroll = true;
                    scl_y_max = ( scl_y_height - win_height ) + 50; /// 50 = margin.

                    SCROLLINFO si = {0};
                    si.cbSize = sizeof( SCROLLBARINFO );
                    si.fMask  = SIF_ALL | SIF_DISABLENOSCROLL;
                    si.nMin   = 0;
                    si.nMax   = scl_y_max;
                    si.nPos   = 0;

                    SetScrollInfo( hwndDlg, SB_VERT, &si, TRUE );
                }
                else
                {
                    ShowScrollBar( hwndDlg, SB_VERT, FALSE );
                }

                // -------------------------------------------------------------
                // Apply config data to dailog ...

                // A-streaming version ...
                TCHAR verstr[64] = {0};
                TCHAR verresstr[] = TEXT(VERSION_S);
                _stprintf( verstr,
                           TEXT("A-streaming win32 client version: %s"),
                           verresstr );

                SetWindowText( GetDlgItem( hwndDlg, IDC_STATIC_AS_VERSION ),
                               verstr );

                // A-streaming IP address.
                TCHAR ipstr[64] = {0};
#ifdef UNICODE
                mbstowcs( ipstr, configuration->server_ip, 64 );
#else
                _tcscpy( ipstr, configuration->server_ip );
#endif
                HWND hEditIP = GetDlgItem( hwndDlg, IDC_COMBO_AS_IP );

                SendMessage( hEditIP,
                             CB_ADDSTRING,
                             0,
                             (LPARAM)ipstr );
                ComboBox_SetCurSel( hEditIP, 0 );

                // Source resoulutions
                TCHAR* combolist_input_res[] =
                {
                    TEXT("FHD : 1920x1080p"),
                    TEXT("HD  : 1280x720p")
                };

                HWND hComboAsResSrc = GetDlgItem( hwndDlg, IDC_COMBO_AS_RES_SRC );

                for( int cnt=0; cnt<2; cnt++ )
                {
                    SendMessage( hComboAsResSrc,
                                 CB_ADDSTRING,
                                 0,
                                 (LPARAM)combolist_input_res[cnt] );
                }

                ComboBox_SetCurSel( hComboAsResSrc, 0 );
                EnableWindow( hComboAsResSrc, FALSE );

                // Source BPS
                HWND hComboAsBpsSrc = GetDlgItem( hwndDlg, IDC_COMBO_AS_BPS_SRC );

                for( int cnt=0; cnt<as_source_bps_size; cnt++ )
                {
                    SendMessage( hComboAsBpsSrc,
                                 CB_ADDSTRING,
                                 0,
                                 (LPARAM)as_source_bps_table[cnt].text );
                }

                ComboBox_SetCurSel( hComboAsBpsSrc, 0 );
                EnableWindow( hComboAsBpsSrc, FALSE );

                // Checkbox : clear type.
                if ( configuration->cleartype > 0 )
                {
                    SendMessage( GetDlgItem( hwndDlg, IDC_CHECK_USE_CLEAR_TTF ),
                                 BM_SETCHECK,
                                 BST_CHECKED,
                                 0 );
                }

                // Client resolutions
                TCHAR* combolist_output_res[] =
                {
                    TEXT("FHD : 1920x1080p"),
                    TEXT("HD  : 1280x720p"),
                    TEXT("qHD : 960x540"),
                    TEXT("nHD : 640x360")
                };

                HWND hComboAsResDst = GetDlgItem( hwndDlg, IDC_COMBO_AS_RES_DST );

                for( int cnt=0; cnt<4; cnt++ )
                {
                    SendMessage( hComboAsResDst,
                                 CB_ADDSTRING,
                                 0,
                                 (LPARAM)combolist_output_res[cnt] );
                }

                ComboBox_SetCurSel( hComboAsResDst, configuration->res_t );

                // Checkbox : full screen.
                if ( configuration->fullscreen > 0 )
                {
                    SendMessage( GetDlgItem( hwndDlg, IDC_CHECK_USE_FULLSCREEN ),
                                 BM_SETCHECK,
                                 BST_CHECKED,
                                 0 );
                }

                // Checkbox : Load keyexchange
                if ( configuration->loadkeybinds > 0 )
                {
                    SendMessage( GetDlgItem( hwndDlg, IDC_CHECK_LOAD_KEYEXCHANGE ),
                                 BM_SETCHECK,
                                 BST_CHECKED,
                                 0 );
                }

                // Exit input method.
                TCHAR tmpout[64] = {0};

                if ( configuration->ctrl_exittype == 0 )
                {
                    _stprintf( tmpout,
                               TEXT("Keyboard, %X"),
                               configuration->ctrl_exitcode);
                }
                else
                {
                    _stprintf( tmpout,
                               TEXT("Mouse, %X"),
                               configuration->ctrl_exitcode);
                }

                SetWindowText( GetDlgItem( hwndDlg, IDC_BUTTON_EXIT_KEY ), tmpout );

                // Checkboxs : Joystick...
                if ( configuration->joystick_type == 0 )
                {
                    SendMessage( GetDlgItem( hwndDlg, IDC_RADIO_JOY_NORMAL ),
                                 BM_SETCHECK,
                                 BST_CHECKED,
                                 0 );
                }
                else
                {
                    SendMessage( GetDlgItem( hwndDlg, IDC_RADIO_JOY_XINPUT ),
                                 BM_SETCHECK,
                                 BST_CHECKED,
                                 0 );
                }

                tmp_joy_t  = configuration->joystick_type;
                tmp_exit_t = configuration->ctrl_exittype;
                tmp_exit_c = configuration->ctrl_exitcode;

            }
            break;

        case WM_DESTROY:
            UnhookWindowsHookEx( hHook );
            ShowCursor( FALSE );

            g_hwndConf = NULL;
            scl_y_pos = 0;
            scl_y_height = 0;
            scl_y_max = 0;
            dlg_height = 0;
            dlg_need_scroll = false;
            stop_scroll = false;
            needRestart = false;
            tmp_exit_t  = -1;
            tmp_exit_c  = -1;
            break;

        case WM_MOUSEWHEEL:
        case WM_KEYDOWN:
            if ( keycapmode == true )
            {
                break;
            }
            else
            if ( ( dlg_need_scroll == true ) && ( stop_scroll == false ) )
            {
                bool pageMove = false;
                int  posMount = 0;

                if ( uMsg == WM_KEYDOWN )
                {
                    switch( wParam )
                    {
                        case VK_UP:
                            pageMove  = true;
                            posMount -= 20;
                            break;

                        case VK_DOWN:
                            pageMove  = true;
                            posMount += 20;
                            break;

                        case VK_PRIOR:  /// PAGE UP
                            pageMove  = true;
                            posMount += 100;
                            break;

                        case VK_NEXT:   /// PAGE DOWN
                            pageMove  = true;
                            posMount -= 100;

                        case VK_ESCAPE:
                            EndDialog(hwndDlg, 0);
                            break;

                    }
                }
                else
                {
                    int wheelv = GET_WHEEL_DELTA_WPARAM(wParam);

                    if ( wheelv < 0 )
                    {
                        pageMove  = true;
                        posMount -= 20;
                    }
                    else
                    if ( wheelv > 0 )
                    {
                        pageMove  = true;
                        posMount += 20;
                    }
                }

                if ( pageMove == true )
                {
                    int chkval = scl_y_pos - posMount;

                    if ( chkval < 0 )
                    {
                        posMount = scl_y_pos;
                    }
                    else
                    if ( chkval > scl_y_max )
                    {
                        posMount = - ( scl_y_max - scl_y_pos );
                    }

                    ScrollWindow( hwndDlg, 0, posMount , NULL, NULL );
                    scl_y_pos += -posMount;
                    SetScrollPos( hwndDlg, SB_VERT, scl_y_pos, TRUE );

                }
            }
            break;

        case WM_INPUT:
            if ( keycapmode == true )
            {
                BYTE   ridata[40] = {0};
                UINT   risz       = 40;

                GetRawInputData( (HRAWINPUT)lParam,
                                 RID_INPUT,
                                 ridata,
                                 &risz,
                                 sizeof(RAWINPUTHEADER) );

                RAWINPUT* ri = (RAWINPUT*)ridata;

                if( ri->header.dwType == RIM_TYPEMOUSE )
                {
                    int mousev = -1;

                    switch( ri->data.mouse.ulButtons )
                    {
                        case RI_MOUSE_BUTTON_2_UP:
                            mousev = 1;
                            break;

                        case RI_MOUSE_BUTTON_3_UP:
                            mousev = 2;
                            break;

                        case RI_MOUSE_BUTTON_4_UP:
                            mousev = 3;
                            break;

                        case RI_MOUSE_BUTTON_5_UP:
                            mousev = 4;
                            break;
                    }

                    if ( mousev > 0 )
                    {
                        PostMessage( hwndDlg,
                                     WM_USER + 200,
                                     mousev,
                                     0 );
                    }
                }
                else
                if( ri->header.dwType == RIM_TYPEKEYBOARD )
                {
                    PostMessage( hwndDlg,
                                 WM_USER + 201,
                                 ri->data.keyboard.VKey,
                                 0 );
                }
            }
            break;

        case WM_USER + 200:
            xbtn_flag = true;
        case WM_USER + 201:
            {
                if ( keycapmode == true )
                {
                    keycapmode = false;

                    RAWINPUTDEVICE rawInputDev[2];
                    ZeroMemory(rawInputDev, sizeof(RAWINPUTDEVICE)*2);

                    rawInputDev[0].usUsagePage  = 0x01;
                    rawInputDev[0].usUsage      = 0x06;
                    rawInputDev[0].dwFlags      = RIDEV_REMOVE;
                    rawInputDev[0].hwndTarget   = hwndDlg;

                    rawInputDev[1].usUsagePage  = 0x01;
                    rawInputDev[1].usUsage      = 0x02;
                    rawInputDev[1].dwFlags      = RIDEV_REMOVE;
                    rawInputDev[1].hwndTarget   = hwndDlg;

                    RegisterRawInputDevices(rawInputDev, 2, sizeof(RAWINPUTDEVICE) );

                    EnDisable_CfgDlgComps( TRUE );

                    // Check wParam and lParam.
                    TCHAR tmpout[64] = {0};

                    if ( xbtn_flag == true )
                    {
                        tmp_exit_t  = 1;
                        tmp_exit_c  = wParam;

                        _stprintf( tmpout,
                                   TEXT("Mouse, %X"),
                                   wParam);
                        SetWindowText( GetDlgItem( hwndDlg, IDC_BUTTON_EXIT_KEY ), tmpout );
                    }
                    else
                    if ( wParam == VK_ESCAPE )
                    {
                        // Cancelled.
                        SetWindowText( GetDlgItem( hwndDlg, IDC_BUTTON_EXIT_KEY ), tmpout );
                    }
                    else
                    {
                        tmp_exit_t  = 0;
                        tmp_exit_c  = wParam;

                        _stprintf( tmpout,
                                   TEXT("Keyboard, %X"),
                                   wParam);
                        SetWindowText( GetDlgItem( hwndDlg, IDC_BUTTON_EXIT_KEY ), tmpout );
                    }
                }
            }
            break;

        case WM_COMMAND:
            {
                ULONG component = LOWORD(wParam);

                switch( component )
                {
                    case IDC_BUTTON_AS_SEARCH:
                        {
                            stop_scroll = true;

                            HWND hCBoxSCIPX = GetDlgItem( hwndDlg, IDC_CHECK_USE_CUR_IP_SFX );
                            int  reti = SendMessage( hCBoxSCIPX, BM_GETCHECK, 0, 0 );
                            if ( reti == BST_CHECKED )
                            {
                                TCHAR ipstr[64] = {0};
                                GetWindowText( GetDlgItem( hwndDlg, IDC_COMBO_AS_IP ), ipstr, 64 );
#ifdef UNICODE
                                wcstombs( asipsfx, ipstr, 64 );
#else
                                _tcscpy( asipsfx, ipstr );
#endif
                                cfgSchIpSfx = true;
                            }
                            else
                            {
                                cfgSchIpSfx = false;
                                memset( asipsfx, 0 , 64 );
                            }

                            reti = DialogBox( g_hinst,
                                              MAKEINTRESOURCE( IDD_DLG_SEARCH_AS ),
                                              hwndDlg,
                                              (DLGPROC)DlgSearchProc );
                            stop_scroll = false;

                            if ( asipfound == false )
                            {
                                MessageBox( hwndDlg,
                                            TEXT("Failed to find A-streaming on network !"),
                                            TEXT("Failed"),
                                            MB_ICONWARNING );
                            }
                            else
                            {
                                // A-streaming IP address.
                                TCHAR ipstr[64] = {0};
#ifdef UNICODE
                                mbstowcs( ipstr, asipnew, 64 );
#else
                                _tcscpy( ipstr, asipnew );
#endif
                                HWND hEditIP = GetDlgItem( hwndDlg, IDC_COMBO_AS_IP );

                                SendMessage( hEditIP,
                                             CB_DELETESTRING,
                                             0,
                                             0 );

                                SendMessage( hEditIP,
                                             CB_INSERTSTRING,
                                             0,
                                             (LPARAM)ipstr );

                                ComboBox_SetCurSel( hEditIP, 0 );

                                if ( strcmp( configuration->server_ip, asipnew ) != 0 )
                                {
                                    strcpy( configuration->server_ip, asipnew );
                                    needRestart = true;
                                }

                            }
                        }
                        break;

                    case IDC_RADIO_JOY_NORMAL:
                        {
                            HWND hBJN = GetDlgItem( hwndDlg, IDC_RADIO_JOY_NORMAL );

                            if ( Button_GetCheck( hBJN )  == BST_CHECKED )
                            {
                                SendMessage( hBJN,
                                             BM_SETCHECK,
                                             BST_CHECKED,
                                             0 );
                                SendMessage( GetDlgItem( hwndDlg, IDC_RADIO_JOY_XINPUT ),
                                             BM_SETCHECK,
                                             BST_UNCHECKED,
                                             0 );
                            }

                            tmp_joy_t = 0;
                        }
                        break;

                    case IDC_RADIO_JOY_XINPUT:
                        {
                            HWND hBJX = GetDlgItem( hwndDlg, IDC_RADIO_JOY_XINPUT );

                            if ( Button_GetCheck( hBJX )  == BST_CHECKED )
                            {
                                SendMessage( hBJX,
                                             BM_SETCHECK,
                                             BST_CHECKED,
                                             0 );
                                SendMessage( GetDlgItem( hwndDlg, IDC_RADIO_JOY_NORMAL ),
                                             BM_SETCHECK,
                                             BST_UNCHECKED,
                                             0 );
                            }

                            tmp_joy_t = 1;
                        }
                        break;

                    case IDC_BUTTON_AS_CHANGE_EXIT_KEY:
                        {
                            if ( keycapmode == false )
                            {
                                // Set Capture mode on.
                                keycapmode = true;

                                RAWINPUTDEVICE rawInputDev[2];
                                ZeroMemory(rawInputDev, sizeof(RAWINPUTDEVICE)*2);

                                // 0 == keyboard.
                                rawInputDev[0].usUsagePage  = 0x01;
                                rawInputDev[0].usUsage      = 0x06;
                                rawInputDev[0].dwFlags      = RIDEV_INPUTSINK;
                                rawInputDev[0].hwndTarget   = hwndDlg;

                                // 1 == mouse
                                rawInputDev[1].usUsagePage  = 0x01;
                                rawInputDev[1].usUsage      = 0x02;
                                rawInputDev[1].dwFlags      = RIDEV_INPUTSINK;
                                rawInputDev[1].hwndTarget   = hwndDlg;

                                BOOL retb = RegisterRawInputDevices( rawInputDev,
                                                                     2,
                                                                     sizeof(RAWINPUTDEVICE) );
                                if( retb == TRUE )
                                {
                                    // Disable all for capture key ...
                                    EnDisable_CfgDlgComps( FALSE );

                                    SetFocus( GetDlgItem( hwndDlg, IDC_BUTTON_EXIT_KEY ) );
                                }
                            }
                        }
                        break;

                    case IDC_BUTTON_AS_SAVE:
                        {
                            // Let stop dialog input.
                            EnableWindow( hwndDlg, FALSE );

                            // -------------------------------------------------
                            // Check modified issues...
                            int reti = 0;

                            // Server IP (user modified ? )
                            TCHAR tmpstr[64] = {0};
                            ComboBox_GetText( GetDlgItem( hwndDlg, IDC_COMBO_AS_IP ),
                                              tmpstr,
                                              64 );

                            if ( _tcslen( tmpstr ) > 0 )
                            {
                                char convstr[64] = {0};
#ifdef UNICODE
                                wcstombs( convstr, tmpstr, 64 );
#else
                                strcpy( convstr, tmpstr );
#endif

                                if ( strcmp( convstr, configuration->server_ip ) != 0 )
                                {
                                    strcpy( configuration->server_ip, convstr );
                                }
                            }

                            // Clear TTF type ..
                            reti = SendMessage( GetDlgItem( hwndDlg, IDC_CHECK_USE_CLEAR_TTF ),
                                                BM_GETCHECK,
                                                BST_CHECKED,
                                                0 );
                            if ( reti != configuration->cleartype )
                            {
                                configuration->cleartype = reti;
                                needRestart = true;
                            }

                            // Client Resoultion...
                            reti = ComboBox_GetCurSel( GetDlgItem( hwndDlg, IDC_COMBO_AS_RES_DST) );
                            if ( reti >= 0 )
                            {
                                if ( reti != configuration->res_t )
                                {
                                    configuration->res_t = reti;

                                    if ( configuration->fullscreen == 0 )
                                    {
                                        needRestart = true;
                                    }
                                }
                            }

                            // Full screen changed ?
                            reti = SendMessage( GetDlgItem( hwndDlg, IDC_CHECK_USE_FULLSCREEN ),
                                                BM_GETCHECK,
                                                BST_CHECKED,
                                                0 );
                            if ( reti != configuration->fullscreen )
                            {
                                configuration->fullscreen = reti;
                                needRestart = true;
                            }

                            // Exit code...
                            if ( ( configuration->ctrl_exittype != tmp_exit_t ) ||
                                 ( configuration->ctrl_exitcode != tmp_exit_c ) )
                            {
                                configuration->ctrl_exittype = tmp_exit_t;
                                configuration->ctrl_exitcode = tmp_exit_c;
                            }

                            // Joystick types ...
                            if ( configuration->joystick_type != tmp_joy_t )
                            {
                                configuration->joystick_type = tmp_joy_t;
                                needRestart = true;
                            }

                            // Key exchnage loads ?
                            reti = SendMessage( GetDlgItem( hwndDlg, IDC_CHECK_LOAD_KEYEXCHANGE ),
                                                BM_GETCHECK,
                                                BST_CHECKED,
                                                0 );
                            if ( reti != configuration->loadkeybinds )
                            {
                                configuration->loadkeybinds = reti;
                            }

                            // -------------------------------------------------
                            // Last
                            saveConfig();

                            if ( needRestart == true )
                            {
                                HINSTANCE hResult = ShellExecute( g_hwnd,
                                                                  TEXT("open"),
                                                                  g_myFName,
                                                                  TEXT("--restart"),
                                                                  NULL,
                                                                  SW_HIDE );
                                //if ( (DWORD)hResult < 32 )
                                if ( hResult == NULL )
                                {
                                    MessageBox( hwndDlg,
                                                TEXT("Cannot restart program !"),
                                                TEXT("Error:"),
                                                MB_ICONERROR );
                                }
                            }

                            EndDialog( hwndDlg, 0 );
                        }
                        break;

                    case IDC_BUTTON_AS_CANCEL:
                        EndDialog(hwndDlg, 0);
                        break;
                }
            }
            break;
    }

    return 0;
}

LRESULT CALLBACK DlgSearchProc(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    static HANDLE hSearchIpThread = INVALID_HANDLE_VALUE;

    switch( uMsg )
    {
        case WM_INITDIALOG:
            {
                g_hwndSeach = hwndDlg;

                SetWindowText( hwndDlg, TEXT("Searching A-streaming ...") );

                RECT itsrc = {0};
                GetWindowRect( hwndDlg, &itsrc );
                int its_w = itsrc.right - itsrc.left;
                int its_h = itsrc.bottom - itsrc.top;

                if ( configuration->fullscreen == 0 )
                {
                    SetWindowPos( hwndDlg,
                                  g_hwnd,
                                  configuration->win_x + 20,
                                  configuration->win_y + 20,
                                  its_w,
                                  its_h,
                                  0 );
                }
                else
                {
                    SetWindowPos( hwndDlg,
                                  g_hwnd,
                                  20,
                                  20,
                                  its_w,
                                  its_h,
                                  0 );
                }

                HWND hProg = GetDlgItem( hwndDlg, IDC_PROGRESS_FINDING_AS );
                LONG_PTR style = GetWindowLongPtr( hProg, GWL_STYLE );
                SetWindowLongPtr( hProg, GWL_STYLE, style | PBS_MARQUEE );

                SendDlgItemMessage( hwndDlg,
                                    IDC_PROGRESS_FINDING_AS,
                                    PBM_SETMARQUEE,
                                    (WPARAM) TRUE,
                                    (LPARAM) 40 );

                // Create Thread !
                hSearchIpThread = CreateThread( NULL, 0, findingipThread, NULL, 0, NULL );
            }
            break;

        case WM_DESTROY:
            {
                if ( hSearchIpThread != INVALID_HANDLE_VALUE )
                {
                    TerminateThread( hSearchIpThread, 0 );
                    WaitForSingleObject( hSearchIpThread, 5000 );
                    CloseHandle( hSearchIpThread );
                    hSearchIpThread = INVALID_HANDLE_VALUE;
                }
                g_hwndSeach = NULL;
            }
            break;

        case WM_COMMAND:
            {
                ULONG component = LOWORD(wParam);

                switch( component )
                {
                    case IDC_BUTTON_CANCEL_FINDING_AS:
                        EndDialog( hwndDlg, 0 );
                        break;
                }
            }
            break;

        case WM_USER + 100: /// found
        case WM_USER + 101: /// failed to found
            {
                CloseHandle( hSearchIpThread );
                hSearchIpThread = INVALID_HANDLE_VALUE;
                EndDialog( hwndDlg, 0 );
            }
            break;
    }

    return 0;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

DWORD WINAPI rtspconnectThread( LPVOID p )
{
    sprintf( rtsp_url, "rtsp://%s/live1.sdp", configuration->server_ip );

    // test rtsp server open :
    if ( rtspp.TestConnection( configuration->server_ip ) == true )
    {
        if ( rtspNeedReload == true )
        {
            // Wait for a second for stable connection ...
            Sleep( 3 * 1000 );
        }

        if ( rtspp.OpenURL( rtsp_url ) == true )
        {
            rtspConnected = 1;
        }
        else
        {
            rtspConnected = -1;
        }
    }
    else
    {
        rtspConnected = -1;
    }

    CloseHandle( hCThread );
    hCThread = INVALID_HANDLE_VALUE;

    return 0;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

DWORD WINAPI HIDsendThread( LPVOID p )
{
    SOCKET              hSocket = 0;
    SOCKADDR_IN         addrSvr;
    bool                socket_connected = false;
    bool                askexc_enabled = false;

    if ( logger != NULL )
    {
        logger->write(" start thread of (HIDsendThread) ... \n ");
    }

    // ------------------------------------------------------------------------
    // Loading key exchanges if enabeld ...
    if ( configuration->loadkeybinds > 0 )
    {
        if ( proc_askexch_load() == true )
        {
            if ( logger != NULL )
            {
                int kbdbindssz = get_askexch_kbd_binds();
                int msebindssz = get_askexch_mse_binds();
                int joybindssz = get_askexch_joy_binds();

                logger->write("###########################################\n");
                logger->write( "keyexchange load done.\n");
                logger->write( "keyboard exchange binds size = %d\n", kbdbindssz );
                logger->write( "mouse exchange binds size = %d\n", msebindssz );
                logger->write( "joystick exchange binds size = %d\n", joybindssz );
                logger->write("###########################################\n");

                if ( ( kbdbindssz > 0 ) || ( msebindssz > 0 ) || ( joybindssz > 0 ) )
                {
                    askexc_enabled = true;
                }
            }
        }
        else
        {
            if ( logger != NULL )
            {
                logger->write("###########################################\n");
                logger->write( "keyexchange not loaded.\n");
                logger->write("###########################################\n");
            }
        }
    }

    if ( socket_ready == true )
    {
        hSocket = socket( PF_INET, SOCK_STREAM, IPPROTO_TCP );
    }
    else
    {
        hidConnected = false;

        if ( logger != NULL )
        {
            logger->write(" abnormal exit of (HIDsendThread): socket not ready.\n ");
        }

        return 0;
    }

    if ( hSocket > 0 )
    {
        memset( &addrSvr, 0, sizeof(SOCKADDR_IN) );

        if ( logger != NULL )
        {
            logger->write(" -- Connecting to HID server :%s\n ", configuration->server_ip );
        }

        addrSvr.sin_family = AF_INET;
        addrSvr.sin_addr.s_addr = inet_addr( configuration->server_ip );
        addrSvr.sin_port = htons( 8803 );

        int reti = connect( hSocket, (SOCKADDR*)&addrSvr, sizeof(addrSvr) );

        if ( reti != SOCKET_ERROR )
        {
            socket_connected = true;
            hidConnected = true;

            if ( logger != NULL )
            {
                logger->write(" -- HID Socket connected !\n ");
            }
        }
        else
        {
            if ( logger != NULL )
            {
                logger->write(" -- HID Socket failure !\n ");
            }
            closesocket( hSocket );
            hidConnected = false;
        }
    }
    else
    {
        hidConnected = false;
    }

    const char aspkt_kbd_null_ref[] = "k 0 0 0 0 0 0 0 0";
    const int  aspkt_kbd_null_sz    = strlen( aspkt_kbd_null_ref );

    const char aspkt_mse_null_ref[] = "m 0 0 0 0";
    const int  aspkt_mse_null_sz    = strlen( aspkt_mse_null_ref );

    const char aspkt_joy_null_ref[] = "j 0 0 0 0 0 0 0 0";
    const int  aspkt_joy_null_sz    = strlen( aspkt_joy_null_ref );

    char* aspkt_kbd_pre = NULL;
    char* aspkt_mse_pre = NULL;
    char* aspkt_joy_pre = NULL;
    bool  lostFocused = false;

    while( flagStopSThread == false )
    {
        //int exc_applied_kbd_sz = 0;
        //int exc_applied_mse_sz = 0;
        //int exc_applied_joy_sz = 0;

        //bool psy_kbd_skip = false;
        //bool psy_mse_skip = false;
        //bool psy_joy_skip = false;
        bool needwait     = true;

        if ( gamemode == false )
        {
            Sleep(10);
            continue;
        }

        // check this window got focused.
        HWND hwndFocused = GetForegroundWindow();
        if ( ( hwndFocused != NULL ) && ( hwndFocused != g_hwnd ) )
        {
            lostFocused = true;
            continue;
        }
        else
        {
            if ( lostFocused == true )
            {
                lostFocused = false;
                rememberMouse2ProgCeter();
            }
        }
        HRESULT hr = S_FALSE;

        char* aspkt_kbd = NULL;
        int   aspkt_len_kbd = 0;

        char* aspkt_mse = NULL;
        int   aspkt_len_mse = 0;

        char* aspkt_joy = NULL;
        int   aspkt_len_joy = 0;

        KEYSTATE   dxKeys;
        MOUSESTATE dxMouse;
        MOUSESTATE dxMouseExch = {0};
        JOYSTATE   dxJoystick;
        JOYSTATE   dxJoystickExch = {0};

        // ---------------------------------------------------------------------
        // Keyboard !
        if ( ( dxinputDevices & DXINPUT_INIT_RESULT_KEYBOARD ) > 0 )
        {
            hr = UpdateInputStateKeyboard();

            if ( hr == S_OK )
            {
                unsigned char mods = 0;
                unsigned char keys[6] = {0};
                //int keyque = 0;

                GetKeyboardState( &dxKeys );

                if ( askexc_enabled == true )
                {
                    proc_kbd_in_binds( &dxKeys, &dxMouseExch, &dxJoystickExch );
                }

                int keylen = dxkeystoanystreamkeys( &dxKeys, &mods, keys);

                aspkt_len_kbd = anystream_hid_keyboard_combine( mods,
                                                                keys,
                                                                keylen,
                                                                &aspkt_kbd );

            }
        }

        // ---------------------------------------------------------------------
        // Mouse !
        if ( ( dxinputDevices & DXINPUT_INIT_RESULT_MOUSE ) > 0 )
        {
            hr = UpdateInputStateMouse();

            if ( hr == S_OK )
            {
                GetMouseState( &dxMouse );

                // Let roll back mouse pointer to remembered
                // to not mouse going outside of window.
                SetCursorPos( remmousepos.x, remmousepos.y );

                if ( askexc_enabled == true )
                {
                    // check difference ...
                    // and override exchange binds.
                    proc_override_mouse( &dxMouse, &dxMouseExch );
                }
                // ------------- end of override.

                aspkt_len_mse = proc_dxmouse2aspacket( &dxMouse,
                                                       configuration->mouse_div,
                                                       &lastmouseup,
                                                       &aspkt_mse );
            }
        }

        // ---------------------------------------------------------------------
        // Joystick !
        if ( ( dxinputDevices & DXINPUT_INIT_RESULT_JOYSTICK ) > 0 )
        {
            hr = UpdateInputStateJoystick();

            if ( hr == S_OK )
            {
                GetJoystickState( &dxJoystick );

                if ( askexc_enabled == true )
                {
                    if ( proc_joy_in_binds( &dxJoystick, &dxMouseExch ) == true )
                    {
                        bool changed = proc_override_mouse( &dxMouse, &dxMouseExch );

                        if ( changed == true )
                        {
                            if ( aspkt_mse != NULL )
                            {
                                anystream_hid_destroy_packet( &aspkt_mse );
                            }

                            aspkt_len_mse = proc_dxmouse2aspacket( &dxMouse,
                                                                   configuration->mouse_div,
                                                                   &lastmouseup,
                                                                   &aspkt_mse );

                        }
                    }

                }

                int cur_joy_lx = ( float(dxJoystick.lX) / 1000.0f ) * 127.0f;
                int cur_joy_ly = ( float(dxJoystick.lY) / 1000.0f ) * 127.0f;
                int cur_joy_lz = ( float(dxJoystick.lZ) / 1000.0f ) * 127.0f;
                int cur_joy_rx = ( float(dxJoystick.lRx) / 1000.0f ) * 127.0f;
                int cur_joy_ry = ( float(dxJoystick.lRy) / 1000.0f ) * 127.0f;
                int cur_joy_rz = ( float(dxJoystick.lRz) / 1000.0f ) * 127.0f;
                unsigned cur_joy_btns = 0;

                for( int cnt=0; cnt<16; cnt++ )
                {
                    if ( ( dxJoystick.rgbButtons[cnt] & 0x80 ) > 0 )
                    {
                        cur_joy_btns |= 1 << cnt;
                    }
                }

                //char* aspckt = NULL;

                aspkt_len_joy = anystream_hid_joystick_combine( cur_joy_lx,
                                                                cur_joy_ly,
                                                                cur_joy_lz,
                                                                cur_joy_rx,
                                                                cur_joy_ry,
                                                                cur_joy_rz,
                                                                cur_joy_btns,
                                                                &aspkt_joy );

            }
        }

        // ---------------------------------------------------------------------
        // Check packet duplicated updated ...
        if ( aspkt_kbd_pre != NULL )
        {
            if( strcmp( aspkt_kbd_pre, aspkt_kbd ) == 0 )
            {
                if ( strncmp( aspkt_kbd_pre, aspkt_kbd_null_ref, aspkt_kbd_null_sz ) == 0 )
                {
                    anystream_hid_destroy_packet( &aspkt_kbd );
                    aspkt_len_kbd = 0;
                }
            }
        }

        if ( aspkt_mse_pre != NULL )
        {
            if( strcmp( aspkt_mse_pre, aspkt_mse ) == 0 )
            {
                if ( strncmp( aspkt_mse_pre, aspkt_mse_null_ref, aspkt_mse_null_sz ) == 0 )
                {
                    anystream_hid_destroy_packet( &aspkt_mse );
                    aspkt_len_mse = 0;
                }
            }
        }


        if ( aspkt_joy_pre != NULL )
        {
            if( strcmp( aspkt_joy_pre, aspkt_joy ) == 0 )
            {
                if ( strncmp( aspkt_joy_pre, aspkt_joy_null_ref, aspkt_joy_null_sz ) == 0 )
                {
                    anystream_hid_destroy_packet( &aspkt_joy );
                    aspkt_len_joy = 0;
                }
            }
        }
        // ---------------------------------------------------------------------

        // Send control packets if connected to server.
        if ( hidConnected == true )
        {

            // Send finally completed packets ...
            if ( aspkt_len_kbd > 0 )
            {
                send( hSocket, aspkt_kbd, aspkt_len_kbd+1, 0 );
                Sleep(5);
                if ( aspkt_kbd_pre != NULL )
                {
                    anystream_hid_destroy_packet( &aspkt_kbd_pre );
                }
                aspkt_kbd_pre = strdup( aspkt_kbd );
                anystream_hid_destroy_packet( &aspkt_kbd );
            }

            if ( aspkt_len_mse > 0 )
            {
                send( hSocket, aspkt_mse, aspkt_len_mse+1, 0 );
                Sleep(5);

                if ( aspkt_mse_pre != NULL )
                {
                    anystream_hid_destroy_packet( &aspkt_mse_pre );
                }
                aspkt_mse_pre = strdup( aspkt_mse );
                anystream_hid_destroy_packet( &aspkt_mse );
            }

            if ( aspkt_len_joy > 0 )
            {
                send( hSocket, aspkt_joy, aspkt_len_joy+1, 0 );
                Sleep(5);

                if ( aspkt_joy_pre != NULL )
                {
                    anystream_hid_destroy_packet( &aspkt_joy_pre );
                }
                aspkt_joy_pre = strdup( aspkt_joy );
                anystream_hid_destroy_packet( &aspkt_joy );
            }

            needwait = true;
        }
        else
        {
            if ( aspkt_len_kbd > 0 )
            {
                anystream_hid_destroy_packet( &aspkt_kbd );
            }

            if ( aspkt_len_mse > 0 )
            {
                anystream_hid_destroy_packet( &aspkt_mse );
            }

            if ( aspkt_len_joy > 0 )
            {
                anystream_hid_destroy_packet( &aspkt_joy );
            }
        }

        // ----

        if ( needwait == true )
        {
            Sleep(10);
        }
    }

    // Out of loop ...
    // Send releasing packets...

    if ( aspkt_kbd_pre != NULL )
    {
        anystream_hid_destroy_packet( &aspkt_kbd_pre );
    }

    if ( aspkt_mse_pre != NULL )
    {
        anystream_hid_destroy_packet( &aspkt_mse_pre );
    }

    if ( aspkt_joy_pre != NULL )
    {
        anystream_hid_destroy_packet( &aspkt_joy_pre );
    }


    // AnySteaming HID need send NULL arrays to release all devices to host OS.
    char* aspkt_release = NULL;
    int   pktlen_r = 0;

    if ( ( dxinputDevices & DXINPUT_INIT_RESULT_KEYBOARD ) > 0 )
    {
        unsigned char null_keys[6] = {0,0,0,0,0,0};

        pktlen_r = anystream_hid_keyboard_combine( 0, null_keys, 6, &aspkt_release );

        if ( pktlen_r > 0 )
        {
            if ( hidConnected == true )
            {
                send( hSocket, aspkt_release, pktlen_r+1, 0 );
                Sleep(5);
            }
            anystream_hid_destroy_packet( &aspkt_release );
        }
    }

    if ( ( dxinputDevices & DXINPUT_INIT_RESULT_JOYSTICK ) > 0 )
    {
        pktlen_r = anystream_hid_mouse_combine( 0,0,0,0,0,&aspkt_release );
        if ( pktlen_r > 0 )
        {
            if ( hidConnected == true )
            {
                send( hSocket, aspkt_release, pktlen_r+1, 0 );
                Sleep(5);
            }
            anystream_hid_destroy_packet( &aspkt_release );
        }
    }

    if ( ( dxinputDevices & DXINPUT_INIT_RESULT_JOYSTICK ) > 0 )
    {
        pktlen_r = anystream_hid_joystick_combine( 0,0,0,0,0,0,0,&aspkt_release );
        if ( pktlen_r > 0 )
        {
            if ( hidConnected == true )
            {
                send( hSocket, aspkt_release, pktlen_r+1, 0 );
                Sleep(5);
            }
            anystream_hid_destroy_packet( &aspkt_release );
        }
    }

    if ( hidConnected == true )
    {
        char tmpstr[32] = ">>>quit";
        send( hSocket, tmpstr, strlen(tmpstr), 0 );
        Sleep(5);

        closesocket( hSocket );
        hidConnected = false;
    }

    if ( configuration->loadkeybinds > 0 )
    {
        proc_askexch_unload();
    }

    if ( logger != NULL )
    {
        logger->write(" ... exit thread of (HIDsendThread) \n ");
    }

    return 0;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

DWORD WINAPI controlThread( LPVOID p )
{
    SOCKET              hSocket;
    SOCKADDR_IN         addrSvr;
    bool                socket_connected = false;

    if ( logger != NULL )
    {
        logger->write(" ... entering thread of (controlThread) \n ");
    }

    rtspNeedReload = false;

    if ( socket_ready == true )
    {
        hSocket = socket( PF_INET, SOCK_STREAM, IPPROTO_TCP );
    }
    else
    {
        return 0;
    }

    if ( hSocket > 0 )
    {
        memset( &addrSvr, 0, sizeof(SOCKADDR_IN) );

        addrSvr.sin_family = AF_INET;
        addrSvr.sin_addr.s_addr = inet_addr( configuration->server_ip );
        addrSvr.sin_port = htons( 8801 );

        int reti = connect( hSocket, (SOCKADDR*)&addrSvr, sizeof(addrSvr) );

        if ( reti != SOCKET_ERROR )
        {
            socket_connected = true;
            u_long mode = 1;  /// enable non-blocking socket
            ioctlsocket( hSocket, FIONBIO, &mode );

            // Let knwo current AS connected video stream qual.
            char testcmd[] = "AT+GETQUAL=?";
            send( hSocket, testcmd, strlen( testcmd ), 0 );
        }
        else
        {
            closesocket( hSocket );
            return 0;
        }

    }
    else
    {
        return 0;
    }

    #define RECVCMDS_LEN        512
    bool ignore_close_cmd = false;

    char recvCmds[RECVCMDS_LEN];

    while( flagStopXThread == false )
    {
        memset( recvCmds, 0, RECVCMDS_LEN );

        // Check data in read...
        int reti = recv( hSocket, recvCmds, RECVCMDS_LEN, 0 );
        if ( reti > 0 )
        {
            char* pCmd = NULL;

            // check Quality check ...
            pCmd = strstr( recvCmds, "AT+RSPQUAL=" );
            if ( pCmd != NULL )
            {
                char* pVals = pCmd + ( strlen( "AT+RSPQUAL=" ) );
                if ( pVals != NULL )
                {
                    char* dupped = strdup( pVals );
                    int   ididx  = 0;

                    char* stok = strtok( dupped, "," );

                    if ( logger != NULL )
                    {
                        logger->write( "===================================================\n" );
                        logger->write( "= Video quality summary/changed                   =\n" );
                        logger->write( "===================================================\n" );
                    }

                    while( stok != NULL )
                    {
                        switch( ididx )
                        {
                            case 0: /// VQCB, CBVR ...
                                if ( logger != NULL )
                                {
                                    logger->write( "Video Quality type = %s\n", stok );
                                }
                                break;

                            case 1: /// BPS.
                                if ( logger != NULL )
                                {
                                    logger->write( "Video BPS = %s BPS\n", stok );
                                }
                                break;

                            case 2: /// Qual. Nm.
                                if ( logger != NULL )
                                {
                                    logger->write( "Video Q.N = %s\n", stok );
                                }
                                break;

                            case 3: /// Null.
                                if ( logger != NULL )
                                {
                                    logger->write( "Video endindex = %s\n", stok );
                                }
                                break;

                            case 4: /// interlaced flag.
                                if ( logger != NULL )
                                {
                                    logger->write( "Video interlace flag = %s\n", stok );
                                }

                                if ( stok[0] == '0' )
                                {
                                    rtspp.SetInterlaced( true );
                                }
                                else
                                {
                                    rtspp.SetInterlaced( false );
                                }

                                break;

                        } /// end of switch

                        ididx++;
                        stok = strtok( NULL, "," );
                    }

                    free( stok );

                    ctrlConnected = true;
                }
            }

            // connection force close.
            pCmd = strstr( recvCmds, "CLOSE_CONNECTION" );
            if ( pCmd != NULL )
            {
                if ( logger != NULL )
                {
                    logger->write( "server sent = %s\n", recvCmds );
                }
                // connection closed.
                rtspForceClosed = true;
                ignore_close_cmd = true;
            }

            // need reload media.
            pCmd = strstr( recvCmds, "AT+RELOAD" );
            if ( pCmd != NULL )
            {
                if ( logger != NULL )
                {
                    logger->write( "server sent = %s\n", recvCmds );
                    logger->write( "Media stream reload !\n" );
                }
                // It must be reload video.
                if ( rtspConnected > 0 )
                {
                    if ( ( rtspNeedReload == false ) &&
                         ( videoBuffered == true ) )
                    {
                        if ( videodrawbuff != NULL )
                        {
                            if ( videodrawbuff_reload != NULL )
                            {
                                SafeRelease( &videodrawbuff_reload );
                            }

                            int width  = rtspp.GetVideoWidth();
                            int height = rtspp.GetVideoHeight();

                            // Need to do not draw videodrawbuff while copied.
                            videodrawbuff_lock = true;

                            CloneBitmap( videodrawbuff,
                                         width,
                                         height,
                                         videodrawbuff_reload );

                            videodrawbuff_lock = false;

                            rtspNeedReload = true;
                        }
                    }
                }
            }

            // audio buffer need reset.
            pCmd = strstr( recvCmds, "AT+DRAUD=" );
            if ( pCmd != NULL )
            {
                // do not call, audio going muted. I don't know why.
                //rtspp.SetReconfigAudio();
            }

        }

        // Need wait a little for thread CPU usage.
        Sleep( 10 );
    }

    if ( ignore_close_cmd == false )
    {
        char strClose[] = "UC+CLOSE";
        send( hSocket, strClose, strlen(strClose), 0 );
        Sleep(10);
    }

    closesocket( hSocket );

    if ( logger != NULL )
    {
        logger->write(" ... exit thread of (controlThread) \n ");
    }

    return 0;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

DWORD WINAPI findingipThread( LPVOID p )
{
    SOCKET      hSockTest = 0;
    SOCKADDR_IN addrSvr;
    ULONG       flag = 1;
    fd_set      fdset;
    struct
    timeval     tv;

    if ( logger != NULL )
    {
        logger->write(" Starting of find A-streaming on network ... \n ");
    }

    int ipend = 1;
    int ipme  = 0;
    unsigned char srchIps[4] = {0};

    if ( cfgSchIpSfx == false )
    {
        // Get current interface IP.
        winEthInfo netinfo = {0};

        if ( getFirstWinEthInfo( netinfo ) == true )
        {
            if ( logger != NULL )
            {
                logger->write( "Current system IP (first detected): %d.%d.%d.%d\n ",
                               netinfo.ip4[0],
                               netinfo.ip4[1],
                               netinfo.ip4[2],
                               netinfo.ip4[3] );
            }

            // need double check...
            if ( netinfo.ip4[0] == 0 )
            {
                if ( logger != NULL )
                {
                    logger->write("Current network is not availed situation !\n");
                    PostMessage( g_hwndSeach, WM_USER + 101, 0, 0 );
                    return 0;
                }
            }
        }
        else
        {
            if ( logger != NULL )
            {
                logger->write("No enabled network card found !\n");
                PostMessage( g_hwndSeach, WM_USER + 101, 0, 0 );
                return 0;
            }
        }

        ipme  = netinfo.ip4[3];

        srchIps[0] = netinfo.ip4[0];
        srchIps[1] = netinfo.ip4[1];
        srchIps[2] = netinfo.ip4[2];
        srchIps[3] = netinfo.ip4[3];
    }
    else
    {
        convert_ipstr_to_iparray( asipsfx, srchIps );
    }

    // Now finding A-streaming ...
    while( true )
    {
        char testip[64] = {0};

        sprintf( testip,
                 "%d.%d.%d.%d",
                 srchIps[0],
                 srchIps[1],
                 srchIps[2],
                 ipend );

        if ( logger != NULL )
        {
            logger->write("Test destination ip : %s ... \n ", testip );
        }

        if ( g_hwndSeach != INVALID_HANDLE_VALUE )
        {
            HWND hStatic = GetDlgItem( g_hwndSeach, IDC_STATIC_FINDING_AS );
            if ( hStatic != INVALID_HANDLE_VALUE  )
            {
                TCHAR outstr[256] = TEXT("Looking for [");

#ifdef UNICODE
                wchar_t ipstr[64] = {0};
                mbstowcs( ipstr, testip, 64 );
                wcscat( outstr, ipstr );
#else
                strcat( outstr, ipstr );
#endif
                _tcscat( outstr, TEXT("] ...") );

                SetWindowText( hStatic, outstr );
                SendMessage( hStatic, WM_PAINT, 0, 0 );
            }
        }

        hSockTest = socket( PF_INET, SOCK_STREAM, IPPROTO_TCP );

        if ( hSockTest > 0 )
        {
            // Set to non-block.
            ioctlsocket( hSockTest, FIONBIO, &flag );

            FD_ZERO( &fdset );
            FD_SET( hSockTest, &fdset );

            memset( &addrSvr, 0, sizeof(SOCKADDR_IN) );

            addrSvr.sin_family = AF_INET;
            addrSvr.sin_addr.s_addr = inet_addr( testip );
            addrSvr.sin_port = htons( 8801 );    /// control port is 8801

            int reti = connect( hSockTest, (SOCKADDR*)&addrSvr, sizeof(addrSvr) );

            bool isConn = false;

            if ( reti == SOCKET_ERROR )
            {
                int sop = 0;
                socklen_t soplen = sizeof( sop );

                int ret = getsockopt( hSockTest,
                                      SOL_SOCKET,
                                      SO_ERROR,
                                      (char*)&sop,
                                      &soplen );
                if ( ret >= 0 )
                {
                    if ( sop == 0 )
                    {
                        isConn = true;
                    }
                }
            }
            else
            {
                isConn = true;
            }

            if ( isConn == true )
            {
                tv.tv_sec  = 0;
                tv.tv_usec = 100000;
                select( hSockTest + 1, NULL, &fdset, NULL, &tv );

                int retset = FD_ISSET( hSockTest, &fdset );
                if (  retset == 0 )
                {
                    if ( logger != NULL )
                    {
                        logger->write("Failure!\n");
                    }

                    closesocket( hSockTest );
                }
                else
                {
                    if ( logger != NULL )
                    {
                        logger->write("Ok.\n");
                        logger->write("Found An ip : %s \n ", testip );
                    }

                    closesocket( hSockTest );
                    strcpy( asipnew, testip );
                    asipfound = true;
                    PostMessage( g_hwndSeach, WM_USER + 100, 0, 0 );

                    return 100;
                }
            }
            else
            {
                if ( logger != NULL )
                {
                    logger->write("Connection error: %d\n", reti );
                }
            }
        }

        ipend++;

        // Skip my IP.
        if ( ipend == ipme )
        {
            ipend++;
        }

        if ( ipend > 255 )
        {
            break;
        }
    }

    if ( logger != NULL )
    {
        logger->write(" End of find A-streaming on network ... \n ");
    }

    PostMessage( g_hwndSeach, WM_USER + 101, 0, 0 );

    return 0;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
