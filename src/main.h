#ifndef __MAIN_H__
#define __MAIN_H__

#include <windows.h>
#include <windowsx.h>
#include <tchar.h>
#include <D2D1.h>
#include <dwrite.h>
#include <wincodec.h>
#include <mmsystem.h>
#include <dsound.h>

#ifdef DEBUG
    #include <cstdio>
    #include <cstdlib>
    #include <cstring>
#endif

#endif /// of __MAIN_H__
