#include "mcustdc.h"

#ifdef DEBUG
    #include <stdio.h>
    #include <stdlib.h>
#endif

int  um_pow( int n, int m )
{
    int num = n;
    int cnt = 0;

    if ( m == 0 )
        return 0;

    for( cnt=0; cnt<m-1; cnt++ )
    {
        num *= n;
    }

    return num;
}

void um_memset( char* buf, unsigned char fill, unsigned long sz )
{
    int cnt = 0;

    if ( buf != NULL )
    {
        for( cnt=0; cnt<sz; cnt++ )
        {
            *buf = fill,
            buf++;
        }
    }
}

void um_atoh( const char* src, unsigned int* out )
{
    char cnt;
    char c_ch;
    char c_que=0;
    char c_buf[10]={0};

    if ( src != NULL )
    {
        while( *src )
        {
            c_ch = *src;
            if ( c_ch == ' ' )
                break;

            switch ( c_ch )
            {
                case '0': c_ch = 0; break;
                case '1': c_ch = 1; break;
                case '2': c_ch = 2; break;
                case '3': c_ch = 3; break;
                case '4': c_ch = 4; break;
                case '5': c_ch = 5; break;
                case '6': c_ch = 6; break;
                case '7': c_ch = 7; break;
                case '8': c_ch = 8; break;
                case '9': c_ch = 9; break;
                case 'a':
                case 'A': c_ch = 10; break;
                case 'b':
                case 'B': c_ch = 11; break;
                case 'c':
                case 'C': c_ch = 12; break;
                case 'd':
                case 'D': c_ch = 13; break;
                case 'e':
                case 'E': c_ch = 14; break;
                case 'f':
                case 'F': c_ch = 15; break;
            }

            c_buf[c_que] = c_ch;
            c_que++;
            if ( c_que > 4 )
                return;
            src++;
        }

        *out = 0;


        for(cnt=0;cnt<c_que-1;cnt++)
        {
            *out += c_buf[cnt] * um_pow( 16 , c_que - cnt - 1 );
        }
        *out += c_buf[cnt];

    }
}

int  um_atoi( const char* src )
{
    int  reti = 0;
    char neg = 0;
    char cnt;
    char c_ch;
    char c_que=0;
    char c_buf[32]={0};

    if ( src != NULL )
    {
        while( *src )
        {
            if ( *src == 0x20 )
                break;

            if ( *src == '-' )
            {
                neg = 1;
                src++;
            }
            else
            {
                c_ch = *src;

                switch( c_ch )
                {
                    case '0': c_ch = 0; break;
                    case '1': c_ch = 1; break;
                    case '2': c_ch = 2; break;
                    case '3': c_ch = 3; break;
                    case '4': c_ch = 4; break;
                    case '5': c_ch = 5; break;
                    case '6': c_ch = 6; break;
                    case '7': c_ch = 7; break;
                    case '8': c_ch = 8; break;
                    case '9': c_ch = 9; break;
                }

                if ( ( c_ch >= 0 ) && ( c_ch <= 9 ) )
                {
                    c_buf[c_que] = c_ch;

                    c_que++;

                    if ( c_que > 31 )
                        break;
                }

                src++;
            }
        }

        for(cnt=0;cnt<c_que-1;cnt++)
        {
            reti += c_buf[cnt] * um_pow( 10 , c_que - cnt - 1 );
        }

        reti += c_buf[cnt];

        if ( neg == 1 )
            reti *= -1;
    }

    return reti;
}

int  um_strlen( const char* buf )
{
    int slen = 0;

    while( *buf )
    {
        slen++;
        buf++;
    }

    return slen;
}

void um_strcat( char* out, const char* src )
{
    if ( ( out != NULL ) && ( src != NULL ) )
    {
        while( *out != NULL )
        {
            out++;
        }

        while( *src != NULL )
        {
            *out = *src;
            out++;
            src++;
        }

        *out = NULL;
    }
}

void um_memcpy( char* out, const char* src, long len )
{
    int cnt;

    if ( ( out != NULL ) && ( src != NULL ) && ( len > 0 ) )
    {
        for(cnt=0; cnt<len; cnt++ )
        {
            *out = *src;
            out++;
            src++;
        }
    }
}
