#ifndef __MCUSTDC_H__
#define __MCUSTDC_H__

#ifndef NULL
    #define NULL    0
#endif

void um_memset( char* buf, unsigned char fill, unsigned long sz );
void um_atoh( const char* src, unsigned int* out );
int  um_atoi( const char* src );
int  um_strlen( const char* buf );
void um_strcat( char* out, const char* src );
void um_memcpy( char* out, const char* src, long len );


#endif /// of __MCUSTDC_H__
