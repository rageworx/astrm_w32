#include <cstdio>
#include <cstdlib>
#ifdef DEBUG
    #include <cstring>
#endif

#include "mp3wav.h"
#include "mp3wave.h"

mp3wav::mp3wav()
 : wavdata( NULL ),
   wavsize( 0 )
{
    memset( &wavfmt, 0, sizeof(WAVEFORMATEX) );
}

mp3wav::~mp3wav()
{
    CloseStream();
}

bool mp3wav::OpenStream(const TCHAR* pcfilename)
{
    char* ascii_pcfilename = NULL;

    if ( sizeof(TCHAR) > 1 )
    {
        int wclen = wcslen( pcfilename );
        ascii_pcfilename = (char*)malloc( wclen + 1 );
        memset( ascii_pcfilename, 0, wclen + 1 );
        wcstombs( ascii_pcfilename, pcfilename, wclen );
    }
    else
    {
        ascii_pcfilename = (char*)_tcsdup( pcfilename );
    }

    FILE* fp = fopen( ascii_pcfilename, "rb" );
    if ( fp != NULL )
    {
        fseek( fp, 0, SEEK_END );
        int stream_sz = ftell( fp );
        fseek( fp, 0, SEEK_SET );

#ifdef DEBUG
        printf(" mp3 loaded, %d bytes.\n", stream_sz );
#endif
        unsigned char* stream_data = new unsigned char[ stream_sz ];

        if ( stream_data != NULL )
        {
            int mp3fsize = fread( stream_data, 1, stream_sz, fp );
            fclose( fp );

            if ( mp3fsize > 0 )
            {
                mp3track_info minfo = {0};

                wavsize = convert_mp3_to_wave( stream_data, mp3fsize, &wavdata, &minfo );

                wavfmt.cbSize = sizeof( WAVEFORMATEX );
                wavfmt.wFormatTag = WAVE_FORMAT_PCM;
                wavfmt.wBitsPerSample = 16;
                wavfmt.nChannels = minfo.channels;
                wavfmt.nSamplesPerSec = minfo.sample_rate;
                wavfmt.nBlockAlign = wavfmt.nChannels * wavfmt.wBitsPerSample / 8;
                wavfmt.nAvgBytesPerSec = wavfmt.nBlockAlign * wavfmt.nSamplesPerSec;
            }

            delete[] stream_data;
        }
        else
        {
            return false;
        }

        return true;
    }

    return false;
}

void mp3wav::CloseStream()
{
    if ( wavdata != NULL )
    {
        delete[] wavdata;
    }

    wavsize = 0;
}

WAVEFORMATEX* mp3wav::WaveFormat()
{
    return &wavfmt;
}
