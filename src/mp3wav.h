#ifndef __MP3WAV_H__
#define __MP3WAV_H__

#include <windows.h>
#include <tchar.h>
#include <mmsystem.h>

class mp3wav
{
    public:
        mp3wav();
        ~mp3wav();

    public:
        bool     OpenStream(const TCHAR* pcfilename);
        void     CloseStream();

    public:
        WAVEFORMATEX*   WaveFormat();
        unsigned char*  WaveData()  { return wavdata; }
        int             WaveSize()  { return wavsize; }

    protected:
        WAVEFORMATEX    wavfmt;
        unsigned char*  wavdata;
        int             wavsize;
};

#endif /// of __MP3WAV_H__
