// resource header
// -----------------------------------------------------------------------------

#define VERSION_I                       0,4,12,115
#define VERSION_S                       "0.4.12.115"

#ifdef _WIN64
#define VERSION_P                       "x86.64"
#else
#define VERSION_P                       "x86.32"
#endif // _WIN64

#ifndef IDC_STATIC
    #define IDC_STATIC (-1)
#endif /// of IDC_STATIC

#define ICON_A                                  1

#define IDD_DLG_CONF                            100
#define IDC_COMBO_AS_IP                         110
#define IDC_COMBO_AS_RES_SRC                    111
#define IDC_COMBO_AS_BPS_SRC                    112
#define IDC_COMBO_AS_RES_DST                    113

#define IDC_CHECK_USE_CUR_IP_SFX                120
#define IDC_CHECK_USE_CLEAR_TTF                 121
#define IDC_CHECK_USE_FULLSCREEN                122
#define IDC_CHECK_LOAD_KEYEXCHANGE              123

#define IDC_STATIC_AS_VERSION                   130
#define IDC_STATIC_AS_IP                        131
#define IDC_STATIC_AS_RES_SRC                   132
#define IDC_STATIC_AS_RES_DST                   133
#define IDC_STATIC_AS_INPUTS                    134
#define IDC_STATIC_AS_EXITKEY                   135
#define IDC_STATIC_AS_KEYEXCHNG                 136

#define IDC_RADIO_JOY_NORMAL                    140
#define IDC_RADIO_JOY_XINPUT                    141

#define IDC_BUTTON_AS_SEARCH                    150
#define IDC_BUTTON_AS_CHANGE_EXIT_KEY           151
#define IDC_BUTTON_AS_RES_SRC_APPLY             152
#define IDC_BUTTON_AS_SAVE                      154
#define IDC_BUTTON_AS_CANCEL                    155

#define IDD_DLG_SEARCH_AS                       200
#define IDC_STATIC_FINDING_AS                   240
#define IDC_PROGRESS_FINDING_AS                 241
#define IDC_BUTTON_CANCEL_FINDING_AS            242

#define IDC_BUTTON_EXIT_KEY                     951
