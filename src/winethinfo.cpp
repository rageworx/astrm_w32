#include <windows.h>
#include <tchar.h>

#include <winsock2.h>
#include <iphlpapi.h>
#include <Ifdef.h>

#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "winethinfo.h"

bool convert_ipstr_to_iparray( const char* is, unsigned char* ia )
{
    if ( ( is == NULL ) || ( ia == NULL ) )
    {
        return false;
    }

    char* srcs = strdup( is );

    if ( srcs == NULL )
    {
        return false;
    }

    char* stok = strtok( srcs, "." );
    int   ncnt = 0;

    while( stok != NULL )
    {
        unsigned in = atoi( stok );
        ia[ncnt] = in;
        ncnt++;
        if ( ncnt > 3 )
            break;
        stok = strtok( NULL, "." );
    }

    free( srcs );

    return true;
}

bool getFirstWinEthInfo( winEthInfo& info )
{
    PIP_ADAPTER_INFO pAdapterInfo;
    PIP_ADAPTER_INFO pAdapter = NULL;
    DWORD dwRetVal = 0;
    ULONG ulOutBufLen = sizeof( IP_ADAPTER_INFO );

    pAdapterInfo = new IP_ADAPTER_INFO;

    if ( pAdapterInfo == NULL )
    {
        return false;
    }

    if ( GetAdaptersInfo( pAdapterInfo, &ulOutBufLen ) == ERROR_BUFFER_OVERFLOW )
    {
        delete pAdapterInfo;

        //pAdapterInfo = (IP_ADAPTER_INFO *)SYS_MALLOC(ulOutBufLen);
        pAdapterInfo = new IP_ADAPTER_INFO[ ulOutBufLen / sizeof( IP_ADAPTER_INFO ) ];
        if ( pAdapterInfo == NULL )
        {
            return false;
        }
    }

    dwRetVal = GetAdaptersInfo( pAdapterInfo, &ulOutBufLen );

    if ( dwRetVal == NO_ERROR )
    {
        pAdapter = pAdapterInfo;
        while ( pAdapter != NULL )
        {
            if ( ( pAdapter->Type == MIB_IF_TYPE_ETHERNET ) ||
                 ( pAdapter->Type == IF_TYPE_IEEE80211 ) ) /// WTF?!? MS, IF_TYPE_IEEE80211 is Wireless..
            {
                // test ip
                unsigned char testip4[4] = {0};
                unsigned char testnm4[4] = {0};
                unsigned char testgw4[4] = {0};

                convert_ipstr_to_iparray( pAdapter->IpAddressList.IpAddress.String,
                                          testip4 );
                convert_ipstr_to_iparray( pAdapter->IpAddressList.IpMask.String,
                                          testnm4 );
                convert_ipstr_to_iparray( pAdapter->GatewayList.IpAddress.String,
                                          testgw4 );

                // IP4 "0.0.0.0" and netmask4 "0.0.0.0" are not allowed to sens.
                if ( ( testip4[0] > 0 ) &&
                     ( testnm4[0] > 0 ) &&
                     ( testgw4[0] > 0 ) )
                {
                    info.comboindex = pAdapter->ComboIndex;
                    strcpy_s( info.name, WINETHSTRLEN, pAdapter->AdapterName  );
                    strcpy_s( info.description, WINETHSTRLEN, pAdapter->Description );

                    for (int cnt=0; cnt<pAdapter->AddressLength; cnt++)
                    {
                        if ( cnt < 8 )
                        {
                            info.mac[cnt] = (unsigned char)pAdapter->Address[cnt];
                        }
                    }

                    info.adaptorindex = pAdapter->Index;

                    convert_ipstr_to_iparray( pAdapter->IpAddressList.IpAddress.String,
                                              info.ip4 );

                    convert_ipstr_to_iparray( pAdapter->IpAddressList.IpMask.String,
                                              info.mask4 );

                    convert_ipstr_to_iparray( pAdapter->GatewayList.IpAddress.String,
                                              info.gateway4 );

                    if ( pAdapter->DhcpEnabled > 0 )
                    {
                        info.DHCP = true;
                        convert_ipstr_to_iparray( pAdapter->DhcpServer.IpAddress.String,
                                                  info.DHCPip4 );
                    }

                    if ( pAdapterInfo != NULL )
                    {
                        delete pAdapterInfo;
                    }

                    return true;
                }
            }

            pAdapter = pAdapter->Next;
        }
    }

    if ( pAdapterInfo != NULL )
    {
        delete pAdapterInfo;
    }

    return false;
}
