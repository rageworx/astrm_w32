// refer to:
// https://msdn.microsoft.com/en-us/library/windows/desktop/aa365917%28v=vs.85%29.aspx
//
// You may need link with IPHLPAPI

#define WINETHSTRLEN        128

typedef struct _winEthInfo
{
    int             comboindex;
    char            name[WINETHSTRLEN];
    char            description[WINETHSTRLEN];
    unsigned char   mac[8];
    int             adaptorindex;
    unsigned char   ip4[4];
    unsigned char   mask4[4];
    unsigned char   gateway4[4];
    bool            DHCP;
    unsigned char   DHCPip4[4];
}winEthInfo;

bool convert_ipstr_to_iparray( const char* is, unsigned char* ia );
bool getFirstWinEthInfo( winEthInfo& info );
